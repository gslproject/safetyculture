﻿
Public Class ReportCourse
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("iIncident")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadCompany()
            LoadCourse
            'txtStartDate.Text = Today.Date.ToString("dd/MM/yyyy")
            'txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")
        End If

    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("iIncident")("ROLE_SPA") = True Or Request.Cookies("iIncident")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("iIncident")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub LoadCourse()
        Dim ctlT As New ElearningController
        ddlCourse.DataSource = ctlT.Course_GetActive '(ddlCompany.SelectedValue)
        ddlCourse.DataTextField = "Name"
        ddlCourse.DataValueField = "UID"
        ddlCourse.DataBind()
    End Sub


    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        If ddlCourse.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกหลักสูตรก่อน');", True)
            Exit Sub
        End If

        'Dim Bdate, Edate, Cond As String
        ''Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
        ''Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)

        'Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + ConvertYearEN(CInt(Left(txtStartDate.Text, 2)))
        'Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + ConvertYearEN(CInt(Left(txtEndDate.Text, 2)))


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=csprog&COM=" & ddlCompany.SelectedValue & "&CS=" & ddlCourse.SelectedValue & "&RPTTYPE=" & IIf(chkExcel.Checked = True, "EXCEL", "PDF") & "','_blank');", True)

    End Sub

End Class

