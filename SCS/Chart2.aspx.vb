﻿Imports System.Net.Mail

Public Class Chart2
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    'Dim dtAll As New DataTable
    Dim dtDv As New DataTable
    Dim dtDp As New DataTable
    Public Shared dtItem As New DataTable
    Public Shared catebar1 As String
    Public Shared catebar2 As String
    Public Shared databar1 As String
    Public Shared databar2 As String

    'Public Shared databar2All As String
    'Public Shared databar2Asm As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            If Request("y") = "0" Then
                lblYear.Text = ""
            Else
                lblYear.Text = Request("y")
            End If


            If Not IsNothing(Request.Cookies("SafetyCS")) Then

                dtItem = ctlR.EvaluationGroup_GetByEvaluationUID(1)

                databar1 = ""

                dtDv = ctlR.RPT_Assessment_AvgScore(StrNull2Zero(Request("y")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                catebar1 = ""
                databar1 = ""

                For i = 0 To dtDv.Rows.Count - 1
                    catebar1 = catebar1 + "'Q" + dtDv.Rows(i)("Code") + "'"
                    databar1 = databar1 + dtDv.Rows(i)("AvgScore").ToString()

                    If i < dtDv.Rows.Count - 1 Then
                        catebar1 = catebar1 + ","
                        databar1 = databar1 + ","
                    End If
                Next

                dtDp = ctlR.RPT_Assessment_AvgScoreByGroup(StrNull2Zero(Request("y")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                catebar2 = ""
                databar2 = ""

                For i = 0 To dtDp.Rows.Count - 1
                    catebar2 = catebar2 + "'" + dtDp.Rows(i)("EvaluationGroup") + "'"
                    databar2 = databar2 + dtDp.Rows(i)("Score").ToString()

                    If i < dtDp.Rows.Count - 1 Then
                        catebar2 = catebar2 + ","
                        databar2 = databar2 + ","
                    End If
                Next

            End If
        End If
    End Sub

    Protected Sub cmdExport_Click(sender As Object, e As EventArgs) Handles cmdExport.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=sss&comp=" & Request.Cookies("SafetyCS")("LoginCompanyUID") & "&y=" & Request("y") & "&RPTTYPE=EXCEL','_blank');", True)
    End Sub
End Class