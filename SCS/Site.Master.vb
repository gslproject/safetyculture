﻿Imports System.IO
Imports Newtonsoft.Json
Public Class Site
    Inherits System.Web.UI.MasterPage
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadUserDetail()
        End If
        LoadCalendarData()
    End Sub

    Private Sub LoadUserDetail()
        Dim ctlU As New UserController
        dt = ctlU.User_GetByUserID(Request.Cookies("SafetyCS")("userid"))
        If dt.Rows.Count > 0 Then
            lblUserName1.Text = String.Concat(dt.Rows(0)("Name"))
            lblUsername2.Text = String.Concat(dt.Rows(0)("Name"))

            'lblProfileDesc.Text = "1/2564"
            'lblUserDesc.Text = "1/2564"

            If DBNull2Str(dt.Rows(0).Item("ImagePath")) <> "" Then
                Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")))

                If objfile.Exists Then
                    imgUser1.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")
                    imgUser2.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")
                Else
                    imgUser1.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                    imgUser2.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                End If

            End If

        End If
        dt = Nothing
    End Sub

    Dim ctlM As New MasterController
    Dim dtCalendar As New DataTable
    Public Shared json As String
    Private Sub LoadCalendarData()
        'If Request.Cookies("SafetyCS")("ROLE_ASR") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Or Request.Cookies("SafetyCS")("ROLE_SPA") = True Then
        '    dtCalendar = ctlE.Event_GetByAdmin(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginPersonUID")))
        'Else
        '    dtCalendar = ctlE.Event_GetByUser(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginPersonUID")))
        'End If


        dtCalendar = ctlM.Event_GetByUser(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginPersonUID")))

        'dtCalendar = ctlE.Event_GetByAdmin(1, 1)

        json = JsonConvert.SerializeObject(dtCalendar, Formatting.Indented)
    End Sub



End Class