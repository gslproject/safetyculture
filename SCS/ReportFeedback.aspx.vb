﻿
Public Class ReportFeedback
    Inherits System.Web.UI.Page

    Dim ctlR As New ReportController
    Dim dtAll As New DataTable
    Dim dtDv As New DataTable
    'Dim dtDp As New DataTable

    Public Shared catebar1 As String
    'Public Shared catebar2 As String
    Public Shared databar1 As String
    'Public Shared databar2 As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            'If Request("y") = "0" Then
            '    lblYear.Text = ""
            'Else
            '    lblYear.Text = Request("y")
            'End If

            If Not IsNothing(Request.Cookies("SafetyCS")) Then
                databar1 = ""

                dtAll = ctlR.RPT_Assessment360_AvgScoreByEmployeeLevel_Pivot(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))
                If Not dtAll Is Nothing Then
                    grdData.DataSource = dtAll
                    grdData.DataBind()
                Else
                    grdData.DataSource = Nothing
                End If

                dtDv = ctlR.RPT_Assessment360_AvgScoreByEmployeeLevel(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                If dtDv.Rows.Count <= 0 Then
                    cmdExport.Visible = False
                    Exit Sub
                Else
                    cmdExport.Visible = True
                End If
                'catebar1 = ""
                'databar1 = ""

                'For i = 0 To dtDv.Rows.Count - 1
                '    catebar1 = catebar1 + "'Q" + dtDv.Rows(i)("Code") + "'"
                '    databar1 = databar1 + dtDv.Rows(i)("AvgScore").ToString()

                '    If i < dtDv.Rows.Count - 1 Then
                '        catebar1 = catebar1 + ","
                '        databar1 = databar1 + ","
                '    End If
                'Next


                Dim sName As String
                Dim sData As String


                catebar1 = ""
                databar1 = ""

                sName = dtDv.Rows(0)("LevelName").ToString()
                sData = "{" & vbCrLf & "name: '" & sName & "'," & vbCrLf & "data:["


                For i = 0 To dtDv.Rows.Count - 1

                    If sName = String.Concat(dtDv.Rows(i)("LevelName")) Then
                        sData = sData & dtDv.Rows(i)("AvgScore").ToString()
                    Else
                        sName = dtDv.Rows(i)("LevelName").ToString()
                        sData = Left(sData, Len(sData) - 1)
                        sData = sData & "]" & vbCrLf & "}," & vbCrLf & "{" & vbCrLf & "name: '" & sName & "'," & vbCrLf & "data:[" & dtDv.Rows(i)("AvgScore").ToString()

                    End If

                    'catebar2 = catebar2 & "'" & dtdv.Rows(i)("EvaluationGroup") & "'"

                    If i < dtDv.Rows.Count - 1 Then
                        sData = sData & ","
                        'catebar2 = catebar2 & ","
                    End If
                Next

                databar1 = sData & "]" & vbCrLf & "}"
                catebar1 = "'Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8','Q9'"

                'Q1.SETS THE STANDARD AND EXPECTATIONS',
                'Q2.WALK THE TALK AND ROLE MODELS',
                'Q3.VISIBILITY',
                'Q4.CHALLENGES',
                'Q5.EMPOWER OTHERS',
                'Q6.COMMUNICATES',
                'Q7.COACHES',
                'Q8.PROMOTES IMPROVEMENTS AND SHARING',
                'Q9.ARTICULATES THE VISION FOR SAFETY',



                'catebar2 = ""
                'databar2 = ""

                'For i = 0 To dtDp.Rows.Count - 1
                '    catebar2 = catebar2 + "'" + dtDp.Rows(i)("EvaluationGroup") + "'"
                '    databar2 = databar2 + dtDp.Rows(i)("Score").ToString()

                '    If i < dtDp.Rows.Count - 1 Then
                '        catebar2 = catebar2 + ","
                '        databar2 = databar2 + ","
                '    End If
                'Next

            End If
        End If

    End Sub
    Protected Sub cmdExport_Click(sender As Object, e As EventArgs) Handles cmdExport.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=sls&comp=" & Request.Cookies("SafetyCS")("LoginCompanyUID") & "&y=" & Request.Cookies("SafetyCS")("ASSMYEAR") & "&RPTTYPE=EXCEL','_blank');", True)
    End Sub
End Class

