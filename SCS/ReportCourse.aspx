﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportCourse.aspx.vb" Inherits="iIncident.ReportCourse" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>Course Progressing<small></small></h1>   
</section>

<section class="content">  

         <div class="box box-solid">
            <div class="box-header">           
            </div>
            <div class="box-body"> 
 <div class="col-lg-2"></div>

<div class="col-lg-8 row" style="background-color:#14539a;color: white">
      <br />   <br />   <br />      
      <div class="row">
   <div class="col-md-6">
          <div class="form-group">
            <label>Company</label>
              <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%">                   
            </asp:DropDownList>
          </div>

        </div>

           <div class="col-md-6">
          <div class="form-group">
            <label>Course</label>
                <asp:DropDownList ID="ddlCourse" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
         </div>
               
                <div class="row"> 
                    <div class="col-md-2">
          <div class="form-group">           
              <asp:CheckBox ID="chkExcel" runat="server" Text="Excel"  />
          </div>

        </div>
     
      
      </div>

  <div class="row">
   <div class="col-md-12 text-center">
       <asp:Button ID="cmdPrint" runat="server" CssClass="btn btn-info" Width="120px"   Text="Print Preview" />
   
      
       </div>
      </div>

       <br />   <br />   <br />
    </div>
 <div class="col-lg-2"></div>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

</section>   
</asp:Content>
