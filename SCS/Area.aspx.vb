﻿
Public Class Area
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            ClearData()
            txtUID.Text = ""
            'LoadDivision()
            LoadAreaToGrid()
        End If

    End Sub

    'Private Sub LoadDivision()
    '    ddlDivision.DataSource = ctlM.Division_Get(Request.Cookies("SafetyCS")("LoginCompanyUID"))
    '    ddlDivision.DataTextField = "DivisionName"
    '    ddlDivision.DataValueField = "DivisionUID"
    '    ddlDivision.DataBind()
    'End Sub


    Private Sub LoadAreaToGrid()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlM.Area_GetBySearch(Request.Cookies("SafetyCS")("LoginCompanyUID"), txtSearch.Text)
        Else
            dt = ctlM.Area_GetAll(Request.Cookies("SafetyCS")("LoginCompanyUID"))
        End If
        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlM.Area_Delete(e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("SafetyCS")("username"), ACTTYPE_DEL, "Area", "ลบชื่อสถานที่เกิดเหตุ :" & txtName.Text, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadAreaToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

                    End If


            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlM.Area_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtUID.Text = DBNull2Str(dt.Rows(0)("UID"))
                txtName.Text = DBNull2Str(dt.Rows(0)("Name"))
                txtSort.Text = DBNull2Str(dt.Rows(0)("DisplayOrder"))
                chkStatus.Checked = ConvertStatusFlag2CHK(dt.Rows(0)("StatusFlag"))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.txtUID.Text = ""
        txtUID.Text = ""
        txtName.Text = ""
        txtSort.Text = "0"
        chkStatus.Checked = True
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(4).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If

        Dim item As Integer

        If txtUID.Text = "" Then
            If ctlM.Area_CheckDuplicate(Request.Cookies("SafetyCS")("LoginCompanyUID"), txtName.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ชื่อสถานที่เกิดเหตุนี้มีอยู่่ในระบบแล้ว');", True)
                Exit Sub
            End If
            item = ctlM.Area_Add(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
        Else
            item = ctlM.Area_Update(StrNull2Zero(txtUID.Text), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
        End If


        LoadAreaToGrid()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)


    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadAreaToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadAreaToGrid()
    End Sub
End Class

