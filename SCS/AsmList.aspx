﻿<%@ Page Title="Employee List" MetaDescription="รายชื่อพนักงานที่ท่านต้องประเมิน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmList.aspx.vb" Inherits="SCS.AsmList" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
     
<script>
    $(document).ready(function() {
    $('#tbdata').DataTable( {
        "order": [[ 6, "desc" ]]
    } );
} );
 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-check icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-database icon-gradient bg-success">
            </i>พนักงานที่ท่านต้องประเมิน
            <div class="btn-actions-pane-right">                
            </div>
        </div>
        <div class="card-body">                  
              <table id="tbdata" class="table table-hover table-striped table-borderless dataTable dtr-inline">
                <thead>
                <tr>
                  
                    <th>รหัส</th>
                  <th>ชื่อ-นามสกุล</th>
                  <th>ตำแหน่ง</th>
                      <th>แผนก</th>
                  <th>ฝ่าย</th>   
                      <th>ระดับ</th>   
                       <th aria-sort="ascending">กลุ่ม</th> 
                     <th>สถานะ</th>  
                     <th class="sorting_asc_disabled"></th>   
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtEmp.Rows %>
                <tr>              
                 <td><% =String.Concat(row("Code")) %></td>
                 <td><% =String.Concat(row("EmployeeName")) %></td>
                 <td><% =String.Concat(row("PositionName")) %></td>
                 <td><% =String.Concat(row("DepartmentName")) %></td> 
                 <td><% =String.Concat(row("DivisionName")) %></td> 
                 <td><% =String.Concat(row("EmployeeLevelName")) %></td>
                 <td aria-sort="ascending"><% =String.Concat(row("AssesseeType")) %></td>
                 <td><% =String.Concat(row("AsmStatus")) %> </td>
                 <td width="60"><a  href="AsmSCS360.aspx?id=<% =String.Concat(row("AssessmentUID")) %>&empuid=<% =String.Concat(row("EmployeeUID")) %>" class="btn btn-primary" >ประเมิน</a></td>                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
  
    </section>
</asp:Content>
