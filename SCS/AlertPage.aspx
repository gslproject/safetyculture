﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AlertPage.aspx.vb" Inherits="SCS.AlertPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-attention icon-gradient bg-red"></i>
                                </div>
                                <div>Alert Page
                                    <div class="page-title-subheading"></div>
                                </div>
                            </div>
                        </div>
</div> 

    <section class="content">

      <div class="error-page">
        <h2 class="headline text-red"><i class="fa fa-exclamation-triangle text-red"></i></h2>

        <div class="error-content"><br /><br />
          <h3>Oops! Something went wrong.
              <br />หมดเขตให้ทำการประเมินแล้ว</h3>

          <p> 
          </p>         
        </div>
      </div>
    </section>

</asp:Content>
