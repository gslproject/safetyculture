﻿Imports System.Data
Public Class AsmList
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessmentController
    Dim ctlM As New MasterController
    Public dtEmp As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        Dim dtA As New DataTable
        Dim AsmYear As Integer = ctlM.AsmYear_Get()

        dtA = ctlA.Assessor_GetDetail(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
        If dtA.Rows.Count > 0 Then
            Select Case dtA.Rows(0)("EmployeeLevelID")
                Case "1"
                    dtEmp = ctlA.Assessee_GetByTop(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                Case "2"
                    dtEmp = ctlA.Assessee_GetByManager(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                Case "3"
                    dtEmp = ctlA.Assessee_GetBySupervisor(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                Case "4"
                    dtEmp = ctlA.Assessee_GetByEmployee(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                Case Else
                    dtEmp = ctlA.Assessee_GetByEmployee(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
            End Select
            'dtEmp.DefaultView.Sort = "AssesseeType"
        End If
    End Sub

End Class