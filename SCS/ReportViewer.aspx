<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportViewer.aspx.vb" Inherits="SCS.ReportViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
<section class="content">             
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-print"></i>
              <h3 class="box-title"><asp:Label ID="lblReportTitle" runat="server" Text=""></asp:Label></h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>                 
            </div>
            <div class="box-body">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" BackColor="White" DocumentMapWidth="100%" ZoomMode="PageWidth">
    </rsweb:ReportViewer>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>     
</asp:Content>
