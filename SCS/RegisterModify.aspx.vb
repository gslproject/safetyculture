﻿
Public Class RegisterModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlStd As New RegisterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            LoadBusinessTypeToDDL()
            LoadProvinceToDDL()

            If Not Request("pid") = Nothing Then
                LoadRegisterData()
            End If
        End If

        'txtGPAX.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
    End Sub
    Private Sub LoadBusinessTypeToDDL()
        dt = ctlM.BusinessType_Get
        If dt.Rows.Count > 0 Then
            With ddlBusinessType
                .Enabled = True
                .DataSource = dt
                .DataTextField = "BusinessName"
                .DataValueField = "UID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadProvinceToDDL()
        dt = ctlM.Province_Get
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadRegisterData()
        dt = ctlStd.Register_GetByID(Request("pid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdRegisterID.Value = String.Concat(.Item("UID"))
                optPersonType.SelectedValue = String.Concat(.Item("PersonType"))
                txtContactName.Text = DBNull2Str(.Item("ContactName"))
                txtCompanyName.Text = DBNull2Str(.Item("CompanyName"))
                ddlBusinessType.SelectedValue = .Item("BusinessTypeID")
                txtTel.Text = String.Concat(.Item("Tel"))
                txtMail.Text = String.Concat(.Item("Email"))
                txtAddressNo.Text = String.Concat(.Item("AddressNo"))
                txtMoo.Text = String.Concat(.Item("Moo"))
                txtSoi.Text = String.Concat(.Item("Soi"))
                txtRoad.Text = String.Concat(.Item("Road"))
                txtTumbol.Text = String.Concat(.Item("Subdistrict"))
                txtCity.Text = String.Concat(.Item("District"))
                ddlProvince.SelectedValue = .Item("ProvinceID")
                txtZipcode.Text = String.Concat(.Item("Zipcode"))
                txtWebsite.Text = String.Concat(.Item("Website"))
                txtAddress4Inv.Text = String.Concat(.Item("AddressInvoice"))

                chkErgo.Checked = ConvertYN2Boolean(String.Concat(.Item("isErgonomic")))
                chkLA.Checked = ConvertYN2Boolean(String.Concat(.Item("isLA")))
                chkIncident.Checked = ConvertYN2Boolean(String.Concat(.Item("isIncident")))
                chkSafety.Checked = ConvertYN2Boolean(String.Concat(.Item("isSafetyCulture")))
            End With
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtContactName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อผู้ติดต่อก่อน');", True)
            Exit Sub
        End If
        If txtTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุเบอร์โทรสำหรับติดต่อก่อน');", True)
            Exit Sub
        End If

        ctlStd.Register_Save(StrNull2Zero(hdRegisterID.Value), optPersonType.SelectedValue, txtContactName.Text, txtCompanyName.Text, ddlBusinessType.SelectedValue, txtTel.Text, txtMail.Text, txtAddressNo.Text, txtMoo.Text, txtSoi.Text, txtRoad.Text, txtTumbol.Text, txtCity.Text, StrNull2Zero(ddlProvince.SelectedValue), txtZipcode.Text, txtAddress4Inv.Text, txtWebsite.Text, ConvertBoolean2YN(chkErgo.Checked), ConvertBoolean2YN(chkLA.Checked), ConvertBoolean2YN(chkIncident.Checked), ConvertBoolean2YN(chkSafety.Checked))

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

End Class