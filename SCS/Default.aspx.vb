﻿
Public Class _Default
    Inherits Page
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Request("logout") = "y" Then
            Session.Contents.RemoveAll()
            Session.Abandon()
            Session.RemoveAll()
            Response.Cookies.Clear()

            Dim delCookie1 As HttpCookie
            delCookie1 = New HttpCookie("SafetyCS")
            delCookie1.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(delCookie1)
        Else
            'RenewSessionFromCookie()
        End If

        If Not IsPostBack Then
            If Request("logout") = "y" Then
                Session.Abandon()
                Request.Cookies("SafetyCS")("userid") = Nothing
            End If
            txtUsername.Focus()
        End If

        Session.Timeout = 360
        'Response.Redirect("Home")

    End Sub

    Private Sub CheckUser()
        dt = acc.User_CheckLogin(txtUsername.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            '--------------------------------------------------------------------------------------------

            Dim SCSCookie As HttpCookie = New HttpCookie("SafetyCS")
            SCSCookie("userid") = dt.Rows(0).Item("UserID")
            SCSCookie("username") = String.Concat(dt.Rows(0).Item("USERNAME")).ToString()
            SCSCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            SCSCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            SCSCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            SCSCookie("LoginPersonUID") = dt.Rows(0).Item("UserID")
            SCSCookie("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            SCSCookie("ROLE_ADM") = False
            SCSCookie("ROLE_CUS") = False
            SCSCookie("ROLE_ASR") = False
            SCSCookie("ROLE_SPA") = False


            Dim ctlCf As New MasterController
            SCSCookie("ASSMYEAR") = ctlCf.DataConfig_GetByCode(DBNull2Zero(dt.Rows(0).Item("CompanyUID")), CFG_ASSMYEAR)

            SCSCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(SCSCookie)


            'If Request.Cookies("SafetyCS")("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    Request.Cookies("SafetyCS")("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(Request.Cookies("SafetyCS")("ProfileID"))
            'End If

            'Dim ctlCfg As New SystemConfigController()
            'Request.Cookies("SafetyCS")("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'Request.Cookies("SafetyCS")("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            'genLastLog()
            'acc.User_GenLogfile(txtUsername.Text, ACTTYPE_LOG, "Users", "", "")

            If acc.User_IsAssessmentLevel(dt.Rows(0).Item("UserID")) Then
                SCSCookie("ROLE_ASR") = True
                Request.Cookies("SafetyCS")("ROLE_ASR") = True
            End If


            Dim rd As New UserRoleController

                dt = rd.UserAccessGroup_GetByUID(dt.Rows(0).Item("UserID"))
                If dt.Rows.Count > 0 Then

                    For i = 0 To dt.Rows.Count - 1
                        Select Case dt.Rows(i)("UserGroupUID")
                            Case 1   'Customer Admin
                                SCSCookie("ROLE_ADM") = True
                                Request.Cookies("SafetyCS")("ROLE_ADM") = True
                            Case 2  'Customer User
                                SCSCookie("ROLE_CUS") = True
                                Request.Cookies("SafetyCS")("ROLE_CUS") = True
                        'Case 3 'Assessor
                        '    SCSCookie("ROLE_ASR") = True
                        '    Request.Cookies("SafetyCS")("ROLE_ASR") = True
                            Case 9  'NPC-SE Administrator
                                SCSCookie("ROLE_SPA") = True
                                Request.Cookies("SafetyCS")("ROLE_SPA") = True
                        End Select
                    Next

                End If

                'Select Case Request.Cookies("SafetyCS")("UserProfileID")
                '    Case 1 'Customer
                '        Response.Redirect("Default.aspx")
                '    Case 2, 3 'Teacher
                '        Response.Redirect("Default.aspx")
                '    Case 4 'Admin
                Response.Redirect("Home")
                'End Select

            Else
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        'txtUsername.Value = "admin"
        'txtPassword.Value = "112233"
        'Request.Cookies("SafetyCS")("userid") = 1
        'Request.Cookies("SafetyCS")("ROLE_SPA") = True
        'Response.Redirect("Home")
        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUsername.Text)
    End Sub

End Class