﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportIncident.aspx.vb" Inherits="SCS.ReportIncident" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div class="app-page-title">
          <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph2 icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Incident Report
                            <div class="page-title-subheading">รายงานอุบัติการณ์</div>
                        </div>
                    </div>
                </div>
            </div>    

<section class="content">  
         <div class="box box-solid">       
            <div class="box-body"> 
<div class="col-lg-12" style="background-color:#14539a;color: white">
      <br />   
   <div class="row"> 
     
   <div class="col-md-12">
          <div class="form-group">
            <label>Company</label>
              <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%">             
            </asp:DropDownList>
          </div>

        </div>

           <div class="col-md-6">
          <div class="form-group">
            <label>Type</label>
                <asp:DropDownList ID="ddlType" runat="server" cssclass="form-control select2" Width="100%">
                   
            </asp:DropDownList>
          </div>

        </div>
       <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
                <asp:DropDownList ID="ddlStatus" runat="server" cssclass="form-control select2" Width="100%">
                    <asp:ListItem value="A" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="O">Open</asp:ListItem>
                     <asp:ListItem Value="X">Close</asp:ListItem> 
            </asp:DropDownList>
          </div>

        </div>
              

            <div class="col-md-6">
          <div class="form-group">
            <label>Start Date</label>
              <div class="input-group">                
                  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"  autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                   <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                </div>
              <!-- data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask -->
            
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>End Date</label>
              <div class="input-group">                 
                  <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                   <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                </div>
          </div>

        </div>
      
       </div>
             <div class="row"> 
                    <div class="col-md-2">
          <div class="form-group">           
              <asp:CheckBox ID="chkExcel" runat="server" Text="Excel" Checked="True"  />
          </div>

        </div>
        </div>
      

  <div class="row">
   <div class="col-md-12 text-center">
       <asp:Button ID="cmdPrint" runat="server" CssClass="btn btn-info" Width="120px"   Text="Export" />
   
      
       </div>
      </div>

       <br />   <br />   <br />
    </div>

</div>
           
          </div>            
  
</section>   
</asp:Content>
