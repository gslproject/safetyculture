﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Chart2All.aspx.vb" Inherits="SCS.Chart2All" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>
    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <section class="content-header">
    <h1>Safety Culture Survey Analysis <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
      <small></small>
    </h1>  
  </section>

  <!-- Main content -->
  <section class="content">  
          <div class="row"> 
               <section class="col-lg-6 connectedSortable">
  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-database icon-gradient bg-success">
            </i>
            <div class="btn-actions-pane-right">                
            </div>
        </div>
        <div class="card-body">     
               <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" Width="100%" PageSize="20" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Left" />
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True"                  VerticalAlign="Middle" HorizontalAlign="Left" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>

            
            </div> 
          </div>
  
          
</section>   
      <section class="col-lg-6 connectedSortable">
          <div class="box box-solid">  
    <div class="box-body">             
                  <div id="chart"></div>                 
              </div>
          </div>
</section>   
      
        </div>     
        <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdExport" CssClass="btn btn-primary" runat="server" Text="Export Data" Width="120px" /> 
        </div>
      </div>
</section> 
    <script>      
        var options = {
         series: [<%=databar2 %>],
          chart: {
          height: 400,
              type: 'radar',
          dropShadow: {
            enabled: true,
            blur: 1,
            left: 1,
            top: 1
          }
        },
        title: {
          text: ''
            },
          dataLabels: {
              enabled: true,              
          formatter: function (val) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ['#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8']
          }
        },
        xaxis: {
          categories: [<%=catebar2 %>]
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>
                
</asp:Content>