﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Microsoft.Reporting.WebForms

Public Class ReportViewer
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Public Shared ReportFormula As String
    Dim ReportFileName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request("RPTTYPE") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        LoadReport()
    End Sub
    Private Sub LoadReport()

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        Dim xParam As New List(Of ReportParameter)

        Select Case Request("r")
            Case "personal"
                ReportFileName = "SafetyLeadershipPersonal" & "_" & Request("emp")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SafetyLeadershipPersonal")
                xParam.Add(New ReportParameter("AsmYear", Request("y").ToString()))
                xParam.Add(New ReportParameter("EmployeeUID", Request("emp").ToString()))
                'xParam.Add(New ReportParameter("IncidentStatus", Request("st").ToString()))
                'xParam.Add(New ReportParameter("StartDate", Request("bdt").ToString()))
                'xParam.Add(New ReportParameter("EndDate", Request("edt").ToString()))
                'xParam.Add(New ReportParameter("UserID", Request.Cookies("SafetyCS")("userid").ToString()))
            Case "seg"
                ReportFileName = "SurveyEngagement" & "_" & Request("y")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SurveyEngagementByDivision")
                xParam.Add(New ReportParameter("AsmYear", Request("y").ToString()))
                xParam.Add(New ReportParameter("CompanyUID", Request("comp").ToString()))
            Case "sss"
                ReportFileName = "SafetySurveyScore" & "_" & Request("y")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SafetySurveyScore")
                xParam.Add(New ReportParameter("AsmYear", Request("y").ToString()))
                xParam.Add(New ReportParameter("CompanyUID", Request("comp").ToString()))
            Case "ssg"
                ReportFileName = "SafetySurveyAvgScore" & "_" & Request("comp")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SafetySurveyAvgScore")
                xParam.Add(New ReportParameter("CompanyUID", Request("comp").ToString()))
            Case "sls"
                ReportFileName = "SafetyLeaderSurveyScore" & "_" & Request("y")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SafetyLeaderSurveyScore")
                xParam.Add(New ReportParameter("AsmYear", Request("y").ToString()))
                xParam.Add(New ReportParameter("CompanyUID", Request("comp").ToString()))
            Case "le"
                ReportFileName = "SafetyLeaderEngagement" & "_" & Request("y")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SafetyLeaderEngagement")
                xParam.Add(New ReportParameter("AsmYear", Request("y").ToString()))
                xParam.Add(New ReportParameter("CompanyUID", Request("comp").ToString()))
            Case Else
                Exit Sub
                'ReportViewer1.ServerReport.ReportPath = credential.ReportPath("")
                'xParam.Add(New ReportParameter("UserID", Request.Cookies("SafetyCS")("userid").ToString()))
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        Select Case Request("RPTTYPE") ' FagRPT
            Case "EXCEL"
                ' Variables
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty
                Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = mimeType
                Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=" & ReportFileName & "." & extension))
                Response.BinaryWrite(bytes)
                Response.Flush()
            Case Else
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty

                Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

                Response.Buffer = True
                Response.Clear()
                Response.ContentType = mimeType
                'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
                Response.BinaryWrite(bytes)
        End Select
    End Sub
End Class