﻿'Imports Newtonsoft.Json
Public Class ReportEngagement
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtPt As New DataTable
    Dim dt As New DataTable
    Public Shared datachart As String
    Public Shared catebar As String
    Public Shared databar As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        dt = ctlR.RPT_Survey_Engagement(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

        datachart = dt.Rows(0)(0).ToString()   'JsonConvert.SerializeObject(dt, Formatting.None)

        dt = ctlR.RPT_Survey_EngagementByDivision(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

        catebar = ""
        databar = ""
        For i = 0 To dt.Rows.Count - 1
            catebar = catebar + "'" + dt.Rows(i)("DivisionName") + "'"
            databar = databar + dt.Rows(i)("Engm").ToString()

            If i < dt.Rows.Count - 1 Then
                catebar = catebar + ","
                databar = databar + ","
            End If
        Next
    End Sub

    Protected Sub cmdExport_Click(sender As Object, e As EventArgs) Handles cmdExport.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=seg&comp=" & Request.Cookies("SafetyCS")("LoginCompanyUID") & "&y=" & Request.Cookies("SafetyCS")("ASSMYEAR") & "&RPTTYPE=EXCEL','_blank');", True)
    End Sub
End Class