﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportEngagement2.aspx.vb" Inherits="SCS.ReportEngagement2" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
     
<script>
    $(document).ready(function() {
    $('#tbdata').DataTable( {
        "order": [[ 6, "desc" ]]
    } );
} );
 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-check icon-gradient bg-success"></i>
                        </div>
                        <div>Leadership Survey Engagement&nbsp; <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                            <div class="page-title-subheading">สถานะการประเมิน Leadership Survey Engagement  </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-star icon-gradient bg-success">
            </i>สถานะการประเมิน
            <div class="btn-actions-pane-right">                
            </div>
        </div>
        <div class="card-body">   
            <div class="table-responsive">
              <table id="tbdata" class="table table-hover table-striped table-borderless dataTable dtr-inline">
                <thead>
                <tr>
                  
                    <th>รหัส</th>
                  <th>ชื่อผู้ถูกประเมิน</th>
                  <th>ตำแหน่ง</th>
                      <th>แผนก</th>
                  <th>ฝ่าย</th>   
                      <th>ระดับ</th>   
                       <th>ที่ต้องประเมิน</th> 
                     <th>ประเมินแล้ว</th>   
                     <th>สถานะ</th>   
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtEmp.Rows %>
                <tr>              
                 <td><% =String.Concat(row("EmployeeCode")) %></td>
                 <td><% =String.Concat(row("EmployeeName")) %></td>
                 <td><% =String.Concat(row("PositionName")) %></td>
                 <td><% =String.Concat(row("DepartmentName")) %></td> 
                 <td><% =String.Concat(row("DivisionName")) %></td> 
                 <td><% =String.Concat(row("EmployeeLevelName")) %></td>
                 <td class="text-center"><% =String.Concat(row("AssessorCount")) %></td>
                 <td class="text-center"><% =String.Concat(row("AsmCount")) %> </td>
                 <td class="text-center"><% =IIf(Convert.ToInt32(row("AsmCount")) < Convert.ToInt32(row("AssessorCount")), "Pending", "Complete") %> </td>         
                </tr>
            <%  Next %>
                </tbody>               
              </table>                  </div>  
            <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdExport" CssClass="btn btn-primary" runat="server" Text="Export Data" Width="120px" /> 
        </div>
      </div>                 
            </div> 
          </div>
  
    </section>
</asp:Content>
