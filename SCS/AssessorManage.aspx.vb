﻿Public Class AssessorManage
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlA As New AssessorController
    Dim ctlU As New UserController
    'Dim ctlC As New CompanyController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            cmdDel.Visible = False
            LoadAssesseeDetail()
            LoadAssessorToDLL()
            LoadAssessorToGrid()

            If Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
                cmdDel.Visible = True
            Else
                cmdDel.Visible = False
            End If
        End If

        cmdDel.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบผู้ประเมินทั้งหมดใช่หรือไม่?"");")
    End Sub
    Private Sub LoadAssesseeDetail()
        Dim dtE As New DataTable
        Dim ctlP As New UserController
        dtE = ctlP.User_GetByUserID(Request("uid"))
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                hdAssesseeUID.Value = .Item("UserID")
                lblCode.Text = String.Concat(.Item("Code"))
                lblEmployeeName.Text = String.Concat(.Item("EmployeeName"))
                lblPositionName.Text = String.Concat(.Item("PositionName"))
                lblDepartmentName.Text = String.Concat(.Item("DepartmentName"))
                lblDivisionName.Text = String.Concat(.Item("DivisionName"))
            End With
        End If
    End Sub

    Private Sub LoadAssessorToDLL()
        ddlAssessor.DataSource = ctlU.User_GetForAssessor(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), StrNull2Zero(hdAssesseeUID.Value))
        ddlAssessor.DataTextField = "Name"
        ddlAssessor.DataValueField = "UserID"
        ddlAssessor.DataBind()
    End Sub

    Protected Sub cmdAddAssessor_Click(sender As Object, e As EventArgs) Handles cmdAddAssessor.Click
        If ddlAssessor.SelectedValue Is Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกผู้ประเมินก่อน');", True)
            Exit Sub
        End If

        '--------------Add Assessor-----------------
        ctlA.Assessor_Save(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(hdAssesseeUID.Value), StrNull2Zero(ddlAssessor.SelectedValue), "3", Request.Cookies("SafetyCS")("UserID"))
        '-----------------End Add -----------
        ctlU.User_GenLogfile(Request.Cookies("SafetyCS")("username"), ACTTYPE_UPD, "Assessor", "เพิ่ม Assessor Asignment :{AssesseeUID=" & hdAssesseeUID.Value & "}{AssessorUID=" & ddlAssessor.SelectedValue & "}", "")

        LoadAssessorToGrid()
    End Sub

    Private Sub LoadAssessorToGrid()
        Dim dtA As New DataTable
        dtA = ctlA.Assessor_GetByAssesseeUID(StrNull2Zero(hdAssesseeUID.Value))
        If dtA.Rows.Count > 0 Then
            With grdAssessor
                .Visible = True
                .DataSource = dtA
                .DataBind()
            End With
            cmdDel.Visible = True
        Else
            grdAssessor.DataSource = Nothing
            grdAssessor.Visible = False
            cmdDel.Visible = False
        End If
        dtA = Nothing
    End Sub
    Protected Sub grdAssessor_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAssessor.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    If ctlA.Assessor_Delete(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("SafetyCS")("username"), "DEL", "Assessor", "ลบ Assessor Assessor :" & e.CommandArgument, "")
                        LoadAssessorToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub

    Protected Sub cmdDel_Click(sender As Object, e As EventArgs) Handles cmdDel.Click
        ctlA.Assessor_DeleteByAssessee(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(hdAssesseeUID.Value))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','ลบข้อมูลทั้งหมดเรียบร้อย');", True)
        LoadAssessorToGrid()
    End Sub
End Class