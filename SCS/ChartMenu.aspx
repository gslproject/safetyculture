﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ChartMenu.aspx.vb" Inherits="SCS.ChartMenu" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-medal icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Safety Culture Survey Anylysis Report
                                    <div class="page-title-subheading">เลือกปีสำหรับดูรายงาน</div>
                                </div>
                            </div>
                        </div>
</div>
 

<section class="content">        
     <div class="row">
      <% For Each row As DataRow In dtYear.Rows %>
            <div class="col-lg-3 col-xs-3">
           <a  href="<% If String.Concat(row("AsmYear")) <> 0 Then %>Chart2<% Else %>Chart2All<% End If %>.aspx?m=ra&y=<% =String.Concat(row("AsmYear")) %>" class="menu-banner" >     
          <div class="small-box <% If String.Concat(row("AsmYear")) <> 0 Then %>bg-blue<% Else %> bg-green <% End If %>">
            <div class="inner text-center">
              <h3><% =String.Concat(row("YearName")) %></h3> 
            </div>            
          </div>
               </a>
        </div>
                                  
            <%  Next %>                 
      
         </div>
     

    </section>
</asp:Content>
