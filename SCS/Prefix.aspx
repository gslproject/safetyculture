﻿<%@ Page Title="คำนำหน้าชื่อ" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Prefix.aspx.vb" Inherits="iIncident.Prefix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-config icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   
<section class="content">  

         <div class="box box-primary">

            <div class="box-header">
              <i class="fa fa-grear"></i>

              <h3 class="box-title">จัดการคำนำหน้าชื่อ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 


                
      <div class="row">
   <div class="col-md-1">
          <div class="form-group">
            <label>UID</label>
              <asp:TextBox ID="txtUID" runat="server" cssclass="form-control text-center"  ReadOnly="True"></asp:TextBox>
          </div>

        </div>
    <div class="col-md-3">
          <div class="form-group">
            <label>Name1</label>
              <asp:TextBox ID="txtName1" runat="server" cssclass="form-control" placeholder="คำนำหน้าชื่อ (ไทย)"></asp:TextBox>
          </div>

        </div>
           <div class="col-md-3">
          <div class="form-group">
            <label>Name2</label>
              <asp:TextBox ID="txtName2" runat="server" cssclass="form-control text-center" placeholder="คำนำหน้าชื่อ (ภาษาอังกฤษ)"></asp:TextBox>
          </div>

        </div>
          
              <div class="col-md-3">
          <div class="form-group">
            <label>Name3</label>
              <asp:TextBox ID="txtName3" runat="server" cssclass="form-control text-center" placeholder="คำย่อ หรือชื่ออื่นๆ"></asp:TextBox>
          </div>
        </div>
 
             <div class="col-md-2">
          <div class="form-group">
            <label>Status</label>
              <br />
              <asp:CheckBox ID="chkStatus" runat="server" Text="Active" 
                                                            Checked="True" />
          </div>

        </div>
     
      
      </div>

  <div class="row">
   <div class="col-md-12 text-center">
               <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Width="100" Text="บันทึก"></asp:Button>
    &nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="btn btn-default" Width="100" Text="ยกเลิก"></asp:Button> 
       </div>
      </div>

</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-share-alt"></i>

              <h3 class="box-title">รายชื่อคำนำหน้าชื่อ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">    
              
              <table border="0"  >
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" cssclass="form-control"  Width="200px"></asp:TextBox>                </td>
              <td >
                   &nbsp;<asp:Button ID="cmdFind" runat="server" CssClass="btn btn-success" Width="70" Text="ค้นหา"></asp:Button>              
                </td>
            </tr>
           
          </table>      

        
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="20" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="ID" DataField="UID">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Name1">
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
<asp:BoundField DataField="Name2" HeaderText="Name2">
                <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
                <asp:BoundField DataField="AliasName" HeaderText="Name3">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            <asp:TemplateField HeaderText="Active">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# iIncident.ConvertStatusFlag2CHK(DataBinder.Eval(Container.DataItem, "StatusFlag")) %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True"                  VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
 
                                                 
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>
                           
</section>   
</asp:Content>
