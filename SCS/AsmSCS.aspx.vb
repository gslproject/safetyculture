﻿Public Class AsmSCS
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlA As New AssessmentController
    Dim ctlP As New UserController
    'Dim ctlM As New MasterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadPerson()
            'LoadTaskData()
            'optAsmType.SelectedValue = Session("asmtype")
            'txtAsmDate.Text = Today.Date.ToShortDateString()

            'If Session("asmtype") = "O" Then
            '    ddlPerson.Enabled = True
            'Else
            '    ddlPerson.SelectedValue = Request.Cookies("SafetyCS")("LoginPersonUID")
            '    ddlPerson.Enabled = False
            'End If


            If Not Request("id") Is Nothing Then
                LoadAssessmentData()
            End If
        End If

        'cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub
    Private Sub LoadAssessmentData()
        'dt = ctlA.Assessment_GetByUID("ROSA", Request("id"))

        'If dt.Rows.Count > 0 Then
        '    With dt.Rows(0)
        '        hdUserID.Value = .Item("UID")
        '        'txtTaskNo.Text = .Item("TaskNo")
        '        'optAsmType.SelectedValue = .Item("AsmType")
        '        'ddlPerson.SelectedValue = .Item("PersonUID")
        '        'txtAsmDate.Text = .Item("AsmDate")
        '        txtRemark.Text = .Item("Remark")
        '        ddlUseMonitor.SelectedValue = .Item("MonitorUse")
        '        ddlUsePhone.SelectedValue = .Item("PhoneUse")
        '        ddlUseKeyboard.SelectedValue = .Item("KeyboardUse")
        '        ddlUseMouse.Text = .Item("MouseUse")
        '        optRosa1.SelectedValue = .Item("S1")
        '        optRosa2.SelectedValue = .Item("S2")
        '        optRosa3.SelectedValue = .Item("S3")
        '        optRosa4.SelectedValue = .Item("S4")
        '        optRosa5.SelectedValue = .Item("S5")
        '        optRosa6.SelectedValue = .Item("S6")
        '        optRosa7.SelectedValue = .Item("S7")
        '        optRosa8.SelectedValue = .Item("S8")

        '        'S1A = S2A = S3A, S4A, S5A, S6A, S7A, S8A

        '        Dim str1(), str2(), str3(), str4(), str5(), str6(), str7(), str8() As String

        '        str1 = Split(String.Concat(.Item("S1A")), "|")
        '        For i = 0 To str1.Length - 1
        '            For n = 0 To chkRosaAdd1.Items.Count - 1
        '                If str1(i) = chkRosaAdd1.Items(n).Value Then
        '                    chkRosaAdd1.Items(n).Selected = True
        '                End If
        '            Next
        '        Next


        '        str2 = Split(String.Concat(.Item("S2A")), "|")
        '        For i = 0 To str2.Length - 1
        '            For n = 0 To chkRosaAdd2.Items.Count - 1
        '                If str2(i) = chkRosaAdd2.Items(n).Value Then
        '                    chkRosaAdd2.Items(n).Selected = True
        '                End If
        '            Next
        '        Next


        '        str3 = Split(String.Concat(.Item("S3A")), "|")
        '        For i = 0 To str3.Length - 1
        '            For n = 0 To chkRosaAdd3.Items.Count - 1
        '                If str3(i) = chkRosaAdd3.Items(n).Value Then
        '                    chkRosaAdd3.Items(n).Selected = True
        '                End If
        '            Next
        '        Next

        '        str4 = Split(String.Concat(.Item("S4A")), "|")
        '        For i = 0 To str4.Length - 1
        '            For n = 0 To chkRosaAdd4.Items.Count - 1
        '                If str4(i) = chkRosaAdd4.Items(n).Value Then
        '                    chkRosaAdd4.Items(n).Selected = True
        '                End If
        '            Next
        '        Next

        '        str5 = Split(String.Concat(.Item("S5A")), "|")
        '        For i = 0 To str5.Length - 1
        '            For n = 0 To chkRosaAdd5.Items.Count - 1
        '                If str5(i) = chkRosaAdd5.Items(n).Value Then
        '                    chkRosaAdd5.Items(n).Selected = True
        '                End If
        '            Next
        '        Next

        '        str6 = Split(String.Concat(.Item("S6A")), "|")
        '        For i = 0 To str6.Length - 1
        '            For n = 0 To chkRosaAdd6.Items.Count - 1
        '                If str6(i) = chkRosaAdd6.Items(n).Value Then
        '                    chkRosaAdd6.Items(n).Selected = True
        '                End If
        '            Next
        '        Next

        '        str7 = Split(String.Concat(.Item("S7A")), "|")
        '        For i = 0 To str7.Length - 1
        '            For n = 0 To chkRosaAdd7.Items.Count - 1
        '                If str7(i) = chkRosaAdd7.Items(n).Value Then
        '                    chkRosaAdd7.Items(n).Selected = True
        '                End If
        '            Next
        '        Next

        '        str8 = Split(String.Concat(.Item("S8A")), "|")
        '        For i = 0 To str8.Length - 1
        '            For n = 0 To chkRosaAdd8.Items.Count - 1
        '                If str8(i) = chkRosaAdd8.Items(n).Value Then
        '                    chkRosaAdd8.Items(n).Selected = True
        '                End If
        '            Next
        '        Next




        '        lblFinalScore.Text = .Item("FinalScore")
        '        lblFinalResult.Text = .Item("ResultText")
        '    End With
        'End If


    End Sub
    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlP.User_GetByUserID(Request.Cookies("SafetyCS")("LoginPersonUID"))
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                hdUserID.Value = .Item("UserID")
                lblCode.Text = String.Concat(.Item("Code"))
                lblEmployeeName.Text = String.Concat(.Item("EmployeeName"))
                lblPositionName.Text = String.Concat(.Item("PositionName"))
                lblDepartmentName.Text = String.Concat(.Item("DepartmentName"))
                lblDivisionName.Text = String.Concat(.Item("DivisionName"))
            End With
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click


        Dim Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21 As Integer

        Q1 = StrNull2Zero(optQ1.SelectedValue)
        Q2 = StrNull2Zero(optQ2.SelectedValue)
        Q3 = StrNull2Zero(optQ3.SelectedValue)
        Q4 = StrNull2Zero(optQ4.SelectedValue)
        Q5 = StrNull2Zero(optQ5.SelectedValue)
        Q6 = StrNull2Zero(optQ6.SelectedValue)
        Q7 = StrNull2Zero(optQ7.SelectedValue)
        Q8 = StrNull2Zero(optQ8.SelectedValue)
        Q9 = StrNull2Zero(optQ9.SelectedValue)
        Q10 = StrNull2Zero(optQ10.SelectedValue)
        Q11 = StrNull2Zero(optQ11.SelectedValue)
        Q12 = StrNull2Zero(optQ12.SelectedValue)
        Q13 = StrNull2Zero(optQ13.SelectedValue)
        Q14 = StrNull2Zero(optQ14.SelectedValue)
        Q15 = StrNull2Zero(optQ15.SelectedValue)
        Q16 = StrNull2Zero(optQ16.SelectedValue)
        Q17 = StrNull2Zero(optQ17.SelectedValue)
        Q18 = StrNull2Zero(optQ18.SelectedValue)
        Q19 = StrNull2Zero(optQ19.SelectedValue)
        Q20 = StrNull2Zero(optQ20.SelectedValue)
        Q21 = StrNull2Zero(optQ21.SelectedValue)

        If Q1 = 0 Or Q2 = 0 Or Q3 = 0 Or Q4 = 0 Or Q5 = 0 Or Q6 = 0 Or Q7 = 0 Or Q8 = 0 Or Q9 = 0 Or Q10 = 0 Or Q11 = 0 Or Q12 = 0 Or Q13 = 0 Or Q14 = 0 Or Q15 = 0 Or Q16 = 0 Or Q17 = 0 Or Q18 = 0 Or Q19 = 0 Or Q20 = 0 Or Q21 = 0 Then

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาทำแบบประเมินให้ครบทุกข้อ');", True)
            Exit Sub
        End If

        'CalculateScore()

        'Dim AsmDate As String
        'AsmDate = ctlA.GET_DATETIME_SERVER

        ctlA.Assessment_Save(StrNull2Zero(hdUserID.Value), Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, txtComment1.Text, txtComment2.Text, Request.Cookies("SafetyCS")("userid"))

        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("SafetyCS")("userid"), ACTTYPE_UPD, "Assessment", "Safety Culture Survey", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("AsmResult.aspx")
    End Sub

    Private Sub CalculateScore()
        Dim sc1, sc2, sc3, sc4, sc5, sc6, sc7, sc8 As Integer

        'sc1 = ctlA.AssessmentAnswer_GetScore("ROSA", 1, optRosa1.SelectedValue)
        'sc2 = ctlA.AssessmentAnswer_GetScore("ROSA", 2, optRosa2.SelectedValue)
        'sc3 = ctlA.AssessmentAnswer_GetScore("ROSA", 3, optRosa3.SelectedValue)
        'sc4 = ctlA.AssessmentAnswer_GetScore("ROSA", 4, optRosa4.SelectedValue)
        'sc5 = ctlA.AssessmentAnswer_GetScore("ROSA", 5, optRosa5.SelectedValue)
        'sc6 = ctlA.AssessmentAnswer_GetScore("ROSA", 6, optRosa6.SelectedValue)
        'sc7 = ctlA.AssessmentAnswer_GetScore("ROSA", 7, optRosa7.SelectedValue)
        'sc8 = ctlA.AssessmentAnswer_GetScore("ROSA", 8, optRosa8.SelectedValue)

        ''step1 ความสูงของเก้าอี้
        'For i = 0 To chkRosaAdd1.Items.Count - 1
        '    If chkRosaAdd1.Items(i).Selected Then
        '        sc1 = sc1 + ctlA.AssessmentAnswer_GetScore("ROSA", 1, chkRosaAdd1.Items(i).Value)
        '    End If
        'Next

        'If sc1 > 5 Then 'ความสูง ไม่เกิน 5
        '    sc1 = 5
        'End If

        ''step2 ความลึกเก้าอี้
        'For i = 0 To chkRosaAdd2.Items.Count - 1
        '    If chkRosaAdd2.Items(i).Selected Then
        '        sc2 = sc2 + ctlA.AssessmentAnswer_GetScore("ROSA", 2, chkRosaAdd2.Items(i).Value)
        '    End If
        'Next
        'If sc2 > 3 Then 'ความลึก ไม่เกิน 3
        '    sc2 = 3
        'End If

        ''step3 ที่พักแขน
        'For i = 0 To chkRosaAdd3.Items.Count - 1
        '    If chkRosaAdd3.Items(i).Selected Then
        '        sc3 = sc3 + ctlA.AssessmentAnswer_GetScore("ROSA", 3, chkRosaAdd3.Items(i).Value)
        '    End If
        'Next
        'If sc3 > 5 Then 'ความลึก ไม่เกิน 5
        '    sc3 = 5
        'End If

        ''step4 พนักพิง
        'For i = 0 To chkRosaAdd4.Items.Count - 1
        '    If chkRosaAdd4.Items(i).Selected Then
        '        sc4 = sc4 + ctlA.AssessmentAnswer_GetScore("ROSA", 4, chkRosaAdd4.Items(i).Value)
        '    End If
        'Next
        'If sc4 > 4 Then 'ไม่เกิน 4
        '    sc4 = 4
        'End If



        ''step5 หน้าจอ
        'For i = 0 To chkRosaAdd5.Items.Count - 1
        '    If chkRosaAdd5.Items(i).Selected Then
        '        sc5 = sc5 + ctlA.AssessmentAnswer_GetScore("ROSA", 5, chkRosaAdd5.Items(i).Value)
        '    End If
        'Next
        'If sc5 > 6 Then 'ไม่เกิน 6
        '    sc5 = 6
        'End If


        ''step6 โทรศัพท์
        'For i = 0 To chkRosaAdd6.Items.Count - 1
        '    If chkRosaAdd6.Items(i).Selected Then
        '        sc6 = sc6 + ctlA.AssessmentAnswer_GetScore("ROSA", 6, chkRosaAdd6.Items(i).Value)
        '    End If
        'Next
        'If sc6 > 5 Then 'ไม่เกิน 5
        '    sc6 = 5
        'End If

        ''step7 เมาส์
        'For i = 0 To chkRosaAdd7.Items.Count - 1
        '    If chkRosaAdd7.Items(i).Selected Then
        '        sc7 = sc7 + ctlA.AssessmentAnswer_GetScore("ROSA", 7, chkRosaAdd7.Items(i).Value)
        '    End If
        'Next
        'If sc7 > 6 Then 'ไม่เกิน 6
        '    sc7 = 6
        'End If

        ''step8 คีย์บอร์ด ไม่เกิน 6
        'For i = 0 To chkRosaAdd8.Items.Count - 1
        '    If chkRosaAdd8.Items(i).Selected Then
        '        sc8 = sc8 + ctlA.AssessmentAnswer_GetScore("ROSA", 8, chkRosaAdd8.Items(i).Value)
        '    End If
        'Next
        'If sc8 > 6 Then 'ไม่เกิน 6
        '    sc8 = 6
        'End If



        'Dim ScoreA, ScoreB, ScoreC, ScoreD, ScoreFinal As Integer
        ''Step9 ตาราง A หาคะแนนเก้าอี้ (สูง+ลึก,ที่พักแขน+พนักพิง) 
        'ScoreA = ctlA.ScoreROSA_GetScore("A", (sc1 + sc2), (sc3 + sc4))
        ''Step10 นำค่าคะแนนโทรศัพท์และคะแนนจอภาพมาอ่านค่าคะแนนในตาราง B
        'ScoreB = ctlA.ScoreROSA_GetScore("B", sc6 + CInt(ddlUsePhone.SelectedValue), sc5 + CInt(ddlUseMonitor.SelectedValue))
        ''Step11 และนำค่าคะแนนเมาส์และคะแนนแป้นพิมพ์มาอ่านค่าคะแนน ในตาราง C
        'ScoreC = ctlA.ScoreROSA_GetScore("C", sc7 + CInt(ddlUseMouse.SelectedValue), sc8 + CInt(ddlUseKeyboard.SelectedValue))
        ''Step12 นำคะแนนประเมินโทรศัพท์และจอภาพ (คะแนน B) และคะแนนประเมินเมาส์และแป้นพิมพ์ (คะแนน C) มาอ่านค่าคะแนนใน ตาราง D 
        'ScoreD = ctlA.ScoreROSA_GetScore("D", ScoreB, ScoreC)
        ''Step13 การหาค่าคะแนนรวมและการสรุปผล ROSA
        'ScoreFinal = ctlA.ScoreROSA_GetScore("AD", ScoreA, ScoreD)
        ''5 monitor
        ''6 Phone
        ''7 mouse
        ''8 keyboard
        'lblFinalScore.Text = ScoreFinal.ToString()

        'If ScoreFinal <= 2 Then
        '    lblFinalResult.Text = "ระดับ 1 ความเสี่ยงต่ำ"
        'ElseIf ScoreFinal >= 3 And ScoreFinal <= 4 Then
        '    lblFinalResult.Text = "ระดับ 2 ความปานกลาง"
        'ElseIf ScoreFinal >= 5 And ScoreFinal <= 7 Then
        '    lblFinalResult.Text = "ระดับ 3 ความเสี่ยงสูง"
        'Else
        '    lblFinalResult.Text = "ระดับ 4 ความเสี่ยงสูงมาก"
        'End If

        'If ScoreFinal < 5 Then
        '    lblFinalResult.Text = lblFinalResult.Text & " ยังไม่จำเป็นต้องมีการประเมิน หรือศึกษาเพิ่มเติม"
        'Else
        '    lblFinalResult.Text = lblFinalResult.Text & " จำเป็นต้องมีการประเมิน หรือศึกษาเพิ่มเติมทันที"
        'End If
        ''ค่าคะแนน ROSA สามารถสรุปผลการประเมินได้ 2 ลักษณะ ดังนี้
        ''- คะแนนน้อยกว่า 5 คะแนน หมายถึงยังไม่จำเป็นต้องมีการประเมิน หรือศึกษาเพิ่มเติม
        ''- คะแนนตั้งแต่ 5 คะแนนขึ้นไป หมายถึง จำเป็นต้องมีการประเมิน หรือศึกษาเพิ่มเติมทันที
    End Sub

    'Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click

    '    If Not Request("ActionType") Is Nothing And Request("ActionType") = "agrpt" Then
    '        Response.Redirect("Evaluated.aspx?ActionType=agrpt&pid=" & Request.Cookies("SafetyCS")("LoginPersonUID"))
    '    Else
    '        Response.Redirect("Evaluated.aspx?ActionType=agrpt&pid=" & Request("id"))
    '    End If

    'End Sub

    'Protected Sub optRosa1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa1.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd1.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa2.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa3.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd3.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa4.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa5.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa6.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa7.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub optRosa8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRosa8.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd2.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd4.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd5.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd6.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd7.SelectedIndexChanged
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRosaAdd8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRosaAdd8.SelectedIndexChanged
    '    CalculateScore()
    'End Sub


End Class