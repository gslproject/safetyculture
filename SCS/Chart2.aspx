﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Chart2.aspx.vb" Inherits="SCS.Chart2" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>
    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
   <section class="content-header">
    <h1>Safety Culture Survey Analysis <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
      <small></small>
    </h1>  
  </section>

  <!-- Main content -->
  <section class="content">  
          <div class="row"> 
      <section class="col-lg-6 connectedSortable">
          <div class="box box-solid">  
    <div class="box-body">             
                  <div id="chart2"></div>                 
              </div>
          </div>
</section>   
         <section class="col-lg-6 connectedSortable">
               <div class="box box-solid">  
                    <div class="box-header">
              <i class="fa fa-comment"></i>
              <h3 class="box-title">คำอธิบาย</h3>             
                                
            </div>
    <div class="box-body">  
               <table id="dataScore" class="table table-striped table-borderless">
                <thead>
                <tr>                  
                   <th class="text-center">Code</th>
                  <th class="text-center">Desciption</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtItem.Rows %>
                    <tr>        
                        <td class="text-center"><% =String.Concat(row("EvaluationGroup")) %></td>
                        <td><% =String.Concat(row("EvaluationGroupName")) %></td>                         
                </tr>
            <%  Next %>
                </tbody>               
              </table>     
         </div>
          </div>
             </section>
        </div> 
    <div class="row"> 
      <section class="col-lg-12 connectedSortable">

          <div class="box box-solid">  
    <div class="box-body">             
                  <div id="chart"></div>                 
              </div>
          </div>
</section>   
      
        </div>    
          <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdExport" CssClass="btn btn-primary" runat="server" Text="Export Data" Width="120px" /> 
        </div>
      </div>
</section>
    <script>
      
        var options = {
          series: [{
          name: '',
          data: [<%=databar1 %>]
        }],
          chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            borderRadius: 10,
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ["#304758"]
          }
        },
        
        xaxis: {
          categories: [<%=catebar1 %>],
          position: 'bottom',
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: true
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false,
            },
          min: 0,
      max: 5,  
          labels: {
            show: true,
            formatter: function (val) {
              return val ;
            }
          }
        
        },
        title: {
          text: '',
          show: false,
          position:'top',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>  
    <script>
      
        var options2 = {
          series: [{
          name: '',
          data: [<%=databar2 %>],
        }],
          chart: {
          height: 350,
          type: 'radar',
        },
        title: {
          text: ''
            },
          dataLabels: {
              enabled: true,
              
          formatter: function (val) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ["#00f"]
          }
        },
        xaxis: {
          categories: [<%=catebar2 %>]
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
        chart2.render();
      
      
    </script>
                
</asp:Content>