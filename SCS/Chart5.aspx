﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Chart5.aspx.vb" Inherits="Ergonomic.Chart5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />
   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
   <section class="content-header">
    <h1>การรับรู้อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ (MSDs) 
      <small></small>
    </h1>
  
  </section>

  <!-- Main content -->
  <section class="content">   
    <div class="row"> 
      <section class="col-lg-3 connectedSortable">
          <div class="box box-solid">
    <div class="box-header"> 
    </div> 
    <div class="box-body text-center">             
        <img src="images/body.png">
              </div>
          </div>
</section>   
        <section class="col-lg-9 connectedSortable">
             <div class="box box-solid">
    <div class="box-header">
 <h5 class="card-title">การรับรู้อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ (MSDs) <asp:Label ID="Label1" runat="server" Text="(n=)"></asp:Label></h5>
    </div> 
    <div class="box-body">      
  <div id="chart"></div>
              </div>
          </div>

            </section>
        </div>  
    
</section>
            
     <script>
      
        var options = {
            series: [{
              name: 'ขวา',
          data: [<%=databar1R %>]
            }, {
                    name: 'ซ้าย',
          data: [<%=databar1L %>]
                }],
          chart: {
          type: 'bar',
          height: 400
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: 12,
          style: {
            fontSize: '12px',
            colors: ['#000']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: [<%=catebar1 %>],
            },
        yaxis: {
    labels: { 
      formatter: function(val) {
        return val;
      }
    }
  }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>
        
</asp:Content>