﻿<%@ Page Title="Employee" MetaDescription="จัดการข้อมูลพนักงาน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SafetyLeadershipReport.aspx.vb" Inherits="SCS.SafetyLeadershipReport" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>
    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-success"></i>
            </div>
            <div>Safety Leadership Survey
                    <div class="page-title-subheading">
                   
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">  
       <div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-user-circle"></i>
              <h3 class="box-title">ข้อมูลพนักงาน</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>รหัสพนักงาน</label><asp:HiddenField ID="hdEmpUID" runat="server" />
              <asp:label ID="lblCode" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อ-นามสกุล</label>
              <asp:label ID="lblEmployeeName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>ตำแหน่ง</label>
              <asp:label ID="lblPositionName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
              <asp:label ID="lblDepartmentName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
              <asp:label ID="lblDivisionName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
      </div> 
         
    </div>
    <div class="box-footer clearfix">  
        ผู้ประเมิน <asp:Label ID="lblAsmCount" runat="server" cssclass="text-blue no-border" Text=""></asp:Label>
            </div>
  </div>
</section>
           </div>
<div class="row">
   <section class="col-lg-5 connectedSortable">
          <div class="main-card mb-3 card">
    <div class="card-header">
        <i class="header-icon lnr-list icon-gradient bg-primary">
            </i>Overall Score<asp:HiddenField ID="hdCompUID" runat="server" />
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
        <table id="dataScore" class="table table-hover table-striped table-borderless dataTable dtr-inline">
                <thead>
                <tr>                  
                   <th class="text-center">Item</th>
                  <th class="text-center">Score</th>
                  <th class="text-center">Status</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtScore.Rows %>
                    <tr>              
                  <td><% =String.Concat(row("Descriptions")) %></td>
                  <td class="text-center"><% =String.Concat(row("AvgScore")) %></td>
                  <td class="text-center" bgcolor="<% =String.Concat(row("ColorStatus")) %>" ><% =String.Concat(row("AsmStatus")) %></td>                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                        
                           
    </div>
  </div>
  </section>
     <section class="col-lg-7 connectedSortable">
     <div id="chart"></div>         
  </section>

</div>   
            <div class="row">
             <div class="col-md-4">       
          <div class="box box-primary">
       <div class="box-header">
              <h6 class="box-title">1.Sets The Standard And Expectations</h6>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>                 
            </div>
    <div class="box-body">         
              <p>ผู้ถูกประเมินมีลักษณะปฏิบัติตามนโยบาย ขั้นตอนการปฏิบัติงาน และแนวทางทางด้านความปลอดภัยอย่างสม่ำเสมอ แสดงออกถึงการดูแลคนที่ทำงานด้วยกันอย่างจริงใจ มุ่งมั่นตั้งใจที่จะปรับปรุงประสิทธิภาพการทำงานให้ปลอดภัยอย่างต่อเนื่องเพื่อให้ทุกคนสามารถกลับบ้านได้อย่างปลอดภัยในทุกวัน</p> 
          </div>
      <div class="box-footer text-center clearfix">  
             Average Score <asp:Label ID="lblQ1" runat="server" CssClass="text-blue text-bold"></asp:Label>          
            </div>  
        </div>
</div>
             <div class="col-md-4">       
             <div class="box box-primary">
       <div class="box-header"><h6 class="box-title">2.Walk The Talk And Role Models</h6>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>                 
            </div>
    <div class="box-body">         
              <p>ผู้ถูกประเมินมีลักษณะ มักจะค้นหา/ตรวจสอบและพูดคุยเกี่ยวกับเรื่องความปลอดภัยเสมอ มีส่วนร่วมอย่างแข็งขันในการประชุมและริเริ่มการปรับปรุงในเรื่องของความปลอดภัย และแสดงออกเป็นตัวอย่างในเรื่องความปลอดภัยตามที่มาตรฐานกำหนด</p><br />
            </div>          
         <div class="box-footer text-center clearfix">  
                   Average Score <asp:Label ID="lblQ2" runat="server" CssClass="text-blue text-bold"></asp:Label>
         </div>
          </div>
        </div>
                 <div class="col-md-4">       
        <div class="box box-primary">
       <div class="box-header">
              <h6 class="box-title">3.Visibility</h6>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>                 
            </div>
    <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะของการเดินในพื้นที่ปฏิบัติงานอย่างสม่ำเสมอเพื่อทำการตรวจสอบพฤติกรรมที่เกี่ยวข้องกับความปลอดภัยรวมถึงการพูดคุยเกี่ยวกับประเด็นความปลอดภัยและโอกาสที่จะพัฒนาในพื้นที่ เป็นผู้นำให้สมาชิกในทีมปฏิบัติตามขั้นตอนทำงานที่ปลอดภัย และแสดงให้เห็นถึงความเอาใจใส่อย่างแท้จริงกับพนักงาน
</p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ3" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">4.Challenges</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะที่มีความสามารถในการแก้ใขปัญหา หรือเข้าแทรกแซงเกี่ยวกับการแก้ปัญหาเรื่องความปลอดภัย เห็นความท้าทายเป็นเรื่องทางบวกและมีความคิดสร้างสรรค์ มีความรับผิดชอบในการตัดสินใจและเป็นผู้นำในการพูดคุยแก้ไขปัญหาเกี่ยวกับความปลอดภัย<br /><br /><br /></p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ4" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">5.Empower Others</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะ มีบุคลิคลักษณะชัดเจนเกี่ยวกับมาตรฐานและสิ่งที่ต้องการในด้านเป้าหมายความปลอดภัย ให้การสนับสนุนแก่สมาชิกในทีมเพื่อช่วยให้พวกเขามีมาตรฐานและเป็นไปตามเป้าหมายด้านความปลอดภัย ผลักดันให้สมาชิกในทีมในการริเริ่มการปรับปรุงด้านความปลอดภัย สร้างให้สมาชิกในทีมรู้สึกสบายใจ มั่นใจในการหยุดสิ่งที่พวกเขาเห็นว่าไม่ปลอดภัย</p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ5" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">6.Communicates</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะ มีการให้ Feedback แก่สมาชิกในทีมอย่างสม่ำเสมอเกี่ยวกับผลงานด้านความปลอดภัย มีการวางแผนอย่างชัดเจนในการพูดคุยเรื่องความปลอดภัย เป้าหมายทางด้านความปลอดภัยมีการสื่อสารชัดเจนในสถานที่ทำงาน เช่น เป็นบอร์ดสื่อสาร เป็นต้น และเป็นข้อมูลอัพเดทตลอดเวลา<br /><br /></p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ6" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">7.Coaches</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะ ให้คำแนะนำทั้งในทางบวกและในกรณีที่ต้องการการแก้ไข มีทำการออดิท (Audit) และทำ SWO อย่างสม่ำเสมอร่วมกับสมาชิกในทีม เป็นผู้ฟังที่ดี ถามคำถามปลายเปิด (เพื่อต้องการความคิดเห็น) ทำให้คนคิดวิธีการแก้ไขปัญหาต่างๆด้วยตนเองมากกว่าการแนะนำวิธีการแก้ไข<br /><br /></p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ7" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">8.Promotes Improvements And Sharing</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะสามารถวิเคราะห์และแบ่งปันข้อมูลที่เรียนรู้จากการรายงาน Near-miss และ การทำออดิท (Safety Audit) รวมถีงข้อมูลอุบัติการณ์ที่เกิดขึ้นได้ถูกทบทวนและสื่อสารให้กับสมาชิกในทีมได้รับทราบ เรียนรู้ประเด็นปัญหาด้านความปลอดภัยในพื้นที่อื่นๆ จัดให้มีการประชุมความปลอดภัยในทุกเดือนๆหรือต้องนำเรื่องความปลอดภัยเป็นส่วนหนึ่งของการประชุมหน่วยงานอย่างสม่ำเสมอ (ทำด้วยตนเอง)</p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ8" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div>
                 <div class="col-md-4">       
          <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">9.Articulates The Vision For Safety</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>ผู้ถูกประเมินมีลักษณะที่สามารถบอกได้อย่างชัดเจนว่า "อุบัติเหตุเป็นศูนย์" Zero Accident เป็นสิ่งที่เป็นไปได้ และความปลอดภัยที่ดี คือ การดำเนินธุรกิจที่ดีด้วย มีการส่งเสริมให้คนเอาใจใส่ตนเอง ครอบครัวและเพื่อนร่วมงานและเป็นสิ่งที่มีค่าขององค์กร เข้าร่วมการประชุมต่างๆที่เกี่ยวข้องกับวิสัยทัศน์ด้านความปลอดภัย กิจกรรมความปลอดภัย ระบบความปลอดภัยและขั้นตอนต่างๆด้านความปลอดภัย เพื่อเพิ่มความเข้าใจ</p>
            </div>          
               <div class="box-footer text-center clearfix">
                   Average Score <asp:Label ID="lblQ9" runat="server" CssClass="text-blue text-bold"></asp:Label></div>
          </div>
        </div> 

                  <div class="col-md-12">       
         <div  class="box box-primary">
                <div class="box-header"><h6 class="box-title">10.FEEDBACKS/ COMMENT</h6><div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button> 
              </div>    </div>
            <div class="box-body">           
              <p>
                  <asp:Label ID="lblComment" runat="server" Text=""></asp:Label></p>
            </div>          
               <div class="box-footer clearfix">

               </div>
          </div>
        </div> 

                     </div>
    
  <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdPrint" CssClass="btn btn-primary" runat="server" Text="Print" Width="120px" /> 
        </div>
      </div>

        </section>
    
    <script>
      
        var options = {
          series: [{
          name: '',
          data: [<%=databar %>],
        }],
          chart: {
          height: 460,
          type: 'radar',
        },
        title: {
          text: 'Safety Leadership Score'
            },
          dataLabels: {
              enabled: true,
              
          formatter: function (val) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '9px',
            colors: ["#00f"]
          }
            },           
        xaxis: {
            categories: [<%=catebar %>],
             labels: {
    show: true,
    style: {
      colors: ["#a8a8a8"],
      fontSize: "9px"
    }
  }
            },
        
    plotOptions: {
      radar: {
        polygons: {
          strokeColor: '#e8e8e8',
          fill: {
              colors: colors = ['#00E396','#bcf2d8','#FEB019','#FF4560','#c40205']
          }
        }
      }
    }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>
</asp:Content>
