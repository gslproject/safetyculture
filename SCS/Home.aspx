﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="SCS.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    
    <link href="assets/styles.css" rel="stylesheet" />

    <style>
      
        #chart {
      padding: 0;
      max-width: 380px;
      margin: 0px auto;
    }
        #chart2 {
       max-width: 100%;
      margin: 0px auto;
    }
      
    </style>

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-light icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Dashboard
                                    <div class="page-title-subheading">Safety Culture Survey</div>
                                </div>
                            </div>
                        </div>
                    </div>

  <!-- Main content -->
  <section class="content">  
      <div class="row">
              <section class="col-lg-6 connectedSortable">
                    <div class="mb-3 card">
            <div class="card-header"><i class="header-icon lnr-pie-chart icon-gradient bg-success">
                </i>Survey Engagement
                <div class="btn-actions-pane-right">                
                </div>
            </div> 
            <div class="card-body"> 
       <div class="row">  
          <div class="col-md-12">             
              <div id="chart"></div>
          </div>
      </div>
                </div>
              </div>
                  </section>
     
        <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
          </div>
        </div>
      </section>
       </div>
</section>
        
   
    <script>
      
        var options = {
          series: [<%=datachart %>],
            labels: ['พนักงานที่ทำแบบประเมินแล้ว','พนักงานที่ยังไม่ทำแบบประเมิน'],
			legend: {position: 'bottom'},
          chart: {
          height: 480,
          type: 'pie',
        },       
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);     
        chart.render();      
      
    </script> 
</asp:Content>