﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="Site.Master" CodeBehind="ReportEngagement.aspx.vb" Inherits="SCS.ReportEngagement" %>
<%@ Import Namespace="System.Data" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">    
    <link href="assets/styles.css" rel="stylesheet" />
    <style>      
        #chart {
      padding: 0;
      max-width: 380px;
      margin: 0px auto;
    }
        #chart2 {
       max-width: 100%;
      margin: 0px auto;
    }
      
    </style>

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Survey Engagement
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content">
          <div class="mb-3 card">
            <div class="card-header"><i class="header-icon lnr-pie-chart icon-gradient bg-success">
                </i>ภาพรวมทั้งบริษัท
                <div class="btn-actions-pane-right">                
                </div>
            </div> 
            <div class="card-body"> 
       <div class="row">  
          <div class="col-md-12">             
              <div id="chart"></div>
          </div>
      </div>
                </div>
              </div>

       <div class="mb-3 card">
            <div class="card-header"><i class="header-icon lnr-chart-bars icon-gradient bg-success">
                </i>พนักงานที่ทำแบบประเมินแล้ว แยกตามฝ่าย (%)
                <div class="btn-actions-pane-right">                
                </div>
            </div> 
            <div class="card-body">                 
       <div class="row">  
          <div class="col-md-12">
              <div id="chart2"></div>
           </div>
           </div>
</div>
           </div>   

          <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdExport" CssClass="btn btn-primary" runat="server" Text="Export Data" Width="120px" /> 
        </div>
      </div>
    </section>

    <script>
      
        var options = {
          series: [<%=datachart %>],
            labels: ['พนักงานที่ทำแบบประเมินแล้ว','พนักงานที่ยังไม่ทำแบบประเมิน'],
			legend: {position: 'bottom'},
          chart: {
          width: 480,
              type: 'pie',
           toolbar: {
            show: true
          }
        },       
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);


       var options2 = {
           series: [{
               name: "",
               data: [<%=databar %>]
           }],
          chart: {
          type: 'bar',
              height: 430,         
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
            enabled: true,
            formatter: function (val) {
                return val + ' %';
            },
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: [<%=catebar %>],
        },
        };          

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
    
        chart.render();
        chart2.render();
      
      
    </script>
</asp:Content>
