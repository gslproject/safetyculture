﻿<%@ Page Title="กำหนดผู้ประเมิน" MetaDescription="จัดการข้อมูลผู้ประเมิน Leadership Survey (แบบ Manual)" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AssessorManage.aspx.vb" Inherits="SCS.AssessorManage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user-female icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">    
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-warm-flame">
            </i>ข้อมูลผู้ถูกประเมิน
            <div class="btn-actions-pane-right">
              <asp:HiddenField ID="hdAssesseeUID" runat="server" /> 
            </div>
        </div>
        <div class="card-body">     
            <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>รหัสพนักงาน</label> 
              <asp:label ID="lblCode" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อ-นามสกุล</label>
              <asp:label ID="lblEmployeeName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>ตำแหน่ง</label>
              <asp:label ID="lblPositionName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
              <asp:label ID="lblDepartmentName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
              <asp:label ID="lblDivisionName" runat="server" cssclass="text-blue no-border"></asp:label>
          </div>
        </div>
      </div> 
    </div>
  </div> 
   </section>
 </div>
<div class="row">  
    <section class="col-lg-12 connectedSortable">
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-checkmark-circle icon-gradient bg-warm-flame"></i>เพิ่มผู้ประเมิน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body"> 
            <div class="row">                
        <table class="table">
            <tr>
                <td width="120px">เลือกผู้ประเมิน</td>
                <td> <asp:DropDownList ID="ddlAssessor" runat="server" cssclass="form-control select2"  placeholder="--- เลือกผู้ประเมิน ---" Width="100%">
            </asp:DropDownList>
                </td>
                <td Width="50px"><asp:Button ID="cmdAddAssessor" runat="server" Text="Add" Width="50" CssClass="btn btn-primary" /></td>
            </tr>
            <tr>
                <td colspan="3">รายการผู้ประเมิน</td>
            </tr>
            <tr>
                <td colspan="3"> 
     <div class="table-responsive">           
                <asp:GridView ID="grdAssessor" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-hover" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />                            </asp:BoundField>
                        <asp:BoundField DataField="AssessorName" HeaderText="ชื่อ-นามสกุล" />
                            <asp:BoundField DataField="LevelName" HeaderText="ระดับ" />
                            <asp:BoundField DataField="PositionName" HeaderText="ตำแหน่ง" />
                            <asp:BoundField DataField="DepartmentName" HeaderText="แผนก" />
                            <asp:BoundField DataField="DivisionName" HeaderText="ฝ่าย" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
       </div> 
                </td>
            </tr>
        </table>      
                            </div> 
            <div class="row">       
        <div class="col-md-6">
            <asp:Button ID="cmdDel" CssClass="btn btn-danger" runat="server" Text="ลบทั้งหมด" Width="120px" />
        </div>
      </div>
</div>        
          </div>         
        </section>                   
                </div>     
          </section>
</asp:Content>