﻿Imports System.Data
Public Class ReportEngagement2
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtEmp As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        lblYear.Text = Request.Cookies("SafetyCS")("ASSMYEAR").ToString()
        dtEmp = ctlR.RPT_LeaderSurveyEngagement(StrNull2Zero(lblYear.Text), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))
    End Sub
    Protected Sub cmdExport_Click(sender As Object, e As EventArgs) Handles cmdExport.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=le&comp=" & Request.Cookies("SafetyCS")("LoginCompanyUID") & "&y=" & Request.Cookies("SafetyCS")("ASSMYEAR") & "&RPTTYPE=EXCEL','_blank');", True)
    End Sub
End Class