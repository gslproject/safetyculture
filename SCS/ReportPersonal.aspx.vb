﻿
Public Class ReportPersonal
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlU As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadCompany()
            ddlCompany.SelectedIndex = 0
            If ddlCompany.SelectedValue = 0 Then
                ddlCompany.SelectedIndex = 1
            End If

            LoadPerson()


            If Request.Cookies("SafetyCS")("ROLE_CUS") = True Then
                ddlPerson.SelectedValue = Request.Cookies("SafetyCS")("LoginPersonUID")
                ddlPerson.Enabled = False
                ddlCompany.Enabled = False
            Else
                ddlPerson.Enabled = True
                ddlCompany.Enabled = True
            End If
        End If

    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("SafetyCS")("ROLE_SPA") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("SafetyCS")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlU.Person_GetByCompanyUID(ddlCompany.SelectedValue)
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        If ddlPerson.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกพนักงานก่อน');", True)
            Exit Sub
        End If

        'Dim Bdate, Edate, Cond As String
        'Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
        'Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)

        'If ddlPerson.SelectedValue = "ALL" Then
        '    Cond = "ALL"
        'Else
        '    Cond = "LAST"
        'End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('SafetyLeadershipReport.aspx?R=personal&COM=" & ddlCompany.SelectedValue & "&EMP=" & ddlPerson.SelectedValue & "','_blank');", True)
        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=personal&COM=" & ddlCompany.SelectedValue & "&EMP=" & ddlPerson.SelectedValue & "&RPTTYPE=PDF','_blank');", True)
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadPerson()
    End Sub
End Class

