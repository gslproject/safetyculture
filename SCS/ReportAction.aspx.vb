﻿
Public Class ReportAction
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then

            chkExcel.Visible = False
            chkExcel.Checked = True
            LoadCompany()
            LoadType()

            txtStartDate.Text = Today.Date.ToString("dd/MM/yyyy")
            txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")

            If Request.Cookies("SafetyCS")("ROLE_SPA") = True Then
                ddlCompany.Enabled = True
            Else
                ddlCompany.Enabled = False
            End If
        End If
    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("SafetyCS")("ROLE_SPA") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("SafetyCS")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedValue = Request.Cookies("SafetyCS")("LoginCompanyUID")
        End With

    End Sub
    Private Sub LoadType()
        Dim ctlR As New ReferenceValueController

        ddlType.Items.Add("--All--")
        ddlType.Items(0).Value = "ALL"

        dt = ctlR.ReferenceValue_GetByDomainCode("ICRTYPE")

        For i = 0 To dt.Rows.Count - 1
            ddlType.Items.Add(dt.Rows(i)("DisplayName"))
            ddlType.Items(i + 1).Value = dt.Rows(i)("ValueCode")
        Next

        ddlType.SelectedIndex = 0
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        If txtStartDate.Text = "" Or txtEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุช่วงวันที่ก่อน');", True)
            Exit Sub
        End If

        Dim Bdate, Edate As String
        'Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
        'Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)
        If txtStartDate.Text <> "" Then
            Bdate = ConvertYearEN(Right(txtStartDate.Text, 4)) + "-" + Mid(txtStartDate.Text, 4, 2) + "-" + Left(txtStartDate.Text, 2)
        End If
        If txtEndDate.Text <> "" Then
            Edate = ConvertYearEN(Right(txtEndDate.Text, 4)) + "-" + Mid(txtEndDate.Text, 4, 2) + "-" + Left(txtEndDate.Text, 2)
        End If

        'If ddlType.SelectedValue = "ALL" Then
        '    Cond = "ALL"
        'Else
        '    Cond = ddlType.SelectedValue
        'End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?R=actrpt&comp=" & ddlCompany.SelectedValue & "&type=" & ddlType.SelectedValue & "&st=" & ddlStatus.SelectedValue & "&bdt=" & Bdate & "&edt=" & Edate & "&rpttype=" & IIf(chkExcel.Checked = True, "EXCEL", "PDF") & "','_blank');", True)


        'Response.Redirect("Report/ReportViewer?R=tasktotal&COM=" & ddlCompany.SelectedValue & "&COND=" & ddlFillter.SelectedValue & "&BDT=" & Bdate & "&EDT=" & Edate & "&RPTTYPE=" & IIf(chkExcel.Checked = True, "EXCEL", "PDF"))


    End Sub

End Class

