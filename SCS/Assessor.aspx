﻿<%@ Page Title="จัดการผู้ประเมิน" MetaDescription="เลือกผู้ถูกประเมิน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Assessor.aspx.vb" Inherits="SCS.Assessor" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user-female icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-warm-flame">
            </i>รายชื่อผู้ถูกประเมิน
            <div class="btn-actions-pane-right">             
            </div>
        </div>
        <div class="card-body">
            <table id="tbdata" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="sorting_asc_disabled"></th>
                        <th>รหัส</th>
                        <th>ขื่อ-นามสกุล</th>
                        <th>ระดับ</th>
                        <th>ตำแหน่ง</th>
                        <th>แผนก</th>
                        <th>ฝ่าย</th>
                    </tr>
                </thead>
                <tbody>
                    <% For Each row As DataRow In dtAssr.Rows %>
                        <tr>
                            <td width="30"><a class="editcus" href="AssessorManage?m=ma&uid=<% =String.Concat(row("UserID")) %>" ><img src="images/select.png" /></a>
                            </td>
                            <td><%=String.Concat(row("Code")) %></td>
                            <td><%=String.Concat(row("Name")) %></td>
                            <td><%=String.Concat(row("LevelName")) %></td>
                            <td><%=String.Concat(row("PositionName")) %></td>
                            <td><%=String.Concat(row("DepartmentName")) %></td>
                            <td><%=String.Concat(row("DivisionName")) %></td> 
                        </tr>
                        <% Next %>
                </tbody>
            </table>
        </div>
        <div class="d-block text-right card-footer">
        </div>
    </div>
</section>
</asp:Content>