﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ReportController
    Inherits BaseClass
    Dim ds As New DataSet
#Region "Survey"

    Public Function ReportAssessment_GetYear(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ReportAssessment_GetYear"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Assessment_AvgScore(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment_AvgScore"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Assessment_AvgScoreByGroup(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment_AvgScoreByGroup"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Assessment_GetSumaryScoreByYear(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment_GetSumaryScoreByYear"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Assessment_AvgScoreByYear(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment_AvgScoreByYear"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_LegalAction_Assessment_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_LegalAction_Assessment_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Survey_Engagement(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Survey_Engagement"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Survey_EngagementByDivision(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Survey_EngagementByDivision"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Legal_ByStatus_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_ByStatus_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Legal_ByType_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_ByType_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function


    Public Function Legal_GetLastMonthCountByType() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetLastMonthCountByType"))
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetCountByType() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetCountByType"))
        Return ds.Tables(0)
    End Function


#End Region

#Region "360"

    Public Function RPT_LeaderSurveyEngagement(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_LeaderSurveyEngagement"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Assessment360_AvgScoreByEmployeeLevel(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment360_AvgScoreByEmployeeLevel"), AsmYear, CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Assessment360_AvgScoreByEmployeeLevel_Pivot(AsmYear As Integer, CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment360_AvgScoreByEmployeeLevel_Pivot"), AsmYear, CompanyUID)

        If ds.Tables.Count > 0 Then
            Return ds.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function RPT_Assessment360_ScoreByEmployee(AsmYear As Integer, EmployeeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Assessment360_ScoreByEmployee"), AsmYear, EmployeeUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessment360_AssessmentCountByTop(AsmYear As Integer, EmployeeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment360_AssessmentCountByTop"), AsmYear, EmployeeUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessment360_AssessmentCountByManager(AsmYear As Integer, EmployeeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment360_AssessmentCountByManager"), AsmYear, EmployeeUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessment360_AssessmentCountBySupervisor(AsmYear As Integer, EmployeeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment360_AssessmentCountBySupervisor"), AsmYear, EmployeeUID)
        Return ds.Tables(0)
    End Function

    Public Function EvaluationGroup_GetByEvaluationUID(EvaUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EvaluationGroup_GetByEvaluationUID"), EvaUID)
        Return ds.Tables(0)
    End Function


#End Region

End Class
