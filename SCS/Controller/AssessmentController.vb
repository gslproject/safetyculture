﻿Imports Microsoft.ApplicationBlocks.Data
Public Class AssessmentController
    Inherits BaseClass
    Public ds As New DataSet
    Public Function AssessmentAnswer_GetScore(Code As String, TopicUID As Integer, AnswerValue As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentAnswer_GetScore"), Code, TopicUID, AnswerValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function ScoreROSA_GetScore(Code As String, RowScore As Integer, ColScore As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreROSA_GetScore"), Code, RowScore, ColScore)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function Assessment_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_Get"))

        Return ds.Tables(0)

    End Function

    Public Function Assessment_GetByUID(Code As String, UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetByUID"), Code, UID)

        Return ds.Tables(0)

    End Function
    Public Function Assessment_GetEmployeeByAssessorUID(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetEmployeeByAssessorUID"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessor_GetDetail(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetDetail"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessee_GetByTop(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetByTop"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessee_GetByManager(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetByManager"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessee_GetBySupervisor(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetBySupervisor"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessee_GetByEmployee(AsmYear As Integer, AssessorUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetByEmployee"), AsmYear, AssessorUID)
        Return ds.Tables(0)
    End Function

#Region "Asm 360"
    Public Function Assessment360_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment360_GetByUID"), UID)
        Return ds.Tables(0)
    End Function


    Public Function Assessee_GetStatusByTop(AsmYear As Integer, AssessorUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetStatusByTop"), AsmYear, AssessorUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function Assessee_GetStatusByManager(AsmYear As Integer, AssessorUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetStatusByManager"), AsmYear, AssessorUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function Assessee_GetStatusBySupervisor(AsmYear As Integer, AssessorUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetStatusBySupervisor"), AsmYear, AssessorUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function Assessee_GetStatusByEmployee(AsmYear As Integer, AssessorUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_GetStatusByEmployee"), AsmYear, AssessorUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

#End Region

    Public Function Assessment_GetByAssessorUID(AsmYear As Integer, PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetByAssessorUID"), AsmYear, PersonUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessment_Save(ByVal AssessorUID As Integer, ByVal Q1 As Integer, ByVal Q2 As Integer, ByVal Q3 As Integer, ByVal Q4 As Integer, ByVal Q5 As Integer, ByVal Q6 As Integer, ByVal Q7 As Integer, ByVal Q8 As Integer, ByVal Q9 As Integer, ByVal Q10 As Integer, ByVal Q11 As Integer, ByVal Q12 As Integer, ByVal Q13 As Integer, ByVal Q14 As Integer, ByVal Q15 As Integer, ByVal Q16 As Integer, ByVal Q17 As Integer, ByVal Q18 As Integer, ByVal Q19 As Integer, ByVal Q20 As Integer, ByVal Q21 As Integer, ByVal Comment1 As String, ByVal Comment2 As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment_Save"), AssessorUID, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, Comment1, Comment2, CUser)

    End Function
    Public Function Assessment360_Save(ByVal AssessorUID As Integer, ByVal EmployeeUID As Integer, ByVal Q1 As Integer, ByVal Q2 As Integer, ByVal Q3 As Integer, ByVal Q4 As Integer, ByVal Q5 As Integer, ByVal Q6 As Integer, ByVal Q7 As Integer, ByVal Q8 As Integer, ByVal Q9 As Integer, ByVal Comment As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessment360_Save"), AssessorUID, EmployeeUID, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Comment, CUser)

    End Function

    Public Function AsmREBA_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal S1 As String, ByVal S2 As String, ByVal S3 As String, ByVal S5 As String, ByVal S7 As String, ByVal S8 As String, S9 As String, S11 As String, S13 As String, S1A As String, S2A As String, S3A As String, S5A As String, S7A As String, S9A As String, ByVal FinalScore As Integer, ByVal ResultText As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmREBA_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, S1, S2, S3, S5, S7, S8, S9, S11, S13, S1A, S2A, S3A, S5A, S7A, S9A, FinalScore, ResultText, CUser)

    End Function

    Public Function AsmRULA_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal S1 As String, ByVal S2 As String, ByVal S3 As String, ByVal S4 As String, ByVal S6 As String, ByVal S7 As String, ByVal S9 As String, ByVal S10 As String, ByVal S11 As String, ByVal S13 As String, ByVal S14 As String, S1A As String, S2A As String, S3A As String, S9A As String, S10A As String, ByVal FinalScore As Integer, ByVal ResultText As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmRULA_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, S1, S2, S3, S4, S6, S7, S9, S10, S11, S13, S14, S1A, S2A, S3A, S9A, S10A, FinalScore, ResultText, CUser)
    End Function


    Public Function AsmROSA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmROSA_Delete"), UID)
    End Function
    Public Function AsmREBA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmREBA_Delete"), UID)
    End Function
    Public Function AsmRULA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmRULA_Delete"), UID)
    End Function
    Public Function AsmNIOSH_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmNIOSH_Delete"), UID)
    End Function




    Public Function ScoreREBA_GetScore(Code As String, RowScore As Integer, ColScore1 As Integer, ColScore2 As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreREBA_GetScore"), Code, RowScore, ColScore1, ColScore2)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function ScoreRULA_GetScore(Code As String, RowScore1 As Integer, RowScore2 As Integer, ColScore1 As Integer, ColScore2 As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreRULA_GetScore"), Code, RowScore1, RowScore2, ColScore1, ColScore2)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function NIOSH_GetFMScore(F As Double, W As Integer, V As Integer) As Double
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("NIOSH_GetFMScore"), F, W, V)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Dbl(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function AsmNIOSH_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal L As Double, ByVal H As Double, ByVal V As Double, ByVal D As Double, ByVal A As Double, ByVal C As Double, ByVal Dur As Double, ByVal F As Double, ByVal LC As Double, ByVal HM As Double, ByVal VM As Double, DM As Double, AM As Double, CM As Double, FM As Double, RWL As Double, ByVal LI As Double, ByVal RecommendText As String, ByVal CUser As Integer, ByVal FIRWL As Double, ByVal FILI As Double) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmNIOSH_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, L, H, V, D, A, C, Dur, F, LC, HM, VM, DM, AM, CM, FM, RWL, LI, RecommendText, CUser, FIRWL, FILI)
    End Function

#Region "HRA"
    Public Function AsmHRA_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_Get"), CompanyUID)

        Return ds.Tables(0)

    End Function
    Public Function AsmHRA_GetByUID(UID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_GetByUID"), UID)

        Return ds.Tables(0)

    End Function

    Public Function AsmHRA_GetByPerson(PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_GetByPerson"), PersonUID)

        Return ds.Tables(0)

    End Function

    Public Function AsmHRA_Save(ByVal UID As Long, ByVal PersonUID As Integer, ByVal ErgoScore As Double, ByVal ErgoResultText As String, ByVal HRAScore As Double, ByVal HRAResultText As String, ByVal Remark As String, ByVal MUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmHRA_Save"), UID, PersonUID, ErgoScore, ErgoResultText, HRAScore, HRAResultText, Remark, MUser)
    End Function


#End Region

End Class
