﻿Imports Microsoft.ApplicationBlocks.Data

#Region "User"
Public Class UserController
    Inherits BaseClass
    Public ds As New DataSet
    Public Function User_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_Get"))
        Return ds.Tables(0)
    End Function
    Public Function User_GetActive(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function User_GetForAssessor(CompanyUID As Integer, AssesseeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetForAssessor"), CompanyUID, AssesseeUID)
        Return ds.Tables(0)
    End Function


    Public Function User_CheckLogin(ByVal pUsername As String, ByVal pPassword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_Login"), pUsername, pPassword)
        Return ds.Tables(0)
    End Function

    Public Function User_IsAssessmentLevel(ByVal pUserID As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_IsAssessmentLevel"), pUserID)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function User_GetByUserID(ByVal UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetByUserID"), UserID)
        Return ds.Tables(0)
    End Function

    Public Function User_GetUID(ByVal Username As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetUID"), Username)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Users_Online() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Users_Online"))
        Return ds.Tables(0)
    End Function

    Public Function User_GetBySearch(CompanyUID As Integer, Grp As Integer, id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "User_GetSearch", CompanyUID, Grp, id)
        Return ds.Tables(0)
    End Function
    Public Function User_GetNameByUserID(ByVal userid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetNameByUserID"), userid)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function User_GetNameByUsername(ByVal username As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetNameByUsername"), username)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function User_LastLog_Update(ByVal pUsername As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_LastLog_Update"), pUsername)
    End Function
    Public Function User_Delete(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_Delete"), pID)
    End Function

    Public Function User_Save(
           ByVal UserID As Integer,
           ByVal Username As String,
           ByVal Password As String,
             ByVal PrefixUID As Integer,
           ByVal FnameTH As String,
           ByVal LNameTH As String,
            ByVal FnameEN As String,
           ByVal LNameEN As String,
            ByVal Sex As String,
           ByVal Age As String,
   ByVal Tel As String,
           ByVal Email As String,
           ByVal CompanyUID As Integer,
         ByVal DivisionUID As Integer,
           ByVal DepartmentUID As Integer,
              ByVal PositionUID As Integer,
              ByVal EmployeeLevelID As Integer,
              ByVal StartDate As String,
              ByVal EndDate As String,
              ByVal EmployeeStatus As String,
           ByVal StatusFlag As String,
           ByVal CUser As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_Save"),
             UserID _
           , Username _
           , Password _
           , PrefixUID _
           , FnameTH _
           , LNameTH _
           , FnameEN _
           , LNameEN _
           , Sex, Age, Tel, Email _
           , CompanyUID _
           , DivisionUID, DepartmentUID, PositionUID, EmployeeLevelID, StartDate, EndDate, EmployeeStatus _
           , StatusFlag _
           , CUser)

    End Function


    Public Function User_Update(ByVal Username As String, ByVal Password As String, ByVal FnameTH As String, ByVal LName As String, ByVal Email As String, ByVal IsSuperUser As Integer, ByVal Status As Integer, ByVal UserProfileID As Integer, ByVal ProfileID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_Update"), Username, Password, FnameTH, LName, Email, IsSuperUser, Status, UserProfileID, ProfileID)
    End Function

    Public Function User_UpdateDatail(ByVal Username As String, ByVal Password As String, ByVal FnameTH As String, ByVal LName As String, ByVal Email As String, ByVal IsSuperUser As Integer, ByVal Status As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_UpdateDatail"), Username, Password, FnameTH, LName, Email, IsSuperUser, Status)
    End Function
    Public Function User_CheckDuplicate(ByVal pUser As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_GetByUsername"), pUser)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function UserCompany_GetByUserID(UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "UserCompany_GetByUserID", UserID)
        Return ds.Tables(0)
    End Function
    Public Function UserCompany_GetByUsername(Username As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "UserCompany_GetByUsername", Username)
        Return ds.Tables(0)
    End Function
    Public Function User_ChangeUsername(ByVal Username As String, ByVal Username2 As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_ChangeUsername"), Username, Username2)
    End Function

    Public Function Assessor_Add(ByVal Username As String, ByVal RoleID As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessor_Add"), Username, RoleID, UpdBy)
    End Function
    Public Function Assessor_Save(ByVal UserID As String, ByVal GroupUID As Integer, StatusFlag As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserAccessGroup_Save"), UserID, GroupUID, StatusFlag, UpdBy)
    End Function

    Public Function User_UpdateProfileID(ByVal Username As String, sProfileID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_UpdateProfileID"), Username, sProfileID)
    End Function

    Public Function Assessor_UpdateStatus(Username As String, RoleID As Integer, bActive As Integer, UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserRole_UpdateStatus"), Username, RoleID, bActive, UpdBy)
    End Function

    Public Function User_ChangePassword(ByVal Username As String, ByVal Password As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_ChangePassword"), Username, Password)
    End Function

    Public Function User_GenLogfile(ByVal Username As String, ByVal Act_Type As String, DB_Effective As String, Descrp As String, Remark As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_genLogfile"), Username, Act_Type, DB_Effective, Descrp, Remark)
    End Function

    Public Function User_UpdateMail(ByVal Username As String, email As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_UpdateEmail"), Username, email)
    End Function
    Public Function UserCompany_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserCompany_Delete"), UID)
    End Function
    Public Function UserCompany_Save(ByVal UserID As Integer, Username As String, CompanyUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserCompany_Save"), UserID, Username, CompanyUID)
    End Function


    Public Function SendAlert_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("SendAlert_Get"))
        Return ds.Tables(0)
    End Function
    Public Function SendAlert_GetMail() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("SendAlert_GetMail"))
        Return ds.Tables(0)
    End Function

    Public Function SendAlert_Save(ByVal LocationID As Integer, TimePhaseID As Integer, email As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_Save"), LocationID, TimePhaseID, email, Status)
    End Function
    Public Function SendAlert_UpdateStatus(ByVal LocationID As Integer, TimePhaseID As Integer, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_UpdateStatus"), LocationID, TimePhaseID, Status)
    End Function


#Region "Person"
    Public Function Person_GetActive(ByVal CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function


    Public Function User_CheckDuplicateByCode(ByVal CompanyUID As Integer, Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("User_CheckDuplicateByCode"), CompanyUID, Code)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function Person_GetByCompanyUID(ByVal CompID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetByCompanyUID"), CompID, "")
        Return ds.Tables(0)
    End Function

    Public Function User_AddByImport(
           ByVal Code As String,
           ByVal PrefixUID As Integer,
           ByVal NameTH As String,
           ByVal SurnameTH As String,
           ByVal NameEN As String,
           ByVal SurnameEN As String,
           ByVal DateOfBirth As String,
           ByVal Gender As String, Age As Integer,
           ByVal CompanyUID As Integer, EmployeeLevelID As Integer,
           ByVal DivisionUID As Integer,
           ByVal DepartmentUID As Integer,
           ByVal PositionUID As Integer, Username As String, UserGroup As Integer, Email As String, Tel As String, CUser As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("User_AddByImport"),
             Code _
           , PrefixUID _
           , NameTH _
           , SurnameTH _
           , NameEN _
           , SurnameEN _
           , DateOfBirth _
           , Gender, Age _
           , CompanyUID, EmployeeLevelID _
           , DivisionUID _
           , DepartmentUID _
           , PositionUID _
           , Username, UserGroup, Email, Tel, CUser)
    End Function


#End Region

End Class
Public Class UserRoleController
    Inherits BaseClass
    Public ds As New DataSet

    Public Function UserGroup_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserGroup_Get"))
        Return ds.Tables(0)
    End Function


    Public Function UserAccessGroup_Get(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAccessGroup_Get"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserRole_GetByUserID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserRole_GetByUserID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserRole_GetActiveRoleByUID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserRole_GetActiveRoleByUID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserAccessGroup_Save(ByVal UserID As String, ByVal GroupUID As Integer, StatusFlag As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserAccessGroup_Save"), UserID, GroupUID, StatusFlag, UpdBy)
    End Function

    Public Function UserAccessGroup_GetByUID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAccessGroup_GetByUID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserAction_CheckByUser(ByVal pUser As String, ByVal pLocation As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAction_CheckByUser"), pUser, pLocation)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function UserAction_Checked(ByVal pUser As String, ByVal pLocation As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAction_Checked"), pUser, pLocation)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) = 1 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function UserAction_Add(ByVal Username As String, ByVal LocationID As Integer, ByVal RoleAction As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserAction_Add"), Username, LocationID, RoleAction, UpdBy)
    End Function
    Public Function UserAction_Update(ByVal Username As String, ByVal LocationID As Integer, ByVal RoleAction As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserAction_Update"), Username, LocationID, RoleAction, UpdBy)
    End Function

End Class

#End Region


#Region "Assessor"
Public Class AssessorController
    Inherits BaseClass
    Public ds As New DataSet
    'Public Function Assessor_Get(CompanyUID As Integer) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_Get"), CompanyUID)
    '    Return ds.Tables(0)
    'End Function
    Public Function Assessee_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessee_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessor_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetActive"))
        Return ds.Tables(0)
    End Function
    Public Function Assessor_GetByUserID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetByUserID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function Assessor_GetByAssessorUID(ByVal AssesseeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetByAssessorID"), AssesseeUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessor_GetByAssesseeUID(ByVal AssesseeUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetByAssesseeUID"), AssesseeUID)
        Return ds.Tables(0)
    End Function

    Public Function Assessor_GetActiveRoleByUID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetActiveRoleByUID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserAccessGroup_GetByUID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAccessGroup_GetByUID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserAction_CheckByUser(ByVal pUser As String, ByVal pLocation As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserAction_CheckByUser"), pUser, pLocation)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Assessor_Save(ByVal AsmYear As Integer, ByVal AssesseeUID As Integer, ByVal AssessorUID As Integer, ByVal AssessmentType As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessor_Save"), AsmYear, AssesseeUID, AssessorUID, AssessmentType, CUser)
    End Function
    Public Function Assessor_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessor_Delete"), UID)
    End Function

    Public Function Assessor_DeleteByAssessee(ByVal AsmYear As Integer, ByVal AssesseeUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Assessor_DeleteByAssessee"), AsmYear, AssesseeUID)
    End Function

    Public Function Assessor_GetAssessorUID(ByVal Username As String, name As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessor_GetAssessorUID"), Username, name)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function AssessorCustomer_Save(ByVal AssessorUID As Integer, ByVal CustomerUID As Integer, ByVal Username As String, ByVal CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorCustomer_Save"), AssessorUID, CustomerUID, Username, CUser)
    End Function
    Public Function AssessorCustomer_UpdateCustomerUID(ByVal Username As String, ByVal CustomerUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorCustomer_UpdateCustomerUID"), Username, CustomerUID)
    End Function
    Public Function AssessorCustomer_UpdateAssessorUID(ByVal Username As String, ByVal AssessorUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorCustomer_UpdateAssessorUID"), Username, AssessorUID)
    End Function

    Public Function AssessorCustomer_Delete(ByVal AUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AssessorCustomer_Delete"), AUID)
    End Function




End Class
#End Region