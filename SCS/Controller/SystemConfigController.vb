﻿Imports Microsoft.ApplicationBlocks.Data
Public Class SystemConfigController
    Inherits BaseClass
    Dim ds As New DataSet

    Public Function SystemConfig_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SystemConfig_Get", CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function SystemConfig_GetValueByCode(CompanyUID As Integer, Code As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SystemConfig_GetValueByCode", CompanyUID, Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Public Function SystemConfig_GetByCode(CompanyUID As Integer, Code As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "SystemConfig_GetByCode", CompanyUID, Code)
        Return ds.Tables(0)
    End Function

    Public Function DataConfig_Add4NewCompany(companyUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DataConfig_Add4NewCompany", companyUID)
    End Function

    Public Function SystemConfig_Update(CompanyUID As Integer, Code As String, pValue As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "SystemConfig_Update", CompanyUID, Code, pValue)
    End Function

    Public Function DataConfig_UpdateValue(code As String, pValue As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DataConfig_UpdateValue", code, pValue)
    End Function


End Class
 