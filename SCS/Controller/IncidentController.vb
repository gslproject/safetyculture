﻿Imports Microsoft.ApplicationBlocks.Data
Public Class IncidentController
    Inherits BaseClass
    Public ds As New DataSet

#Region "UAUC"
    Public Function UAUC_GetBySearch(CompanyUID As Integer, IncidentType As String, AreaID As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UAUC_GetBySearch"), CompanyUID, IncidentType, AreaID, Keyword)
        Return ds.Tables(0)
    End Function
    Public Function UAUC_GetBySearch(CompanyUID As Integer, PersonUID As String, IncidentType As String, AreaID As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UAUC_GetBySearchPerson"), CompanyUID, PersonUID, IncidentType, AreaID, Keyword)
        Return ds.Tables(0)
    End Function

#End Region
#Region "Incident"
    Public Function Incident_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByStatus(Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByStatus"), Status)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetBySearch(CompanyUID As Integer, IncidentType As String, AreaID As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetBySearch"), CompanyUID, IncidentType, AreaID, Keyword)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetBySearch(CompanyUID As Integer, PersonUID As String, IncidentType As String, AreaID As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetBySearchPerson"), CompanyUID, PersonUID, IncidentType, AreaID, Keyword)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetSearch4Investigation(CompanyUID As Integer, PersonUID As Integer, IncidentType As String, AreaID As String, Status As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetSearch4Investigation"), CompanyUID, PersonUID, IncidentType, AreaID, Status, Keyword)
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetSearchByOwner(CompanyUID As Integer, PersonUID As Integer, IncidentType As String, AreaID As String, Status As String, Keyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetSearchByOwner"), CompanyUID, PersonUID, IncidentType, AreaID, Status, Keyword)
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetByPhaseUID(PhaseUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByPhaseUID"), PhaseUID)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByMinistryUID(PhaseUID As String, MinistryUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByMinistryUID"), PhaseUID, MinistryUID)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByYear(PhaseUID As String, IncidentYear As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByYear"), PhaseUID, IncidentYear)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByBusinessTypeUID(PhaseUID As String, BusinessTypeUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByBusinessTypeUID"), PhaseUID, BusinessTypeUID)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetByKeyword(PhaseUID As String, sKeyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByKeyword"), PhaseUID, sKeyword)
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetByUser(UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByUser"), UserID)
        Return ds.Tables(0)
    End Function
    Public Function Incident_GetActive(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function IncidentYear_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentYear_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function

    Public Function UAUC_Save(ByVal UID As Integer, ByVal CompanyUID As Integer, ByVal Code As String, ByVal IncidentType As String, IncidentDate As String, Time As String, ByVal IncidentName As String, ByVal AreaUID As Integer, ByVal ReporterUID As Integer, IncidentDescription As String, ByVal CauseDescription As String, IncidentStatus As String, isUA As String, isUC As String, ByVal MUser As Integer, ActionDescription As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UAUC_Save"), UID, CompanyUID, Code, IncidentType, IncidentDate, Time, IncidentName, IncidentDescription, AreaUID, ReporterUID, CauseDescription, IncidentStatus, isUA, isUC, MUser, ActionDescription)
    End Function

    Public Function Incident_Save(ByVal UID As Integer, ByVal CompanyUID As Integer, ByVal Code As String, ByVal IncidentType As String, IncidentDate As String, Time As String, ByVal IncidentName As String, ByVal AreaUID As Integer, ByVal ReporterUID As Integer, IncidentDescription As String, ByVal CauseDescription As String, ICRLevel As String, ICRCost As String, IncidentStatus As String, ByVal MUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_Save"), UID, CompanyUID, Code, IncidentType, IncidentDate, Time, IncidentName, IncidentDescription, AreaUID, ReporterUID, CauseDescription, IncidentStatus, ICRLevel, ICRCost, MUser)
    End Function


    Public Function Incident_UpdateActionStatus(ByVal UID As Integer, ActionStatus As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_UpdateActionStatus"), UID, ActionStatus, UpdBy)
    End Function
    Public Function Incident_Approve(ByVal UID As Integer, ActionStatus As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_Approve"), UID, ActionStatus, UpdBy)
    End Function

    Public Function Practice_Get(IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_Get"), IncidentUID)
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetDataPrediction(CompanyUID As Integer, CauseUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetDataPrediction"), CompanyUID, CauseUID)
        Return ds.Tables(0)
    End Function

    Public Function Incident_GetUID(pCode As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_GetUID"), pCode)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Incident_UpdateFile(ByVal UID As Integer, ByVal FilePath As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("Incident_UpdateFile"), UID, FilePath, CUser)
    End Function
#End Region

#Region "Incident Cause Detail"
    Public Function IncidentCause_Delete(ByVal IncidentUID As Integer, TypeCode As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentCause_Delete"), IncidentUID, TypeCode)
    End Function

    Public Function IncidentCause_Save(ByVal IncidentUID As Integer, ByVal TypeCode As String, CauseUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentCause_Save"), IncidentUID, TypeCode, CauseUID, UpdBy)
    End Function
    Public Function IncidentCause_GetByIncidentUID(IncidentUID As Integer, TypeCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentCause_GetByIncidentUID"), IncidentUID, TypeCode)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Incident Keyword"
    Public Function IncidentKeyword_Delete(ByVal IncidentUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentKeyword_Delete"), IncidentUID)
    End Function

    Public Function IncidentKeyword_Save(ByVal IncidentUID As Integer, ByVal KeywordUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentKeyword_Save"), IncidentUID, KeywordUID, UpdBy)
    End Function
    Public Function IncidentKeyword_GetByIncidentUID(IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentKeyword_GetByIncidentUID"), IncidentUID)
        Return ds.Tables(0)
    End Function

#End Region


    Public Function Incident_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_Delete"), UID)
    End Function


    Public Function Incident_UpdateFileName(ByVal UID As Integer, ByVal IncidentNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_UpdateFileName"), UID, IncidentNo, Name)
    End Function

    Public Function IncidentImage_Get(IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentImage_Get"), IncidentUID)
        Return ds.Tables(0)
    End Function

    Public Function IncidentImage_Delete(ByVal UID As Integer, ByVal IncidentNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentImage_Delete"), UID, IncidentNo, Name)
    End Function
    'Public Function Incident_UpdateFileName(ByVal UID As Integer, ByVal IncidentNo As String, ByVal Name As String) As Integer
    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Incident_UpdateFileName"), UID, IncidentNo, Name)
    'End Function


    Public Function JobOwner_Get(CompanyUID As Integer, IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_Get"), CompanyUID, IncidentUID)
        Return ds.Tables(0)
    End Function
    Public Function JobOwner_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function JobOwner_Save(ByVal UID As Integer, ByVal IncidentUID As Integer, ByVal PersonUID As Integer, CompanyUID As Integer, LevelID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Save"), UID, IncidentUID, PersonUID, CompanyUID, LevelID)
    End Function

    Public Function JobOwner_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Delete"), UID)
    End Function

    Public Function Incident_Problem_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Incident_Problem_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function IncidentOwner_GetEmailAlert(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentOwner_GetEmailAlert"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function SendAlert_Save(ByVal CompanyUID As Integer, IncidentUID As Integer, PersonUID As Integer, email As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_Save"), CompanyUID, IncidentUID, PersonUID, email, Status)
    End Function



    Public Function IncidentAction_GetByIncidentUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentAction_GetByIncidentUID"), pUID)
        Return ds.Tables(0)
    End Function

    Public Function Event_GetByAdmin(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByAdmin"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function
    Public Function Event_GetByUser(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByUser"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Incident_Assessment(ByVal CompanyUID As Integer, Bdate As Integer, Edate As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Incident_Assessment"), CompanyUID, Bdate, Edate)
        Return ds.Tables(0)
    End Function

#Region "Incident Master"
    Public Function IncidentMaster_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentMaster_GetActive"))
        Return ds.Tables(0)
    End Function
#End Region

#Region "Incident Type"
    Public Function IncidentType_GetByIncidentID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentType_GetByIncidentID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function IncidentType_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentType_Get"))
        Return ds.Tables(0)
    End Function

#End Region

#Region "Cause Item"
    Public Function CauseItem_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CauseItem_Get"))
        Return ds.Tables(0)
    End Function
    Public Function CauseItem_GetActive(TypeCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("CauseItem_GetActive"), TypeCode)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Injury"
    Public Function IncidentInjury_Get(IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentInjury_Get"), IncidentUID)
        Return ds.Tables(0)
    End Function
    Public Function IncidentInjury_Add(ByVal IncidentUID As Integer, ByVal PersonUID As Integer, PersonType As String, ByVal BodyUID As Integer, ByVal Descriptions As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentInjury_Add"), IncidentUID, PersonUID, PersonType, BodyUID, Descriptions, CUser)
    End Function

    Public Function IncidentInjury_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentInjury_Delete"), UID)
    End Function

#End Region

#Region "Investigation Team"
    Public Function Investigation_Get(IncidentUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Investigation_Get"), IncidentUID)
        Return ds.Tables(0)
    End Function
    Public Function Investigation_Add(ByVal IncidentUID As Integer, ByVal PersonUID As Integer, ByVal RoleUID As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Investigation_Add"), IncidentUID, PersonUID, RoleUID, CUser)
    End Function

    Public Function Investigation_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Investigation_Delete"), UID)
    End Function

#End Region
#Region "Action"
    Public Function IncidentAction_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentAction_Get"))
        Return ds.Tables(0)
    End Function
    Public Function IncidentAction_GetActive(TypeCode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentAction_GetActive"), TypeCode)
        Return ds.Tables(0)
    End Function
    Public Function IncidentAction_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentAction_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function IncidentAction_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("IncidentAction_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function IncidentAction_Save(ByVal UID As Integer, ByVal IncidentUID As Integer, ByVal Descriptions As String, ByVal OwnerUID As Integer, ByVal Duedate As String, ActionStatus As String, CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentAction_Save"), UID, IncidentUID, Descriptions, OwnerUID, Duedate, ActionStatus, CUser)
    End Function

    Public Function IncidentAction_Update(ByVal UID As Integer, ByVal IncidentUID As Integer, ActionStatus As String, CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentAction_Update"), UID, IncidentUID, ActionStatus, CUser)
    End Function



    Public Function IncidentAction_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("IncidentAction_Delete"), UID)
    End Function

#End Region

End Class
