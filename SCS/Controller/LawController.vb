﻿Imports Microsoft.ApplicationBlocks.Data
Public Class LawController
    Inherits BaseClass
    Public ds As New DataSet

#Region "Legal"
    Public Function Legal_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByStatus(Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByStatus"), Status)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetByPhaseUID(PhaseUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByPhaseUID"), PhaseUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByMinistryUID(PhaseUID As String, MinistryUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByMinistryUID"), PhaseUID, MinistryUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByYear(PhaseUID As String, LegalYear As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByYear"), PhaseUID, LegalYear)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByBusinessTypeUID(PhaseUID As String, BusinessTypeUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByBusinessTypeUID"), PhaseUID, BusinessTypeUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByKeyword(PhaseUID As String, sKeyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByKeyword"), PhaseUID, sKeyword)
        Return ds.Tables(0)
    End Function

    Public Function Law_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Law_GetByUser(UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetByUser"), UserID)
        Return ds.Tables(0)
    End Function
    Public Function Law_GetActive(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalYear_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalYear_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function

    Public Function Legal_Save(ByVal UID As Integer, ByVal Code As String, ByVal LegalName As String, ByVal TypeUID As Integer, ByVal LawMasterUID As Integer, OrganizeUID As String, AreaUID As String, PhaseUID As String, IssueDate As String, StartDate As String, RegisDate As String, AnalystUID As Integer, LYear As String, BusinessTypeUID As Integer, MinistryUID As Integer, Keyword As String, ByVal Descriptions As String, StatusFlag As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Save"), UID, Code, LegalName, TypeUID, LawMasterUID, OrganizeUID, AreaUID, PhaseUID, IssueDate, StartDate, RegisDate, AnalystUID, LYear, BusinessTypeUID, MinistryUID, Keyword, Descriptions, StatusFlag, UpdBy)
    End Function
    Public Function Legal_UpdateActionStatus(ByVal UID As Integer, ActionStatus As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_UpdateActionStatus"), UID, ActionStatus, UpdBy)
    End Function
    Public Function Legal_Approve(ByVal UID As Integer, ActionStatus As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Approve"), UID, ActionStatus, UpdBy)
    End Function
    Public Function Legal_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Delete"), UID)
    End Function


    Public Function Practice_Get(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_Get"), LegalUID)
        Return ds.Tables(0)
    End Function

    Public Function Practice_Save(ByVal UID As Integer, LegalUID As Integer, Code As String, Description As String, Recurrence As String, Duedate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Practice_Save"), UID, LegalUID, Code, Description, Recurrence, Duedate)
    End Function

    Public Function Legal_GetUID(pCode As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetUID"), pCode)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Legal_UpdateFile(ByVal UID As Integer, ByVal FilePath As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("Legal_UpdateFile"), UID, FilePath, CUser)
    End Function
#End Region

#Region "Legal Category Detail"
    Public Function LegalCategoryDetail_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_Delete"), LegalUID)
    End Function

    Public Function LegalCategoryDetail_Save(ByVal LegalUID As Integer, ByVal CateUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_Save"), LegalUID, CateUID, UpdBy)
    End Function
    Public Function LegalCategoryDetail_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Legal Keyword"
    Public Function LegalKeyword_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalKeyword_Delete"), LegalUID)
    End Function

    Public Function LegalKeyword_Save(ByVal LegalUID As Integer, ByVal KeywordUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalKeyword_Save"), LegalUID, KeywordUID, UpdBy)
    End Function
    Public Function LegalKeyword_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalKeyword_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region


    Public Function Law_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_Delete"), UID)
    End Function


    Public Function Law_UpdateFileName(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_UpdateFileName"), UID, LawNo, Name)
    End Function

    Public Function LegalImage_Get(LawUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalImage_Get"), LawUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalImage_Delete(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalImage_Delete"), UID, LawNo, Name)
    End Function
    'Public Function Law_UpdateFileName(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_UpdateFileName"), UID, LawNo, Name)
    'End Function


    Public Function JobOwner_Get(CompanyUID As Integer, LawUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_Get"), CompanyUID, LawUID)
        Return ds.Tables(0)
    End Function
    Public Function JobOwner_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function JobOwner_Save(ByVal UID As Integer, ByVal LawUID As Integer, ByVal PersonUID As Integer, CompanyUID As Integer, LevelID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Save"), UID, LawUID, PersonUID, CompanyUID, LevelID)
    End Function

    Public Function JobOwner_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Delete"), UID)
    End Function

    Public Function Law_Problem_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_Problem_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LawAction_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawAction_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LawOwner_GetEmailAlert(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawOwner_GetEmailAlert"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function SendAlert_Save(ByVal CompanyUID As Integer, LawUID As Integer, PersonUID As Integer, email As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_Save"), CompanyUID, LawUID, PersonUID, email, Status)
    End Function
    Public Function SendAlert_UpdateStatus(ByVal CompanyUID As Integer, LawUID As Integer, PersonUID As Integer, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_UpdateStatus"), CompanyUID, LawUID, PersonUID, Status)
    End Function



    Public Function LawAction_GetByLawUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawAction_GetByLawUID"), pUID)
        Return ds.Tables(0)
    End Function


    Public Function LawAction_Save(ByVal UID As Integer, ByVal CompanyUID As Integer, ByVal LawUID As Integer, ByVal ActionUID As String, ByVal OwnerUID As String, ByVal Duedate As String, ActionStatus As String, Comment As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LawAction_Save"), UID, CompanyUID, LawUID, ActionUID, OwnerUID, Duedate, ActionStatus, Comment)
    End Function

    Public Function Event_GetByAdmin(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByAdmin"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function
    Public Function Event_GetByUser(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_Get"))
        Return ds.Tables(0)
    End Function


    Public Function RPT_Law_Assessment(ByVal CompanyUID As Integer, Bdate As Integer, Edate As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Law_Assessment"), CompanyUID, Bdate, Edate)
        Return ds.Tables(0)
    End Function

#Region "Law Master"
    Public Function LawMaster_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawMaster_GetActive"))
        Return ds.Tables(0)
    End Function
#End Region

#Region "Law Type"
    Public Function LawType_GetByLegalID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_GetByLegalID"), UID)
        Return ds.Tables(0)
    End Function
#End Region

#Region "Law Category"
    Public Function LawCategory_GetByLegalID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawCategory_GetByLegalID"), UID)
        Return ds.Tables(0)
    End Function
#End Region
End Class
