﻿<%@ Page Title="Setting" MetaDescription="จัดการค่าพื้นฐาน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Setting.aspx.vb" Inherits="SCS.Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-config icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
     
<section class="content">  
    <div class="row">
   <section class="col-lg-7 connectedSortable">       
     <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-cog icon-gradient bg-success">
            </i>รายการค่าพื้นฐานระบบ
            <div class="btn-actions-pane-right">
               
            </div>
        </div>
        <div class="card-body"> 
                  <div class="row justify-content-center">           
                       <div class="col-md-12">
                <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-hover" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CodeConfig") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                        <asp:BoundField DataField="CodeConfig" HeaderText="Code" />
                            <asp:BoundField HeaderText="Description" DataField="Description" />
                            <asp:BoundField HeaderText="Value" DataField="ValueConfig" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
                      </div>
                        </div>
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
   <section class="col-lg-5 connectedSortable">
          <div class="main-card mb-3 card">
    <div class="card-header">
        <i class="header-icon lnr-cog icon-gradient bg-primary">
            </i>แก้ไขการตั้งค่า<asp:HiddenField ID="hdUID" runat="server" />
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
            
      <div class="row">     
          
        <div class="col-md-12">
          <div class="form-group">
            <label>Code</label>
            <asp:label ID="lblCode" runat="server" cssclass="form-control text-center" ></asp:label>
          </div>

        </div>
               <div class="col-md-12">
          <div class="form-group">
            <label>Descriptions</label>
               <asp:label ID="lblDesc" runat="server" cssclass="form-control text-center" placeholder=""></asp:label>
          </div>
        </div>                

        <div class="col-md-12">
          <div class="form-group">
            <label>Value</label>
            <asp:TextBox ID="txtValue" runat="server" cssclass="form-control text-center" placeholder=""></asp:TextBox>
          </div>
        </div> 
  </div>                
     <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" /> 
        </div>
      </div>
            </div>
  </div>
    </section>
        </div> 
</section>
</asp:Content>
