﻿Imports System.Net.Mail

Public Class Chart5
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Dim dtDv As New DataTable

    Public Shared catebar1 As String
    Public Shared databar1L As String
    Public Shared databar1R As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            If Not IsNothing(Request.Cookies("Ergo")) Then

                dtDv = ctlR.RPT_MSD(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")))

                catebar1 = ""
                databar1L = ""
                databar1R = ""

                For i = 0 To dtDv.Rows.Count - 1
                    catebar1 = catebar1 + "'" + dtDv.Rows(i)("Descriptions") + "'"
                    databar1L = databar1L + dtDv.Rows(i)("L").ToString()
                    databar1R = databar1R + dtDv.Rows(i)("R").ToString()

                    If i < dtDv.Rows.Count - 1 Then
                        catebar1 = catebar1 + ","
                        databar1L = databar1L + ","
                        databar1R = databar1R + ","
                    End If
                Next

                Label1.Text = "(" & dtDv.Rows(0)("nCount") & ")"
            End If
        End If
    End Sub
End Class