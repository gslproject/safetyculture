﻿Imports System.Net.Mail

Public Class Chart1
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Dim dtDv As New DataTable
    Dim dtDp As New DataTable

    Public Shared catebar1 As String
    Public Shared catebar2 As String
    Public Shared datapie As String
    Public Shared databar1All As String
    Public Shared databar1Asm As String
    Public Shared databar2All As String
    Public Shared databar2Asm As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            If Not IsNothing(Request.Cookies("Ergo")) Then
                datapie = ""
                datapie = ctlR.RPT_PersonBodyRisk_GetCount(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")))

                dtDv = ctlR.RPT_PersonBodyRisk_GetCountByDivision(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")))

                catebar1 = ""
                databar1All = ""
                databar1Asm = ""

                For i = 0 To dtDv.Rows.Count - 1
                    catebar1 = catebar1 + "'" + dtDv.Rows(i)("DivisionName") + "'"
                    databar1All = databar1All + dtDv.Rows(i)("nCount").ToString()
                    databar1Asm = databar1Asm + dtDv.Rows(i)("AsmCount").ToString()

                    If i < dtDv.Rows.Count - 1 Then
                        catebar1 = catebar1 + ","
                        databar1All = databar1All + ","
                        databar1Asm = databar1Asm + ","
                    End If
                Next


                dtDp = ctlR.RPT_PersonBodyRisk_GetCountByDepartment(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")))

                catebar2 = ""
                databar2All = ""
                databar2Asm = ""

                For i = 0 To dtDp.Rows.Count - 1
                    catebar2 = catebar2 + "'" + dtDp.Rows(i)("DepartmentName") + "'"
                    databar2All = databar2All + dtDp.Rows(i)("nCount").ToString()
                    databar2Asm = databar2Asm + dtDp.Rows(i)("AsmCount").ToString()

                    If i < dtDp.Rows.Count - 1 Then
                        catebar2 = catebar2 + ","
                        databar2All = databar2All + ","
                        databar2Asm = databar2Asm + ","
                    End If
                Next

            End If
        End If
    End Sub
End Class