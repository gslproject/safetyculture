﻿Public Class ChartMenu
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtYear As New DataTable
    Public isAsm, isAsm360 As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        isAsm = False

        dtYear = ctlR.ReportAssessment_GetYear(Request.Cookies("SafetyCS")("LoginCompanyUID"))

        If dtYear.Rows.Count > 0 Then
            isAsm = True
        End If


    End Sub

End Class