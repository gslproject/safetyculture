﻿Public Class AsmMenu
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessmentController
    Dim ctlCfg As New SystemConfigController
    Dim dt As New DataTable
    Public isAsm, isAsm360 As Boolean
    Public Asm360 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        isAsm = False

        'ตรวจสอบระยะเวลาเปิดประเมินประจำปี
        Dim Bdate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetValueByCode(Request.Cookies("SafetyCS")("LoginCompanyUID"), CFG_STARTDATE)))
        Dim Edate As Integer = StrNull2Zero(ConvertStrDate2DBString(ctlCfg.SystemConfig_GetValueByCode(Request.Cookies("SafetyCS")("LoginCompanyUID"), CFG_ENDDATE)))

        Dim sToday As Integer = StrNull2Zero(ConvertDate2DBString(ctlCfg.GET_DATE_SERVER))

        Dim bAvailable As Boolean

        If sToday < Bdate Then
            bAvailable = False
        ElseIf sToday > Edate Then
            bAvailable = False
        Else
            bAvailable = True
        End If

        If bAvailable = False Then
            Response.Redirect("AlertPage.aspx")
        Else
            dt = ctlA.Assessment_GetByAssessorUID(Request.Cookies("SafetyCS")("ASSMYEAR"), Request.Cookies("SafetyCS")("userid"))
            If dt.Rows.Count > 0 Then
                isAsm = True
            End If

            Dim dtA As New DataTable
            Dim ctlM As New MasterController
            Dim AsmYear As Integer = ctlM.AsmYear_Get()

            dtA = ctlA.Assessor_GetDetail(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
            If dtA.Rows.Count > 0 Then
                Select Case dtA.Rows(0)("EmployeeLevelID")
                    Case "1"
                        Asm360 = ctlA.Assessee_GetStatusByTop(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                    Case "2"
                        Asm360 = ctlA.Assessee_GetStatusByManager(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                    Case "3"
                        Asm360 = ctlA.Assessee_GetStatusBySupervisor(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                    Case Else
                        Asm360 = ctlA.Assessee_GetStatusByEmployee(AsmYear, Request.Cookies("SafetyCS")("LoginPersonUID"))
                End Select
                'dtEmp.DefaultView.Sort = "AssesseeType"
            End If


        End If
    End Sub

End Class