﻿Public Class SafetyLeadershipReport
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtScore As New DataTable

    Dim dtDp As New DataTable

    Public Shared catebar As String
    Public Shared databar As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        'If Not IsPostBack Then
        If Not Request("EMP") = Nothing Then
            LoadPerson()
            LoadAssessmentCount()
            LoadAssessment()
            LoadChart()


            'Else
            '    CompanyPackage = ctlC.Company_GetPackage(Request.Cookies("SafetyCS")("LoginCompanyUID"))
        End If
        'End If

        'Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
        'cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub
    Private Sub LoadAssessmentCount()
        Dim dtA As New DataTable
        Dim ctlA As New UserController
        Dim ctlM As New MasterController
        Dim AsmYear As Integer = ctlM.AsmYear_Get()

        dtA = ctlA.User_GetByUserID(Request("EMP"))
        If dtA.Rows.Count > 0 Then
            Select Case dtA.Rows(0)("EmployeeLevelID")
                Case "1"
                    dtDp = ctlR.Assessment360_AssessmentCountByTop(AsmYear, Request("EMP"))
                Case "2"
                    dtDp = ctlR.Assessment360_AssessmentCountByManager(AsmYear, Request("EMP"))
                Case "3"
                    dtDp = ctlR.Assessment360_AssessmentCountBySupervisor(AsmYear, Request("EMP"))
            End Select
        End If


        If dtDp.Rows.Count > 0 Then
            lblAsmCount.Text = dtDp.Rows(0)("AsmCount") & "/" & dtDp.Rows(0)("AssessorCount")
        End If
    End Sub

    Private Sub LoadPerson()
        Dim dtE As New DataTable
        Dim ctlP As New UserController
        dtE = ctlP.User_GetByUserID(Request("EMP"))
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                hdEmpUID.Value = .Item("UserID")
                hdCompUID.Value = .Item("CompanyUID")
                lblCode.Text = String.Concat(.Item("Code"))
                lblEmployeeName.Text = String.Concat(.Item("EmployeeName"))
                lblPositionName.Text = String.Concat(.Item("PositionName"))
                lblDepartmentName.Text = String.Concat(.Item("DepartmentName"))
                lblDivisionName.Text = String.Concat(.Item("DivisionName"))
            End With
        End If
    End Sub

    Private Sub LoadAssessment()

        dtScore = ctlR.RPT_Assessment360_ScoreByEmployee(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), Request("EMP"))

        If dtScore.Rows.Count > 0 Then
            With dtScore.Rows(0)

                For i = 0 To dtScore.Rows.Count - 1
                    Select Case dtScore.Rows(i)("Code")
                        Case "1"
                            lblQ1.Text = dtScore.Rows(i)("AvgScore")
                        Case "2"
                            lblQ2.Text = dtScore.Rows(i)("AvgScore")
                        Case "3"
                            lblQ3.Text = dtScore.Rows(i)("AvgScore")
                        Case "4"
                            lblQ4.Text = dtScore.Rows(i)("AvgScore")
                        Case "5"
                            lblQ5.Text = dtScore.Rows(i)("AvgScore")
                        Case "6"
                            lblQ6.Text = dtScore.Rows(i)("AvgScore")
                        Case "7"
                            lblQ7.Text = dtScore.Rows(i)("AvgScore")
                        Case "8"
                            lblQ8.Text = dtScore.Rows(i)("AvgScore")
                        Case "9"
                            lblQ9.Text = dtScore.Rows(i)("AvgScore")
                    End Select
                Next

                lblComment.Text = String.Concat(.Item("Comment"))

            End With

        End If
    End Sub

    Private Sub LoadChart()

        dtDp = ctlR.RPT_Assessment360_ScoreByEmployee(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), Request("EMP"))

        catebar = ""
        databar = ""

        For i = 0 To dtDp.Rows.Count - 1
            catebar = catebar + "'" + dtDp.Rows(i)("Descriptions") + "'"
            databar = databar + dtDp.Rows(i)("AvgScore").ToString()

            If i < dtDp.Rows.Count - 1 Then
                catebar = catebar + ","
                databar = databar + ","
            End If
        Next
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=personal&comp=" & hdCompUID.Value & "&y=" & Request.Cookies("SafetyCS")("ASSMYEAR") & "&emp=" & hdEmpUID.Value & "&RPTTYPE=PDF','_blank');", True)
    End Sub
End Class