﻿Public Class ResultPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        Select Case Request("p")
            Case "person"
                hlkBack.NavigateUrl = "Person?ActionType=emp"
            Case "bio"
                hlkBack.NavigateUrl = "Home?ActionType=h"
            Case "ckpwd"
                lblMsg.Text = "ระบบเปลี่ยนรหัสผ่านให้ท่านใหม่เรียบร้อยแล้ว " & vbCrLf & "ในการเข้าระบบครั้งต่อไปท่านต้องใส่รหัสผ่านใหม่ในการเข้าระบบ"
                hlkBack.NavigateUrl = "Home?ActionType=h"
            Case Else
                hlkBack.Visible = False
        End Select


    End Sub

End Class