﻿Imports System.Net.Mail

Public Class Home
    Inherits System.Web.UI.Page

    Dim ctlR As New ReportController
    Dim dt As New DataTable
    Public Shared datachart As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            If Not IsNothing(Request.Cookies("SafetyCS")) Then
                'SendAlertOwner()

                If IsNothing(Request.Cookies("SafetyCS")) Then
                    Response.Redirect("Default.aspx")
                End If

                dt = ctlR.RPT_Survey_Engagement(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                datachart = dt.Rows(0)(0).ToString()   'JsonConvert.SerializeObject(dt, Formatting.None)


            End If
        End If
    End Sub

    'Private Sub SendAlertOwner()
    '    Dim dt As New DataTable

    '    dt = ctlT.TaskOwner_GetEmailAlert(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))
    '    If dt.Rows.Count > 0 Then
    '        For i = 0 To dt.Rows.Count - 1
    '            If String.Concat(dt.Rows(i)("Email")) <> "" Then
    '                SendMailAlert(dt.Rows(i)("CompanyUID"), dt.Rows(i)("TaskUID"), dt.Rows(i)("PersonUID"), dt.Rows(i)("PersonName"), dt.Rows(i)("DueDate"), dt.Rows(i)("Email"))
    '                ctlT.SendAlert_Save(dt.Rows(i)("CompanyUID"), dt.Rows(i)("TaskUID"), dt.Rows(i)("PersonUID"), dt.Rows(i)("Email"), "N")
    '            End If
    '        Next
    '    End If

    'End Sub
    Private Sub SendMailAlert(CompanyUID As Integer, TaskUID As Integer, PersonUID As Integer, PersonName As String, StartDate As String, ByVal sTo As String)
        Try

            Dim MySubject As String = "แจ้งเตือน Task ใกล้ถึง Duedate"
            Dim MyMessageBody As String = ""

            MyMessageBody = "<p>เรียน คุณ" & PersonName & "<br />  นี่คืออีเมล์แจ้งเตือนอัตโนมัติจากระบบ Safety culture survey  ท่านมี Task Action ที่ใกล้ถึงวันกำหนด ในวันที่ " & StartDate & " <br />"

            MyMessageBody &= "  รายละเอียดเพิ่มเติมสามารถดูได้ที่เว็บไซต์ www.easyergoscanner.com <br/> "

            Dim RecipientEmail As String = sTo
            Dim SenderEmail As String = "thaiSafetyCS@gmail.com"
            Dim SenderDisplayName As String = "Safety culture survey"
            Dim SenderEmailPassword As String = "Qazxsw21"

            Dim HOST = "smtp.gmail.com"
            Dim PORT = "587" 'TLS Port

            Dim mail As New MailMessage
            mail.Subject = MySubject
            mail.Body = MyMessageBody

            mail.IsBodyHtml = True
            mail.To.Add(RecipientEmail)
            mail.From = New MailAddress(SenderEmail, SenderDisplayName)

            Dim SMTP As New SmtpClient(HOST)
            SMTP.EnableSsl = True
            SMTP.Credentials = New System.Net.NetworkCredential(SenderEmail.Trim(), SenderEmailPassword.Trim())
            SMTP.DeliveryMethod = SmtpDeliveryMethod.Network
            SMTP.Port = PORT
            SMTP.Send(mail)
            'MsgBox("Sent Message To : " & RecipientEmail, MsgBoxStyle.Information, "Sent!")

            Dim ctlM As New MasterController
            ctlM.SendAlert_UpdateStatus(CompanyUID, TaskUID, PersonUID, "Y")
        Catch ex As Exception
            'DisplayMessage(Me.Page, ex.Message)

        End Try
        '(4) Send the MailMessage (will use the Web.config settings)



    End Sub


End Class