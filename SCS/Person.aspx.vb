﻿Imports System.Data
Public Class Person
    Inherits System.Web.UI.Page
    Dim ctlEmp As New PersonController
    Public dtEmp As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        If Request.Cookies("SafetyCS")("ROLE_ADM") = False And Request.Cookies("SafetyCS")("ROLE_SPA") = False Then
            dtEmp = ctlEmp.Person_GetByCompanyUID(Request.Cookies("SafetyCS")("LoginCompanyUID"))
        Else
            dtEmp = ctlEmp.Person_GetAll
        End If
    End Sub
End Class