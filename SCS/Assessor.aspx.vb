﻿Imports System.Data
Public Class Assessor
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessorController
    Public dtAssr As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        dtAssr = ctlA.Assessee_Get(Request.Cookies("SafetyCS")("LoginCompanyUID"))
    End Sub
End Class