﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="OfficialSite.Master" CodeBehind="RegisterModify.aspx.vb" Inherits=".RegisterModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
      <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-light icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>จัดการข่าวประชาสัมพันธ์
                                    <div class="page-title-subheading">จัดการข้อมูลเกี่ยวกับองค์กร บนหน้าเพจเว็บไซต์</div>
                                </div>
                            </div>
                        </div>
                    </div>      

<section class="content">                    
     <div class="main-card mb-3 card">
        <div class="card-header text-blue">ข้อมูลผู้ลงทะเบียนขอใช้งาน</div>       
            <div class="card-body">     
     <div class="row"> 
                <asp:HiddenField ID="hdRegisterID" runat="server" />
     <div class="col-md-6">
                <div class="form-group">
                  <label>ชื่อผู้ติดต่อ<span class="text-red">*</span></label>
                   <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control"  Width="100%" ></asp:TextBox>
                </div>
              </div>
     <div class="col-md-6">
                <div class="form-group">
                  <label>ประเภท</label>
                    <asp:RadioButtonList ID="optPersonType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="P">บุคคลธรรมดา</asp:ListItem>
                        <asp:ListItem Value="C">บริษัท/หน่วยงาน</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
              </div>

      </div>
   <div class="row">
         <div class="col-md-6">
                <div class="form-group">
                  <label>ชื่อบริษัท</label>
                  <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
              </div> 
           <div class="col-md-6">
                <div class="form-group">
                  <label>ประเภทธุรกิจ</label><br />
                  <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="form-control select2">                 
                  </asp:DropDownList>
                </div>
              </div>
</div>
   <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>เบอร์ติดต่อ<span class="text-red">*</span></label>
                   <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
              </div>   
                <div class="col-md-4">
                <div class="form-group">
                  <label>อีเมล์</label>
                  <asp:TextBox ID="txtMail" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
              </div>
                      <div class="col-md-4">
                <div class="form-group">
                  <label>Website</label>
                  <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
                </div>
  <div class="text-bold text-blue">ที่อยู่</div>  
            <div class="row">
                        
              <div class="col-md-3">
                <div class="form-group">
                  <label>เลขที่</label>
                   <asp:TextBox ID="txtAddressNo" runat="server" CssClass="form-control text-center" MaxLength="50"></asp:TextBox>
                </div>
              </div>
                  <div class="col-md-3">
                <div class="form-group">
                  <label>หมู่</label>
                   <asp:TextBox ID="txtMoo" runat="server" CssClass="form-control text-center" MaxLength="50"></asp:TextBox>
                </div>
              </div>
                  <div class="col-md-3">
                <div class="form-group">
                  <label>ซอย</label>
                   <asp:TextBox ID="txtSoi" runat="server" CssClass="form-control text-center" MaxLength="50"></asp:TextBox>
                </div>
              </div>
                  <div class="col-md-3">
                <div class="form-group">
                  <label>ถนน</label>
                   <asp:TextBox ID="txtRoad" runat="server" CssClass="form-control text-center" MaxLength="50"></asp:TextBox>
                </div>
              </div>
                  <div class="col-md-3">
                <div class="form-group">
                  <label>ตำบล/แขวง</label>
                   <asp:TextBox ID="txtTumbol" runat="server" CssClass="form-control text-center"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อำเภอ/เขต</label>
                  <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>จังหวัด</label><br />
                   <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control select2">
                  </asp:DropDownList>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>รหัสไปรษณีย์</label>
            <asp:TextBox ID="txtZipcode" runat="server" CssClass="form-control text-center" MaxLength="5"></asp:TextBox>
                </div>
              </div>  
   </div> 
               <div class="row">
                 <div class="text-bold text-blue">ชื่อและที่อยู่สำหรับออกใบเสร็จ</div>           
              <div class="col-md-12">
                <div class="form-group">
                   <asp:TextBox ID="txtAddress4Inv" runat="server" CssClass="form-control" TextMode="MultiLine" Height="120"></asp:TextBox>
                </div>
              </div>
               
   </div> 
                   <div class="row"> 
               <div class="text-bold text-blue">โปรแกรมที่สนใจขอใช้งาน</div>
              <div class="col-md-12">
                <div class="form-group">                 
                    <asp:CheckBox ID="chkLA" runat="server" Text="โปรแกรมกฎหมาย (i-LA)" /><br />
                     <asp:CheckBox ID="chkErgo" runat="server" Text="Ergonomic" /><br />
                     <asp:CheckBox ID="chkIncident" runat="server" Text="i-Incident" /><br />
                      <asp:CheckBox ID="chkSafety" runat="server" Text="Safety Culture Survey" /><br />
                </div>
              </div>
            </div>    
                <div class="row justify-content-center">  
 <div align="center">
<asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="บันทึก" Width="100px" />
        </div>
                    </div>
    </div>
</div>
</section>
</asp:Content>
 

