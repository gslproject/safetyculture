﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportPersonal.aspx.vb" Inherits="SCS.ReportPersonal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
    <h1>Personal Report<small></small></h1>
  </section>
  <section class="content">
    <div class="card card-solid">      
      <div class="card-body">
     <div class="row">
         <div class="col-md-3">

         </div>

        <div class="col-md-6" style="background-color:#14539a;color: white">
          <br /> 
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Company</label>
                <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
                </asp:DropDownList>
              </div>
            </div>
              </div>
           <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Person</label>
                <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2" Width="100%">
                </asp:DropDownList>
              </div>
            </div>
          </div> 
       
          <div class="row">
            <div class="col-md-12 text-center">
              <asp:Button ID="cmdPrint" runat="server" CssClass="btn btn-info" Width="120px" Text="Print Preview" />
            </div>
          </div>
          <br /> 
        </div>
        
         <div class="col-md-3"></div>
         </div>
      </div>
    
    </div>
  </section>
</asp:Content>