﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PersonModify.aspx.vb" Inherits="SCS.PersonModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-attention icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Person
                            <div class="page-title-subheading">ข้อมูลบุคคล/พนักงาน</div>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Main content -->   
      <section class="content"> 
           <asp:Label ID="lblAlert1" runat="server" cssclass="alert alert-danger alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  

<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Information<asp:HiddenField ID="hdPersonUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Code</label>
            <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center" placeholder="รหัสพนักงาน"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Prefix</label>
               <asp:DropDownList ID="ddlPrefix" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>
        </div>                

        <div class="col-md-3">
          <div class="form-group">
            <label>Sex</label>
            <asp:RadioButtonList ID="optSex" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="M">Male</asp:ListItem>
              <asp:ListItem Value="F">Female</asp:ListItem>
            </asp:RadioButtonList>
          </div>
        </div>
        <div class="col-md-1">
          <div class="form-group">
            <label>Age</label>
            <asp:TextBox ID="txtAge" runat="server" MaxLength="3" cssclass="form-control text-center" placeholder="อายุ"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Card ID.</label>
            <asp:TextBox ID="txtCardID" runat="server" MaxLength="13" cssclass="form-control text-center" placeholder="เลขประจำตัวประชาชน"></asp:TextBox>
          </div>

        </div> 

      </div>
      

      <div class="row">

        <div class="col-md-6">
          <div class="form-group">
            <label>ชื่อ</label>
            <asp:TextBox ID="txtFirstNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>นามสกุล</label>
            <asp:TextBox ID="txtLastNameTH" runat="server" cssclass="form-control" placeholder="นามสกุลภาษาไทย"></asp:TextBox>
          </div>
        </div>  
             <div class="col-md-3">
          <div class="form-group">
            <label>Telephone</label>
            <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>E-mail</label>
            <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
          </div>
        </div>
      </div>    
    </div>
  </div>
  </section>
</div>   
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Job Description</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                    <div class="row">

         <div class="col-md-9">
          <div class="form-group">
            <label>Company</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>
       <div class="col-md-3">
          <div class="form-group">
            <label>Status</label>
            <asp:DropDownList ID="ddlWorkStatus" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Selected="True" Value="1">ยังทำงานอยู่</asp:ListItem>
                <asp:ListItem Value="2">พ้นสภาพ</asp:ListItem>
                <asp:ListItem Value="0">ลาออก</asp:ListItem>
            </asp:DropDownList>
          </div>

        </div>

                        </div>
                  <div class="row">    
        <div class="col-md-4">
          <div class="form-group">
            <label>Department</label>
            <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>       
                       </div> 
        <div class="box-footer clearfix"> 
            </div>
          </div>        
   </div>
        </section>
                </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" Checked="True" />
          </div>
        </div>
        <div class="col-md-6">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdDel" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
        </div>
      </div>
 <asp:Label ID="lblAlert2" runat="server" cssclass="alert alert-error alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  
          </section>
</asp:Content>