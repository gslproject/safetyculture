﻿'Imports Newtonsoft.Json

Public Class Chart2All
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Dim dtTable As New DataTable
    'Dim dtDv As New DataTable
    Dim dtDp As New DataTable

    Public Shared catebar1 As String
    Public Shared catebar2 As String
    Public Shared databar1 As String
    Public Shared databar2 As String

    'Public Shared databar2All As String
    'Public Shared databar2Asm As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default")
        End If

        If Not IsPostBack Then
            If Request("y") = "0" Then
                lblYear.Text = "Summary"
            Else
                lblYear.Text = Request("y")
            End If


            If Not IsNothing(Request.Cookies("SafetyCS")) Then
                databar1 = ""
                Try


                    dtTable = ctlR.RPT_Assessment_GetSumaryScoreByYear(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))
                    grdData.DataSource = dtTable
                    grdData.DataBind()

                    'dtDv = ctlR.RPT_Assessment_AvgScore(StrNull2Zero(Request.Cookies("SafetyCS")("ASSMYEAR")), StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                    'catebar1 = ""
                    'databar1 = ""

                    'For i = 0 To dtDv.Rows.Count - 1
                    '    catebar1 = catebar1 & "'Q" & dtDv.Rows(i)("Code") & "'"
                    '    databar1 = databar1 & dtDv.Rows(i)("AvgScore").ToString()

                    '    If i < dtDv.Rows.Count - 1 Then
                    '        catebar1 = catebar1 & ","
                    '        databar1 = databar1 & ","
                    '    End If
                    'Next

                    dtDp = ctlR.RPT_Assessment_AvgScoreByYear(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))

                    Dim sName As String
                    Dim sData As String

                    'databar2 = JsonConvert.SerializeObject(dtDp, Formatting.None)

                    catebar2 = ""
                    databar2 = ""

                    sName = dtDp.Rows(0)("AsmYear").ToString()
                    sData = "{" & vbCrLf & "name: '" & sName & "'," & vbCrLf & "data:["


                    For i = 0 To dtDp.Rows.Count - 1

                        If sName = String.Concat(dtDp.Rows(i)("AsmYear")) Then
                            sData = sData & dtDp.Rows(i)("Score").ToString()
                        Else
                            sName = dtDp.Rows(i)("AsmYear").ToString()
                            sData = Left(sData, Len(sData) - 1)
                            sData = sData & "]" & vbCrLf & "}," & vbCrLf & "{" & vbCrLf & "name: '" & sName & "'," & vbCrLf & "data:[" & dtDp.Rows(i)("Score").ToString()

                        End If

                        'catebar2 = catebar2 & "'" & dtDp.Rows(i)("EvaluationGroup") & "'"

                        If i < dtDp.Rows.Count - 1 Then
                            sData = sData & ","
                        End If
                    Next

                    databar2 = sData & "]" & vbCrLf & "}"
                    catebar2 = "'Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8'"

                    cmdExport.Visible = True
                Catch ex As Exception
                    catebar1 = ""
                    databar1 = ""
                    catebar2 = ""
                    databar2 = ""
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ยังไม่พบการประเมิน กรุณาเข้ามาดูรายงานใหม่อีกครั้งหลังพนักงานเริ่มทำการประเมิน');", True)
                    cmdExport.Visible = False
                End Try
            End If
        End If
    End Sub
    Protected Sub cmdExport_Click(sender As Object, e As EventArgs) Handles cmdExport.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('ReportViewer?r=ssg&comp=" & Request.Cookies("SafetyCS")("LoginCompanyUID") & "&y=" & Request.Cookies("SafetyCS")("ASSMYEAR") & "&RPTTYPE=EXCEL','_blank');", True)
    End Sub
End Class