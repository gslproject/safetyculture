﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmMenu.aspx.vb" Inherits="SCS.AsmMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-medal icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Safety Culture survey 
                                    <div class="page-title-subheading">เลือกแบบสำรวจ</div>
                                </div>
                            </div>
                        </div>
</div>
 

<section class="content">        
     <div class="row">
    
        <div class="col-lg-6 col-xs-6">
       <% If Me.isAsm = False Then %><a  href="AsmSCS.aspx?m=a" class="menu-banner">  <% End If %>   
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>Safety Culture survey</h3>
              <p>แบบสํารวจวัฒนธรรมความปลอดภัย</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-checkmark-circle"></i>
            </div>
            <div class="small-box-footer"><% If Me.isAsm = True Then %> Complete <% Else %> Pending <% End If %></div>
          </div>
              <% If Me.isAsm = False Then %>  </a> <% End If %>
        </div>
  
        <div class="col-lg-6 col-xs-6">
           <a  href="AsmList.aspx?m=a" class="menu-banner" >     
          <div class="small-box bg-green">
            <div class="inner">
              <h3>360 Degree</h3>
              <p>แบบสํารวจความเป็นผู้นำด้านความปลอดภัย</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-locate"></i>
            </div>
               <div class="small-box-footer"><% =Me.Asm360 %> </div>
          </div>
               </a>
        </div>
  
        
      
         </div>
     

    </section>
</asp:Content>
