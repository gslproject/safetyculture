﻿Public Class ChangePassword
    Inherits System.Web.UI.Page

    Dim acc As New UserController
    Dim enc As New CryptographyEngine

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblError.Visible = False
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtOld.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ท่านยังไม่ได้ระบุรหัสผ่านเดิม');", True)
            Exit Sub
        End If
        If Request.Cookies("SafetyCS")("Password") <> txtOld.Text Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','รหัสผ่านเดิมท่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If
        If txtNew.Text <> txtRe.Text Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ท่านยืนยันรหัสผ่านใหม่ไม่ตรงกัน กรุณาลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If
        acc.User_ChangePassword(Request.Cookies("SafetyCS")("username"), enc.EncryptString(txtNew.Text, True))

        acc.User_GenLogfile(Request.Cookies("SafetyCS")("Username"), ACTTYPE_UPD, "Users", "Change password", "")

        Response.Redirect("ResultPage?p=ckpwd")
    End Sub
End Class