﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ResultPage.aspx.vb" Inherits="SCS.ResultPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <!-- Main content -->
    <section class="content">

          <div class="error-page">
        <h2 class="headline text-green"><i class="fa fa-info-circle text-green"></i></h2>

        <div class="error-content"><br /><br />
          <h3>Success.
              <br />บันทึกข้อมูลสำเร็จ</h3>
          <p> <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label> 
             </p>   
            <br />
                <asp:HyperLink ID="hlkBack" class="btn btn-primary" runat="server"> <i class="fa fa-backward text-primary"></i>กลับหน้าหลัก</asp:HyperLink>
                 
        </div>
      </div>

    </section>


</asp:Content>
