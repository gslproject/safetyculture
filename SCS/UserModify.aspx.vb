﻿Public Class UserModify
    Inherits System.Web.UI.Page
    Dim ctlC As New CompanyController
    Dim ctlU As New UserController
    Dim ctlR As New UserRoleController
    Dim enc As New CryptographyEngine
    Dim ctlM As New MasterController
    Dim CompanyPackage As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblUserID.Text = ""
            cmdDelete.Enabled = False
            lblAlert.Visible = False
            pnUser.Visible = False
            If Not Request("uid") = Nothing Then
                CompanyPackage = ctlC.PackageNo_GetByUserID(Request("uid"))
            Else
                CompanyPackage = ctlC.Company_GetPackage(Request.Cookies("SafetyCS")("LoginCompanyUID"))
            End If

            LoadCompany()
            BindUserGroup()
            LoadPrefix()
            'LoadProvince()
            LoadDivision()
            LoadDepartment()
            LoadPosition()
            LoadLevel()
            'LoadPerson()


            If Request.Cookies("SafetyCS")("ROLE_SPA") = True Then
                ddlCompany.Enabled = True
                pnUser.Visible = True
            Else
                ddlCompany.Enabled = False
            End If


            If Not Request("uid") = Nothing Then
                EditUser()
            End If
        End If

        If Request.Cookies("SafetyCS")("ROLE_SPA") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
            pnUser.Visible = True
        Else
            pnUser.Visible = False
        End If


        Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub
    Private Sub LoadPrefix()
        ddlPrefix.DataSource = ctlM.Prefix_GetActive()
        ddlPrefix.DataTextField = "Name"
        ddlPrefix.DataValueField = "UID"
        ddlPrefix.DataBind()
    End Sub

    'Private Sub LoadProvince()
    '    ddlProvince.DataSource = ctlM.Province_Get()
    '    ddlProvince.DataTextField = "ProvinceName"
    '    ddlProvince.DataValueField = "ProvinceID"
    '    ddlProvince.DataBind()
    'End Sub
    Private Sub LoadDivision()
        ddlDivision.DataSource = ctlM.Division_Get(StrNull2Zero(ddlCompany.SelectedValue))
        ddlDivision.DataTextField = "DivisionName"
        ddlDivision.DataValueField = "DivisionUID"
        ddlDivision.DataBind()
    End Sub
    Private Sub LoadDepartment()
        ddlDepartment.DataSource = ctlM.Department_GetByDivisionUID(StrNull2Zero(ddlCompany.SelectedValue), ddlDivision.SelectedValue)
        ddlDepartment.DataTextField = "DepartmentName"
        ddlDepartment.DataValueField = "DepartmentUID"
        ddlDepartment.DataBind()
    End Sub
    Private Sub LoadPosition()
        ddlPosition.DataSource = ctlM.Position_Get(StrNull2Zero(ddlCompany.SelectedValue)) 'Request.Cookies("SafetyCS")("LoginCompanyUID"))
        ddlPosition.DataTextField = "PositionName"
        ddlPosition.DataValueField = "PositionUID"
        ddlPosition.DataBind()
    End Sub

    Private Sub LoadCompany()
        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("SafetyCS")("LoginCompanyUID")
    End Sub

    Private Sub LoadLevel()

        ddlLevel.DataSource = ctlM.EmployeeLevel_Get()
        ddlLevel.DataTextField = "LevelName"
        ddlLevel.DataValueField = "LevelUID"
        ddlLevel.DataBind()
    End Sub

    Private Sub BindUserGroup()
        'Dim dtG As New DataTable
        'Dim ctlG As New UserRoleController
        'dtG = ctlG.UserGroup_Get()
        With chkGroup
            .Items.Clear()

            .Items.Add("Customer Admin")
            .Items(0).Value = "1"
                    .Items.Add("User")
                    .Items(1).Value = "2"

                If Request.Cookies("SafetyCS")("ROLE_SPA") = True Then
                    .Items.Add("NPC-SE Administrator")
                    .Items(2).Value = "9"
                End If


        End With
    End Sub


    'Private Sub LoadCompany()
    '    Dim ctlC As New CompanyController
    '    ddlCompany.DataSource = ctlC.Company_GetActive()
    '    ddlCompany.DataTextField = "CompanyName"
    '    ddlCompany.DataValueField = "UID"
    '    ddlCompany.DataBind()
    'End Sub

    'Private Sub EditUserCompanyToGrid()
    '    Dim dtC As New DataTable

    '    If lblUserID.Text <> "" Then
    '        dtC = ctlU.UserCompany_GetByUserID(StrNull2Zero(lblUserID.Text))
    '    Else
    '        dtC = ctlU.UserCompany_GetByUsername(txtUsername.Text)
    '    End If


    '    If dtC.Rows.Count > 0 Then
    '        With grdCompany
    '            .Visible = True
    '            .DataSource = dtC
    '            .DataBind()
    '        End With
    '    Else
    '        grdCompany.DataSource = Nothing
    '        grdCompany.Visible = False
    '    End If
    '    dtC = Nothing

    'End Sub

    Private Sub EditUser()
        Dim dt As New DataTable
        dt = ctlU.User_GetByUserID(Request("uid"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblUserID.Text = String.Concat(.Item("UserID"))
                ddlPrefix.SelectedValue = String.Concat(.Item("PrefixUID"))
                txtFNameTH.Text = String.Concat(.Item("FnameTH"))
                txtLNameTH.Text = String.Concat(.Item("LnameTH"))
                txtFnameEN.Text = String.Concat(.Item("FnameEN"))
                txtLnameEN.Text = String.Concat(.Item("LnameEN"))
                optSex.SelectedValue = String.Concat(.Item("Gender"))
                txtAge.Text = String.Concat(.Item("Age"))
                txtTel.Text = String.Concat(.Item("Telephone"))
                txtEmail.Text = String.Concat(.Item("Email"))
                ddlCompany.SelectedValue = String.Concat(.Item("CompanyUID"))
                ddlDivision.SelectedValue = String.Concat(.Item("DivisionUID"))
                LoadDepartment()
                ddlDepartment.SelectedValue = String.Concat(.Item("DepartmentUID"))
                ddlPosition.SelectedValue = String.Concat(.Item("PositionUID"))
                ddlLevel.SelectedValue = String.Concat(.Item("EmployeeLevelID"))


                txtStartDate.Text = String.Concat(.Item("StartWorkDate"))
                txtEndDate.Text = String.Concat(.Item("EndWorkDate"))

                Session("password") = ""
                txtUsername.Text = String.Concat(.Item("Username"))
                'txtPassword.Text = String.Concat(.Item("Passwords"))
                txtPassword.Text = "**********" ' enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
                Session("password") = enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                'chkGroup.SelectedValue = String.Concat(dt.Rows(0)("UserGroupUID"))

                EditUserGroup()
                cmdDelete.Enabled = True
            End With

        End If
    End Sub

    Private Sub EditUserGroup()
        Dim dtG As New DataTable
        Dim ctlR As New UserRoleController
        chkGroup.ClearSelection()

        dtG = ctlR.UserAccessGroup_Get(lblUserID.Text)
        If dtG.Rows.Count > 0 Then
            For i = 0 To dtG.Rows.Count - 1
                For n = 0 To chkGroup.Items.Count - 1
                    If dtG.Rows(i)("UserGroupUID") = chkGroup.Items(n).Value Then
                        chkGroup.Items(n).Selected = True
                    End If
                Next
            Next
        Else
            chkGroup.Items(1).Selected = True
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If ddlCompany.SelectedIndex = -1 Or ddlCompany.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกหน่วยงานก่อน');", True)
            Exit Sub
        End If

        If txtFNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกชื่อ');", True)
            Exit Sub
        End If
        If txtLNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกนามสกุล');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก E-mail');", True)
            Exit Sub
        End If

        If Request.Cookies("SafetyCS")("ROLE_SPA") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
            If txtUsername.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก Username');", True)
                Exit Sub
            End If
            If txtPassword.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Password');", True)
                Exit Sub
            End If

            If lblUserID.Text = "" Then
                If ctlU.User_CheckDuplicate(txtUsername.Text) Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username นี้มีในระบบแล้ว');", True)
                    Exit Sub
                End If
            End If
        End If

        Dim Password As String = ""
        If txtPassword.Text <> "**********" Then
            Password = enc.EncryptString(txtPassword.Text, True)
        Else
            Password = enc.EncryptString(Session("password"), True)
        End If

        Dim BWork, EWork As String

        BWork = txtStartDate.Text
        EWork = txtEndDate.Text

        If BWork = "" Or BWork = "dd/mm/yyyy" Then
            BWork = ""
        Else
            BWork = SetStrDate2DBDateFormat(BWork)
        End If


        If EWork = "" Or EWork = "dd/mm/yyyy" Then
            EWork = ""
        Else
            EWork = SetStrDate2DBDateFormat(EWork)
        End If

        ctlU.User_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, Password, StrNull2Zero(ddlPrefix.SelectedValue), txtFNameTH.Text, txtLNameTH.Text, txtFnameEN.Text, txtLnameEN.Text, optSex.SelectedValue, StrNull2Zero(txtAge.Text), txtTel.Text, txtEmail.Text, StrNull2Zero(ddlCompany.SelectedValue), StrNull2Zero(ddlDivision.SelectedValue), StrNull2Zero(ddlDepartment.SelectedValue), StrNull2Zero(ddlPosition.SelectedValue), StrNull2Zero(ddlLevel.SelectedValue), BWork, EWork, StrNull2Zero(ddlWorkStatus.SelectedValue), ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("SafetyCS")("userid"))


        Dim UserID As Integer
        UserID = ctlU.User_GetUID(txtUsername.Text)
        lblUserID.Text = UserID
        cmdDelete.Enabled = True

        If Request.Cookies("SafetyCS")("ROLE_SPA") = True Or Request.Cookies("SafetyCS")("ROLE_ADM") = True Then
            For i = 0 To chkGroup.Items.Count - 1
                ctlR.UserAccessGroup_Save(UserID, chkGroup.Items(i).Value, ConvertBoolean2StatusFlag(chkGroup.Items(i).Selected), Request.Cookies("SafetyCS")("uid"))
            Next
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub
    Private Sub ClearData()
        lblUserID.Text = ""
        txtFNameTH.Text = ""
        txtLNameTH.Text = ""
        txtUsername.Text = ""
        txtPassword.Text = ""
        txtTel.Text = ""
        txtEmail.Text = ""
        cmdDelete.Enabled = False
    End Sub
    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlU.User_Delete(lblUserID.Text)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','ลบข้อมูลเรียบร้อย');", True)

        Response.Redirect("Users?m=u&s=u")
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadDivision()
        LoadDepartment()
        LoadPosition()
    End Sub

    Protected Sub ddlDivision_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDivision.SelectedIndexChanged
        LoadDepartment()
    End Sub

    'Private Sub LoadPerson()
    '    Dim dtE As New DataTable
    '    Dim ctlE As New PersonController
    '    'If Request("uid") Is Nothing Then
    '    '    dtE = ctlE.Person_GetNotUser(Request.Cookies("SafetyCS")("LoginCompanyUID"))
    '    'Else
    '    'If Request.Cookies("SafetyCS")("ROLE_ADM") = True Or Request.Cookies("SafetyCS")("ROLE_SPA") = True Then
    '    '    dtE = ctlE.Person_GetForSelection("", "A")
    '    'Else

    '    'End If
    '    dtE = ctlE.Person_GetForSelection(StrNull2Zero(ddlCompany.SelectedValue), "A")

    '    'End If
    '    ddlPerson.DataSource = dtE
    '    ddlPerson.DataTextField = "PersonName"
    '    ddlPerson.DataValueField = "PersonUID"
    '    ddlPerson.DataBind()
    'End Sub
    'Protected Sub grdCompany_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCompany.RowCommand
    '    If TypeOf e.CommandSource Is WebControls.ImageButton Then
    '        Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
    '        Select Case ButtonPressed.ID
    '            Case "imgDel"
    '                If ctlU.UserCompany_Delete(e.CommandArgument) Then
    '                    ctlU.User_GenLogfile(Request.Cookies("SafetyCS")("username"), "DEL", "UserCompany", "ลบ UserCompany :" & e.CommandArgument, "")
    '                    EditUserCompanyToGrid()
    '                Else
    '                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
    '                End If
    '        End Select
    '    End If
    'End Sub

    'Protected Sub cmdAddCompany_Click(sender As Object, e As EventArgs) Handles cmdAddCompany.Click
    '    If ddlCompany.SelectedValue = "0" Then
    '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกบริษัทที่ให้สิทธิก่อน');", True)
    '        Exit Sub
    '    End If

    '    ctlU.UserCompany_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, ddlCompany.SelectedValue)
    '    ddlCompany.SelectedIndex =0
    '    EditUserCompanyToGrid()
    'End Sub
End Class