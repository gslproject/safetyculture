﻿<%@ Page Title="Employee" MetaDescription="จัดการข้อมูลพนักงาน" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserModify.aspx.vb" Inherits="SCS.UserModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
<section class="content">  

    <asp:Label ID="lblAlert" runat="server" cssclass="alert alert-danger alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  

<div class="row">
   <section class="col-lg-12 connectedSortable">
          <div class="main-card mb-3 card">
    <div class="card-header">
        <i class="header-icon lnr-user icon-gradient bg-primary">
            </i>Personal Information<asp:HiddenField ID="hdPersonUID" runat="server" />
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
            
      <div class="row">
            <div class="col-md-1">
          <div class="form-group">
            <label>User ID.</label>
              <asp:Label ID="lblUserID" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>
        </div>  
          
        <div class="col-md-2">
          <div class="form-group">
            <label>รหัสพนักงาน</label>
            <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center" placeholder="รหัสพนักงาน"></asp:TextBox>
          </div>

        </div>
               <div class="col-md-2">
          <div class="form-group">
            <label>Prefix</label>
               <asp:DropDownList ID="ddlPrefix" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>
        </div>                

        <div class="col-md-3">
          <div class="form-group">
            <label>Sex</label>
            <asp:RadioButtonList ID="optSex" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="M">Male</asp:ListItem>
              <asp:ListItem Value="F">Female</asp:ListItem>
            </asp:RadioButtonList>
          </div>
        </div>
        <div class="col-md-1">
          <div class="form-group">
            <label>Age</label>
            <asp:TextBox ID="txtAge" runat="server" MaxLength="3" cssclass="form-control text-center" placeholder="อายุ"></asp:TextBox>
          </div>

        </div>
  </div>
             <div class="row">
          <div class="col-md-3">
          <div class="form-group">
            <label>ชื่อ (ภาษาไทย)</label>
            <asp:TextBox ID="txtFNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>นามสกุล (ภาษาไทย)</label>
            <asp:TextBox ID="txtLNameTH" runat="server" cssclass="form-control" placeholder="นามสกุล"></asp:TextBox>          
          </div>
        </div> 
            <div class="col-md-3">
          <div class="form-group">
            <label>Firstname (English)</label>
            <asp:TextBox ID="txtFnameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Lastname (English)</label>
            <asp:TextBox ID="txtLnameEN" runat="server" cssclass="form-control" placeholder="English Surname"></asp:TextBox>          
          </div>
        </div> 
      </div>
      <div class="row">          
           <div class="col-md-3">
          <div class="form-group">
            <label>เบอร์โทร</label>
            <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>E-mail</label>
            <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
          </div>
        </div>
      </div>             
                           
    </div>
  </div>
  </section>
</div>   
            <div class="row">
    <section class="col-lg-12 connectedSortable">       

                 <div class="main-card mb-3 card">
    <div class="card-header">
        <i class="header-icon lnr-user icon-gradient bg-primary">
            </i>Job Description
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
                <div class="row">          
         <div class="col-md-12">
          <div class="form-group">
            <label>บริษัท</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>
        </div>
      </div>  
              <div class="row">   
                     <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
            <asp:DropDownList ID="ddlDivision" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
            <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>   
                  
      
                       </div> 

                    <div class="row">
   <div class="col-md-6">
          <div class="form-group">
            <label>ตำแหน่ง</label>
            <asp:DropDownList ID="ddlPosition" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div> 
       <div class="col-md-6">
          <div class="form-group">
            <label>ระดับพนักงาน</label>
            <asp:DropDownList ID="ddlLevel" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>  
         <div class="col-md-3">
          <div class="form-group">
            <label>Start Date</label>
              <div class="input-group">
                  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
            
          </div>

        </div>
             </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>End Date</label>
              <div class="input-group">
                 <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                </div>
          </div>

        </div>
                           
       <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
            <asp:DropDownList ID="ddlWorkStatus" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Selected="True" Value="1">ยังทำงานอยู่</asp:ListItem>
                <asp:ListItem Value="2">พ้นสภาพ</asp:ListItem>
                <asp:ListItem Value="0">ลาออก</asp:ListItem>
            </asp:DropDownList>
          </div>

        </div>

                        </div>
                
        <div class="box-footer clearfix"> 
            </div>
          </div>        
   </div>
        </section>
                </div>
      
<asp:Panel ID="pnUser" runat="server">
<div class="row">    
<section class="col-lg-6 connectedSortable">    
  <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-screen icon-gradient bg-primary">
            </i>รหัสผู้ใช้งาน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Username</label>
            <asp:TextBox ID="txtUsername" runat="server" cssclass="form-control text-center" placeholder="ชื่อ Login"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Password</label>
            <asp:TextBox ID="txtPassword" runat="server" cssclass="form-control text-center" placeholder="รหัสผ่าน"></asp:TextBox>
          </div>
      </div>
      </div>
</div>        
          </div>      
    </section>
    <section class="col-lg-6 connectedSortable"> 
         <div class="main-card mb-3 card">
   <div class="card-header"><i class="header-icon lnr-lock icon-gradient bg-primary">
            </i>สิทธิ์การใช้งาน
            <div class="btn-actions-pane-right"> 
            </div>
        </div>
        <div class="card-body">
              <div class="row">
        <div class="col-md-12">
          <div class="form-group">
           
             <asp:CheckBoxList ID="chkGroup" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
        </asp:CheckBoxList> 
          </div>
        </div>        
      </div>
        
          </div>
    </div>
</section>
</div>  
<div class="row"> 
             <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="Active" Checked="True" />
          </div>
        </div>
   </div>
</asp:Panel>
  <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
              <asp:Button ID="cmdClear" CssClass="btn btn-secondary" runat="server" Text="Clear" Width="120px" />
            <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
        </div>
      </div>

        </section>
</asp:Content>
