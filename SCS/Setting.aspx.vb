﻿
Public Class Setting
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlCf As New SystemConfigController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            cmdSave.Enabled = False
            LoadDataToGrid()
        End If
    End Sub

    Private Sub LoadDataToGrid()
        dt = ctlCf.SystemConfig_Get(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")))
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing
    End Sub
    Private Sub EditData(Code As String)
        dt = ctlCf.SystemConfig_GetByCode(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), Code)
        If dt.Rows.Count > 0 Then
            hdUID.Value = dt.Rows(0)("UID")
            lblCode.Text = dt.Rows(0)("CodeConfig")
            lblDesc.Text = dt.Rows(0)("Description")
            txtValue.Text = dt.Rows(0)("ValueConfig")
            cmdSave.Enabled = True
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
            End Select
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        ctlCf.SystemConfig_Update(StrNull2Zero(Request.Cookies("SafetyCS")("LoginCompanyUID")), lblCode.Text, txtValue.Text)
        LoadDataToGrid()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub
End Class

