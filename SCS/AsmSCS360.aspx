﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmSCS360.aspx.vb" Inherits="SCS.AsmSCS360" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
          <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-medal icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>360 Safety Culture survey 
                                    <div class="page-title-subheading">360 Safety Culture survey</div>
                                </div>
                            </div>
                        </div>
</div>

    <section class="content">  
  
<div class="row">
   <section class="col-lg-6 connectedSortable">
  <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-user-circle"></i>
              <h3 class="box-title">ผู้ประเมิน</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>รหัส</label><asp:HiddenField ID="hdAssessorUID" runat="server" />
              <asp:label ID="lblAssessorCode" runat="server" cssclass="form-control text-center"></asp:label>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>ชื่อ-นามสกุล</label>
              <asp:label ID="lblAssessorName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>ตำแหน่ง</label>
              <asp:label ID="lblAssessorPositionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
              <asp:label ID="lblAssessorDepartmentName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
              <asp:label ID="lblAssessorDivisionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
      </div>          
    </div>  
  </div>
</section>   
   <section class="col-lg-6 connectedSortable">
  <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-user-circle"></i>
              <h3 class="box-title">ผู้ถูกประเมิน</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>รหัส</label><asp:HiddenField ID="hdEmployeeUID" runat="server" />
              <asp:label ID="lblEmployeeCode" runat="server" cssclass="form-control text-center"></asp:label>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>ชื่อ-นามสกุล</label>
              <asp:label ID="lblEmployeeName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>ตำแหน่ง</label>
              <asp:label ID="lblEmployeePositionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
              <asp:label ID="lblEmployeeDepartmentName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
              <asp:label ID="lblEmployeeDivisionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
      </div> 
         
    </div>
  </div>
 </section>
</div>
  
<div class="row">
   <section class="col-lg-12 connectedSortable">
        <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">แบบประเมิน</h3>            
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
              <asp:HiddenField ID="hdAssessmentUID" runat="server" />
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q1. SETS THE STANDARD AND EXPECTATIONS : การตั้งมาตรฐานและการตั้งเป้าหมาย
</label>
               <label>ผู้ถูกประเมินมีลักษณะปฏิบัติตามนโยบาย ขั้นตอนการปฏิบัติงาน และแนวทางทางด้านความปลอดภัยอย่างสม่ำเสมอ แสดงออกถึงการดูแลคนที่ทำงานด้วยกันอย่างจริงใจ มุ่งมั่นตั้งใจที่จะปรับปรุงประสิทธิภาพการทำงานให้ปลอดภัยอย่างต่อเนื่องเพื่อให้ทุกคนสามารถกลับบ้านได้อย่างปลอดภัยในทุกวัน</label>
               <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q2.WALK THE TALK AND ROLE MODELS : การเดินโรงงานและการแสดงเป็นแบบอย่างในเรื่องความปลอดภัย</label>
                <label>ผู้ถูกประเมินมีลักษณะ มักจะค้นหา/ตรวจสอบและพูดคุยเกี่ยวกับเรื่องความปลอดภัยเสมอ มีส่วนร่วมอย่างแข็งขันในการประชุมและริเริ่มการปรับปรุงในเรื่องของความปลอดภัย และแสดงออกเป็นตัวอย่างในเรื่องความปลอดภัยตามที่มาตรฐานกำหนด</label>
               <asp:RadioButtonList ID="optQ2" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q3.VISIBILITY : การแสดงเห็นได้เด่นชัดในเรื่องความปลอดภัย</label>            <label>ผู้ถูกประเมินมีลักษณะของการเดินในพื้นที่ปฏิบัติงานอย่างสม่ำเสมอเพื่อทำการตรวจสอบพฤติกรรมที่เกี่ยวข้องกับความปลอดภัยรวมถึงการพูดคุยเกี่ยวกับประเด็นความปลอดภัยและโอกาสที่จะพัฒนาในพื้นที่ เป็นผู้นำให้สมาชิกในทีมปฏิบัติตามขั้นตอนทำงานที่ปลอดภัย และแสดงให้เห็นถึงความเอาใจใส่อย่างแท้จริงกับพนักงาน</label>
              <asp:RadioButtonList ID="optQ3" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div> 
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">"Q4.CHALLENGES : ความท้าทายในการแก้ใขปัญหาด้านความปลอดภัย" </label>
             <label>ผู้ถูกประเมินมีลักษณะที่มีความสามารถในการแก้ใขปัญหา หรือเข้าแทรกแซงเกี่ยวกับการแก้ปัญหาเรื่องความปลอดภัย เห็นความท้าทายเป็นเรื่องทางบวกและมีความคิดสร้างสรรค์ มีความรับผิดชอบในการตัดสินใจและเป็นผู้นำในการพูดคุยแก้ไขปัญหาเกี่ยวกับความปลอดภัย</label>
               <asp:RadioButtonList ID="optQ4" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
             
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q5."EMPOWER OTHERS : การโน้มน้าวให้บุคคลอื่นมีความรับผิดชอบเรื่องความปลอดภัย"</label>
                 <label>ผู้ถูกประเมินมีลักษณะ มีบุคลิคลักษณะชัดเจนเกี่ยวกับมาตรฐานและสิ่งที่ต้องการในด้านเป้าหมายความปลอดภัย ให้การสนับสนุนแก่สมาชิกในทีมเพื่อช่วยให้พวกเขามีมาตรฐานและเป็นไปตามเป้าหมายด้านความปลอดภัย ผลักดันให้สมาชิกในทีมในการริเริ่มการปรับปรุงด้านความปลอดภัย สร้างให้สมาชิกในทีมรู้สึกสบายใจ มั่นใจในการหยุดสิ่งที่พวกเขาเห็นว่าไม่ปลอดภัย</label>
               <asp:RadioButtonList ID="optQ5" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q6."COMMUNICATES : การสื่อสารด้านความปลอดภัย"</label>
                 <label>ผู้ถูกประเมินมีลักษณะ มีการให้ Feedback แก่สมาชิกในทีมอย่างสม่ำเสมอเกี่ยวกับผลงานด้านความปลอดภัย มีการวางแผนอย่างชัดเจนในการพูดคุยเรื่องความปลอดภัย เป้าหมายทางด้านความปลอดภัยมีการสื่อสารชัดเจนในสถานที่ทำงาน เช่น เป็นบอร์ดสื่อสาร เป็นต้น และเป็นข้อมูลอัพเดทตลอดเวลา</label>
               <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>  
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q7.COACHES : การให้คำแนะนำ/สอนด้านความปลอดภัย"</label>
                 <label>ผู้ถูกประเมินมีลักษณะสามารถวิเคราะห์และแบ่งปันข้อมูลที่เรียนรู้จากการรายงาน Near-miss และ การทำออดิท (Safety Audit) รวมถีงข้อมูลอุบัติการณ์ที่เกิดขึ้นได้ถูกทบทวนและสื่อสารให้กับสมาชิกในทีมได้รับทราบ เรียนรู้ประเด็นปัญหาด้านความปลอดภัยในพื้นที่อื่นๆ จัดให้มีการประชุมความปลอดภัยในทุกเดือนๆหรือต้องนำเรื่องความปลอดภัยเป็นส่วนหนึ่งของการประชุมหน่วยงานอย่างสม่ำเสมอ (ทำด้วยตนเอง)</label>
               <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q8.PROMOTES IMPROVEMENTS AND SHARING : การส่งเสริมให้มีการพัฒนา ปรับปรุง และแบ่งปันข้อมูลและการปฏิบัติด้านความปลอดภัย"</label>
                 <label>ผู้ถูกประเมินมีลักษณะสามารถวิเคราะห์และแบ่งปันข้อมูลที่เรียนรู้จากการรายงาน Near-miss และ การทำออดิท (Safety Audit) รวมถีงข้อมูลอุบัติการณ์ที่เกิดขึ้นได้ถูกทบทวนและสื่อสารให้กับสมาชิกในทีมได้รับทราบ เรียนรู้ประเด็นปัญหาด้านความปลอดภัยในพื้นที่อื่นๆ จัดให้มีการประชุมความปลอดภัยในทุกเดือนๆหรือต้องนำเรื่องความปลอดภัยเป็นส่วนหนึ่งของการประชุมหน่วยงานอย่างสม่ำเสมอ (ทำด้วยตนเอง)</label>
              <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">Q9.ARTICULATES THE VISION FOR SAFETY : มีการกำหนดวิสัยทัศน์ด้านความปลอดภัย"</label>
                 <label>ผู้ถูกประเมินมีลักษณะที่สามารถบอกได้อย่างชัดเจนว่า "อุบัติเหตุเป็นศูนย์" Zero Accident เป็นสิ่งที่เป็นไปได้ และความปลอดภัยที่ดี คือ การดำเนินธุรกิจที่ดีด้วย มีการส่งเสริมให้คนเอาใจใส่ตนเอง ครอบครัวและเพื่อนร่วมงานและเป็นสิ่งที่มีค่าขององค์กร เข้าร่วมการประชุมต่างๆที่เกี่ยวข้องกับวิสัยทัศน์ด้านความปลอดภัย กิจกรรมความปลอดภัย ระบบความปลอดภัยและขั้นตอนต่างๆด้านความปลอดภัย เพื่อเพิ่มความเข้าใจ</label>
               <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="1" Text="Very poor"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="Poor"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="Fair"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="Good"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Excellent"></asp:ListItem>
              </asp:RadioButtonList> 
          </div>
        </div> 
        </div> 
    </div>
</div> 
   
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">Q10. FEEDBACKS/ COMMENT (If Any)</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">กรุณาเขียนคำแนะนำ/ข้อเสนอแนะของท่าน ที่เป็นประโยชน์แก่หัวหน้างาน / ผู้จัดการ/ ผู้บริหาร</label>
              <asp:TextBox ID="txtComment" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
          </div>
        </div>                
        </div>         
    </div>
</div>   
         <div class="row"> 
        <div class="col-md-12 text-center">
  <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" />  
            
              </div>
      </div>
</section>
    </div>
</section>
</asp:Content>