﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmSCS.aspx.vb" Inherits="SCS.AsmSCS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
          <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-medal icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Safety Culture survey 
                                    <div class="page-title-subheading">Safety Culture survey</div>
                                </div>
                            </div>
                        </div>
</div>

    <section class="content">  

  <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-user-circle"></i>
              <h3 class="box-title">ผู้ทำแบบประเมิน</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>รหัสพนักงาน</label><asp:HiddenField ID="hdUserID" runat="server" />
              <asp:label ID="lblCode" runat="server" cssclass="form-control text-center"></asp:label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ชื่อ-นามสกุล</label>
              <asp:label ID="lblEmployeeName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>ตำแหน่ง</label>
              <asp:label ID="lblPositionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>แผนก</label>
              <asp:label ID="lblDepartmentName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
            <div class="col-md-6">
          <div class="form-group">
            <label>ฝ่าย</label>
              <asp:label ID="lblDivisionName" runat="server" cssclass="form-control"></asp:label>
          </div>
        </div>
      </div> 
         
    </div>
    <div class="box-footer clearfix">           
                <!--ที่มา : (Sonne, Villalta, & Andrews, 2012) , http://thai-SCS-assessment.blogspot.com-->
            </div>
  </div>
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 1 ภาวะผู้นำและความมุ่งมั่น Leadership & Commitment</h3>            
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">1.1.การสื่อสารด้านความปลอดภัย (Safety communication) ของผู้บริหารเป็นอย่างไร</label>
               <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ผู้บริหารไม่ค่อยใส่ใจ เพียงแค่สื่อสารกับผู้ปฏิบัติงานว่าอย่าสร้างปัญหาหรือทำให้เกิดอุบัติเหตุขึ้น"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ผู้บริหารลงมาสื่อสารเฉพาะเมื่อมีอุบัติเหตุหรือมีปัญหาเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="ผู้บริหารสื่อสารด้านความปลอดภัยอยู่เสมอ แต่เป็นการสื่อสารทางเดียว (one-way communication)"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ผู้บริหารได้เข้าใจถึงวิธีการสื่อสารกับผู้ปฏิบัติงาน และมีช่องทางในการแบบสองทาง (two-way communication) โดยมีทั้งการรับฟังและแสดงความคิดเห็น  เพื่อถ่ายทอดข้อมูลซึ่งกันและกันระหว่างพนักงานในองค์กร"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ผู้บริหารเปิดโอกาสในการรับฟังข้อมูลมากกว่าการบอกเล่า หากมีผู้บาดเจ็บจากการทำงาน จะแสดงออกถึงความห่วงใยเหมือนคนในครอบครัว"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">1.2.ความมุ่งมั่นด้านความปลอดภัย (Safety commitment) ของผู้บริหารและผู้ปฏิบัติงานเป็นอย่างไร ?</label>
               <asp:RadioButtonList ID="optQ2" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ผู้บริหารไม่ให้การสนับสนุนใดๆ ในเรื่องความปลอดภัย ผู้ปฏิบัติงานไม่ใส่ใจความปลอดภัย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ผู้บริหารและผู้ปฏิบัติงาน ใส่ใจเรื่องความปลอดภัยก็ต่อเมื่อเกิดอุบัติเหตุหรือเกิดปัญหา"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="ผู้บริหารและผู้ปฏิบัติงาน ใส่ใจในเรื่องความปลอดภัย แต่ไม่มีการปฏิบัติอย่างจริงจัง"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ผู้บริหารใส่ใจและสนับสนุนด้านความปลอดภัย ผู้ปฏิบัติงานเอาใจใส่ตนเองและห่วงใยเพื่อนร่วมงาน แต่ก็ยังไม่ทั่วทั้งองค์กร"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ผู้บริหารใส่ใจและสนับสนุนด้านความปลอดภัยอย่างเต็มที่ ผู้ปฏิบัติงานใส่ใจและมุ่งมั่นพัฒนามาตรฐานความปลอดภัยต่างๆ อย่างต่อเนื่อง"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">1.3.ผลตอบแทนความสำเร็จด้านความปลอดภัยฯ เป็นอย่างไร ?</label>
              <asp:RadioButtonList ID="optQ3" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีการให้รางวัล มีแต่การทำโทษผู้ที่ทำผิดพลาด"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ไม่มีการให้รางวัลการส่งเสริมด้านความปลอดภัย จะได้รางวัลก็ต่อเมื่อบริษัทไม่มีการบาดเจ็บถึงขั้นหยุดงาน (Loss Time Injury; LTI) เท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีเพียงการชมเชยด้วยวาจาหากมีผลการดำเนินงานด้านความปลอดภัยฯ ที่ดี และมีการมอบรางวัลเล็ก ๆ น้อย ๆ ในการส่งเสริมความปลอดภัย มีการคำนวณผลตอบแทนในรูปแบบของโบนัสหรือรางวัล เมื่อบริษัทฯ มีสถิติอุบัติเหตุที่ลดลง"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการนำผลการดำเนินงานด้านความปลอดภัย (SSHE performance) มาพิจารณาเพื่อใช้ประเมินผลงานหรือการปรับระดับตำแหน่งงาน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ทุกคนปฏิบัติงานด้วยความปลอดภัย โดยไม่คาดหวังรางวัลหรือผลตอบแทน เพราะเห็นว่าเรื่องความปลอดภัยมีคุณค่าและมีประโยชน์ที่ต้องปฏิบัติอยู่แล้ว"></asp:ListItem>               
              </asp:RadioButtonList>
          </div>

        </div> 
        </div> 
    </div>
</div>
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 2 นโยบายและวัตถุประสงค์กลยุทธ์  Policy & Strategic Objectives</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">2.1 คุณคิดว่าผู้ที่ทำให้เกิดอุบัติเหตุในมุมมองผู้บริหารเป็นอย่างไร?</label>
               <asp:RadioButtonList ID="optQ4" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ผู้ที่ทำให้เกิดอุบัติเหตุจะต้องถูกตำหนิและลงโทษ และเป็นความรับผิดชอบของผู้ที่ทำให้เกิดเหตุ"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีความพยายามที่จะจัดการกับผู้ที่มักจะก่อให้เกิดอุบัติเหตุ มีการสอบสวนหาสาเหตุของเกิดอุบัติเหตุ แต่ไม่มีการติดตามผลการต่อว่ารายบุคคลยังมีให้เห็นอย่างชัดเจน"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการสอบสวนหาสาเหตุของอุบัติเหตุ โดยมุ่งเน้นที่ความบกพร่องของเครื่องจักร และใช้วิธีจัดการที่ “ตัวบุคคล” ไม่ได้จัดการที่ “ระบบ”"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ผู้บริหารคำนึงถึงการจัดการอุบัติเหตุทั้งระบบ และยอมรับว่าตนเองมีหน้าที่รับผิดชอบต่ออุบัติเหตุที่เกิดขึ้น พร้อมทั้งยอมรับการตำหนิได้เช่นกัน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ผู้บริหารแสดงความรับผิดชอบเพื่อป้องกันอุบัติเหตุต่าง ๆ และกำหนดมาตรการในการป้องกันโดยพิจารณาถึงการประสานงานกันระหว่างระบบและตัวบุคลากร"></asp:ListItem>
              </asp:RadioButtonList>
             
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">2.2 การให้ความสำคัญระหว่างเรื่องของ“ความปลอดภัย (Safety)” และ “ผลผลิตหรือกำไร (Productivity)” ของบริษัทฯ เป็นอย่างไร?</label>
               <asp:RadioButtonList ID="optQ5" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="เน้น ผลผลิตหรือกำไร (Productivity) เป็นหลัก มองว่าเรื่องความปลอดภัย (Safety) เป็นค่าใช้จ่ายที่ไม่จำเป็น"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ให้ความสำคัญ ผลผลิตหรือกำไร (Productivity) เป็นลำดับแรก โดยมีการลงทุนเพื่อมาตรฐานป้องกันอันตรายอยู่บ้าง"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="ให้ความสำคัญเรื่อง ผลผลิตหรือกำไร (Productivity) เป็นหลัก และให้ความสำคัญเรื่องความปลอดภัย (Safety) ด้วย ผู้บริหารรู้ว่า ความปลอดภัย (Safety) สำคัญแต่บางครั้งยังมองว่าเรื่องความปลอดภัย (Safety) เป็นค่าใช้จ่ายส่วนเกินที่ไม่จำเป็น"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ให้ความสำคัญสูงสุดในเรื่องความปลอดภัย (Safety) และเข้าใจว่าความปลอดภัย (Safety) มีผลต่อการผลิตและกำไรของบริษัท แต่ก็ยังมีการพิจารณาเรื่องค่าใช้จ่ายและความคุ้มทุนในการลงทุนด้านความปลอดภัย (Safety)"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ให้ความสำคัญระหว่างความปลอดภัย (Safety) และผลผลิตหรือกำไร (Productivity) อย่างสมดุล ผู้บริหารเชื่อว่าความปลอดภัย (Safety) มีผลต่อความยั่งยืนของบริษัท"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue"> 2.3 ความเข้าใจและการทำ Safety moments / Safety Talk ก่อนประชุมหรือก่อนเริ่มงานเป็นอย่างไร?</label>
               <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่รู้จัก ไม่มีใครพูดถึง Safety moments / Safety Talk"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="รู้จัก Safety moments / Safety Talk แต่ไม่ได้ทำ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="รู้จัก Safety moments / Safety Talk ทำบ้างเป็นบางครั้งบางคราวเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการทำ Safety moments / Safety Talk ทุกครั้งก่อนการประชุมและก่อนเริ่มงาน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="มีการทำ Safety moments / Safety Talk เป็นประจำ และทุกคนมีความกระตือรือร้นในการนำเสนอ"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div> 
    </div>
</div>
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 3 ทรัพยากรของบริษัทและเอกสาร Organization Resource & Documentation</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">3.1 บริษัทฯ มีการจัดการผู้รับเหมาอย่างไร ?</label>
               <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="บริษัทฯ มีทัศนคติที่ว่าทำอย่างไร ก็ได้ให้งานสำเร็จ โดยมีค่าใช้จ่ายน้อยที่สุด และไม่คำนึงถึงเรื่องความปลอดภัย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="บริษัทฯ จะเลือกผู้รับเหมาเจ้าที่ให้ราคาต่ำที่สุด โดยจะให้ความสำคัญในเรื่องความปลอดภัยฯ ของผู้รับเหมาก็ต่อเมื่อเกิดอุบัติเหตุขึ้นมาแล้ว"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีเกณฑ์ด้านความปลอดภัยในการคัดเลือกผู้รับเหมา กรณีที่ผู้รับเหมาไม่สามารถปฏิบัติตามมาตรฐานความปลอดภัยได้ จะมีการผ่อนปรนมาตรฐานด้านความปลอดภัยของบริษัท"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="คัดเลือกผู้รับเหมาที่มีระบบบริหารจัดการด้านความปลอดภัยที่ใช้งานได้จริง มีการติดตามผลร่วมกันระหว่างบริษัทและผู้รับเหมา"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ไม่มีการประนีประนอมเรื่องความปลอดภัย บริษัทและผู้รับเหมาร่วมมือกันเพื่อแก้ไขปัญหาต่างๆ แม้อาจส่งผลให้งานสำเร็จล่าช้าไป เพื่อให้ได้ตามมาตรฐานด้านความปลอดภัยที่กำหนดไว้"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">3.2 ผู้ปฏิบัติงานสนใจเรื่องของขีดความสามารถในการทำงาน (Competency) และการฝึกอบรม (Training) อย่างไร?</label>
              <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="การฝึกอบรมเป็นเรื่องน่าเบื่อ เข้าอบรมเพราะถูกบังคับและเป็นหลักสูตรที่กฎหมายบังคับ และจะรู้สึกดีเมื่อได้เข้าห้องอบรมเพียงเพราะไม่ต้องทำงาน"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="จัดฝึกอบรมเมื่อมีปัญหาหรือเกิดอุบัติเหตุขึ้นเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="บริษัทฯ กำหนดหลักสูตรอบรมสำหรับพนักงานแต่ละตำแหน่ง มีการทดสอบความรู้ความสามารถ                   มีการอบรม on-the-job เมื่อมีการเปลี่ยนงานเป็นบางกรณี"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="หัวหน้างานให้ความสำคัญกับการฝึกอบรมและทดสอบความสามารถในการทำงาน และผู้ปฏิบัติงานยินดีเข้าอบรม พร้อมทั้งมีส่วนร่วมในการกำหนดความต้องการในการฝึกอบรมของตนเอง"></asp:ListItem>
                   <asp:ListItem Value="5" Text="พนักงานกระตือรือร้นที่จะพัฒนาตนเอง หัวหน้างานให้ความสำคัญในการพัฒนาบุคลากรอย่างเป็นระบบ ความต้องการในการฝึกอบรมถูกเสนอมาจากพนักงาน"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
        </div>
        <div class="row">      
            <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">3.3 คุณคิดว่าหน่วยงานความปลอดภัยฯ ในบริษัทมีขนาดและสถานภาพเป็นอย่างไร?</label>
               <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีหน่วยงานความปลอดภัยฯ หรือ มีคนในหน่วยงานเพียงคนเดียว หรือมีบางส่วนอยู่ในฝ่ายบุคคล"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="เป็นหน่วยงานเล็กๆ และไม่มีความสำคัญ นึกถึงเฉพาะช่วงที่มีปัญหาหรือมีอุบัติเหตุ มักจะถูกมองว่ามีบทบาทเป็นตำรวจคอยจับผิด"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="เป็นหน่วยงานที่ค่อนข้างใหญ่ ทำหน้าที่ตามที่กฎหมายหรือมาตรฐานกำหนดเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="เป็นหน่วยงานที่ได้รับการยอมรับว่ามีความสำคัญ ผู้บริหารหน่วยงานความปลอดภัยรายงานขึ้นตรงต่อผู้บริหารสูงสุดขององค์กร"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ความปลอดภัยเป็นหน้าที่รับผิดชอบของทุกคน จนเป็นวัฒนธรรมความปลอดภัยภายในองค์กร หน่วยงานความปลอดภัยมีหน้าที่ให้คำปรึกษา แนะนำด้านความปลอดภัย"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div> 
        </div> 
    </div>
</div>
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 4 การประเมินและการบริหารจัดการความเสี่ยง   Evaluation & Risk Management</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">4.1 การวางแผนการทำงานและแนวทางในการบริหารจัดการด้านความปลอดภัยฯ เป็นอย่างไร?</label>
             <asp:RadioButtonList ID="optQ10" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีการวางแผนการทำงานและไม่มีแนวทางการบริหารความปลอดภัย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีการวางแผนงานเพื่อความปลอดภัยเฉพาะการป้องกันอุบัติเหตุที่เกิดขึ้นเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีระบบการวิเคราะห์งานเพื่อความปลอดภัย (JSEA) และการขออนุญาตทำงาน (Permit to Work) ทุกคนเชื่อว่าระบบการจัดการที่มีอยู่สามารถป้องกันอุบัติเหตุได้และมีประสิทธิภาพดีอยู่แล้ว"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="การประเมินและวิเคราะห์งานเพื่อความปลอดภัย (JSEA) เป็นส่วนหนึ่งของขั้นตอนการปฏิบัติงาน และมีการประเมินประสิทธิภาพโดยหัวหน้างานและฝ่ายบริหารในระดับหนึ่งแต่ไม่ครอบคลุมทั้งหมด"></asp:ListItem>
                   <asp:ListItem Value="5" Text="มีวางแผนงานเป็นอย่างดีโดยมีการวิเคราะห์ปัญหาและประเมินประสิทธิภาพของแผนงานครบทุกด้าน ผู้ปฏิบัติงานมีส่วนร่วมและเชื่อถือแผนงานพร้อมที่จะนำไปปฏิบัติอย่างมีประสิทธิภาพ"></asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">4.2 มีการใช้เทคนิคความปลอดภัยฯต่าง ๆ ในการทำงานอย่างไร?</label>
              <asp:RadioButtonList ID="optQ11" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีเทคนิคอะไร ทุกคนดูแลตัวเอง"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="จะมีการนำเทคนิคความปลอดภัยฯ มาใช้หลังจากเกิดอุบัติเหตุ แต่ไม่มีการใช้งานที่เป็นระบบ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการใช้เทคนิคความปลอดภัยฯ เช่น การวิเคราะห์ความปลอดภัยฯ ในการทำงาน (JSEA) แต่เป็นการนำมาใช้เพื่อให้เป็นไปตามข้อกำหนดเท่านั้น ไม่มีการนำมาปฏิบัติตามอย่างจริงจัง"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ผู้ปฏิบัติงานให้ความสำคัญและยอมรับเทคนิคความปลอดภัยต่างๆ เช่น การวิเคราะห์งานเพื่อความปลอดภัย (JSEA) การสังเกตและรายงานสภาพการณ์และพฤติกรรมความปลอดภัย (SWO) โดยถือว่าเป็นมาตรฐานในการทำงาน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="การวิเคราะห์งานเพื่อความปลอดภัยฯ (JSEA) ถือเป็นเทคนิคในการจัดการอันตรายในงาน มีการนำใช้ในทุกงานอย่างจริงจัง  ผู้บริหารและพนักงานมีความห่วงใยและเต็มใจที่จะบอกกล่าวกันเรื่องอันตรายต่างๆ ในการทำงาน"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div> 
        </div>       
    </div>
</div>
   
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 5 การลงมือปฏิบัติตามระบบบริหารจัดการของบริษัทฯ  Implementation & Operation Control</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">5.1 จุดประสงค์ของการมีขั้นตอนการทำงานเพื่อความปลอดภัยคืออะไร?</label>
             <asp:RadioButtonList ID="optQ12" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีความจำเป็นต้องมีขั้นตอนการทำงานเพื่อความปลอดภัย ต่างคนต่างทำ"></asp:ListItem> 
                   <asp:ListItem Value="2" Text="มีขั้นตอนการทำงานเพื่อความปลอดภัยเพื่อป้องกันอุบัติเหตุที่เกิดขึ้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีขั้นตอนการทำงานเพื่อความปลอดภัยจำนวนมากและมีการฝึกอบรมขั้นตอนการทำงานเพื่อความปลอดภัย"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="ขั้นตอนการทำงานเพื่อความปลอดภัยอ้างอิงแนวปฏิบัติที่ดี (Best practice) ยังมีที่ไม่ปฏิบัติตามในบางขั้นตอนที่ไม่สะดวกและยากต่อการปฏิบัติแต่มีการละเลยอยู่น้อยมากๆ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="พนักงานมีความเชื่อมั่นว่าการปฏิบัติตามขั้นตอนการทำงานเพื่อความปลอดภัยเป็นสิ่งที่สำคัญต้องปฏิบัติตามอย่างเคร่งครัด และมีส่วนร่วมในการทบทวนปรับปรุงเพื่อให้มีประสิทธิภาพอยู่เสมอ"></asp:ListItem>
              </asp:RadioButtonList>
          </div>                 
        </div>                
        </div>     
    </div>
</div>   
   
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 6 การตรวจติดตามและวัดผล  Monitoring & Measurement</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.1 ระบบการรายงาน การสอบสวน และการวิเคราะห์อุบัติเหตุเป็นอย่างไร?</label>
             <asp:RadioButtonList ID="optQ13" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="หลีกเลี่ยงการรายงาน สนใจอุบัติเหตุร้ายแรงเท่านั้น"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีระบบการรายงานที่ไม่เป็นทางการ การสอบสวนทำเพียงเพื่อหาสาเหตุเบื้องต้น เพื่อจัดทำเอกสารในการยืนยันว่ามีการสอบสวนแล้วเท่านั้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีขั้นตอนการรายงานและสอบสวนอุบัติเหตุโดยนำมาซึ่งข้อมูลและแผนการปรับปรุงมากมาย แต่มักจะพลาดขั้นตอนในการระบุปัญหาที่แท้จริง"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="การสอบสวนอุบัติเหตุมีประสิทธิภาพ มีการติดตามแก้ไขปัญหาอย่างเป็นระบบ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="การสอบสวนและการวิเคราะห์อุบัติเหตุมีประสิทธิภาพ เพื่อค้นหาสาเหตุที่แท้จริง มีการติดตามแก้ไขปัญหาอย่างเป็นระบบ และมีการสื่อสารสิ่งที่ได้เรียนรู้เพื่อป้องกันการเกิดเหตุซ้ำทั่วทั้งองค์กร"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.2 ระบบการสังเกตและรายงานสภาพการณ์และพฤติกรรมความปลอดภัยเป็นอย่างไร?</label>
              <asp:RadioButtonList ID="optQ14" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีการรายงานใด ๆ เลย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีการรายงานแบบง่ายๆ ไม่เป็นระบบ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการรายงาน และมีการเก็บข้อมูลอย่างเป็นระบบ"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการวิเคราะห์ถึง ''สาเหตุ'' และติดตามการแก้ไขอย่างเป็นระบบ ผู้บริหารกำหนดเป้าทั้งในด้านจำนวนและคุณภาพของรายงาน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="พนักงานทุกระดับเข้าถึงและใช้ประโยชน์จากผลการวิเคราะห์ของรายงาน ข้อมูลจากรายงานเป็นข้อมูลที่เปิดเผยและแบ่งปันกันได้ภายในองค์กร"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div> 
        </div>     
           <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.3 เกิดอะไรขึ้นหลังจากการเกิดอุบัติเหตุ? / มีขั้นตอนในการติดตามและแก้ไขปัญหาอย่างไร?</label>
             <asp:RadioButtonList ID="optQ15" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="หลังจากอุบัติเหตุ พนักงานมักจะตกเป็นฝ่ายผิดและถูกทำโทษ ไม่มีการแก้ไขป้องกันการเกิดเหตุซ้ำ"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ฝ่ายบริหารไม่พอใจกับการเกิดอุบัติเหตุ ผู้ปฏิบัติงานพยายามหลีกเลี่ยงการรายงานอุบัติเหตุต่อผู้บังคับบัญชา"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการรายงานอุบัติเหตุ สอบวนตามระบบ ฝ่ายบริหารเป็นห่วงตัวเลขสถิติอุบัติเหตุมากกว่าผู้บาดเจ็บ"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการรายงาน สอบสวนตามระบบ ฝ่ายบริหารใส่ใจกับอาการของผู้บาดเจ็บ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ฝ่ายบริหารมีส่วนร่วมในการป้องกันการเกิดเหตุซ้ำและแสดงความห่วงใยต่อผู้ที่ได้รับผลกระทบหรือได้รับบาดเจ็บ"></asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>                
        </div>
           <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.4 ใครเป็นผู้ตรวจสอบความปลอดภัยฯ ในการทำงานประจำวัน?</label>
             <asp:RadioButtonList ID="optQ16" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีระบบตรวจสอบ ต่างคนต่างทำงาน"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีการตรวจสอบจากบุคคลภายนอกหลังจากเกิดอุบัติเหตุ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="หัวหน้างานทำการตรวจสอบการทำงานอย่างสม่ำเสมอ แต่ไม่เป็นประจำทุกวัน"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="หัวหน้างานมีการกระตุ้นให้ทุกคนตรวจสอบความปลอดภัยก่อนเริ่มงาน ฝ่ายบริหารแสดงความมุ่งมั่นและมีส่วนร่วมในการตรวจสอบร่วมกับพนักงานเป็นประจำ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ทุกคนมีการตรวจสอบความปลอดภัยของตนเองและเพื่อนร่วมงาน โดยไม่ต้องได้รับการตรวจสอบจากหัวหน้างาน และไม่มีปัญหาด้านความปลอดภัย"></asp:ListItem>
              </asp:RadioButtonList>
          </div>                 
        </div>                
        </div>
           <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.5 บรรยากาศในการประชุมด้านความปลอดภัยฯเป็นอย่างไร?</label>
             <asp:RadioButtonList ID="optQ17" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="เป็นการประชุมที่น่าเบื่อ เสียเวลา"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="รูปแบบการประชุมเป็นการที่ฝ่ายบริหารเข้ามาตำหนิข้อผิดพลาด หรือแค่เพียงสรุปการแก้ไขอุบัติเหตุแบบผ่านๆ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีประชุมตามวาระ มีการพูดคุยโต้ตอบในที่ประชุมน้อยมาก"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="การประชุมจะเป็นเวทีที่ทุกคนได้มีโอกาสมาหารือเพื่อความปลอดภัยอย่างจริงจัง การประชุมระดับย่อยๆ จะมีเรื่องความปลอดภัยเป็นหัวข้อหนึ่งในการสนทนา"></asp:ListItem>
                   <asp:ListItem Value="5" Text="พนักงานทุกระดับจัดการประชุมเพื่อหารือการยกระดับความปลอดภัยเป็นประจำ และผู้บริหารให้ความสำคัญเข้าร่วมทุกครั้ง"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>                
        </div>
           <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">6.6 ความเข้าใจเรื่องพฤติกรรมพื้นฐานด้านความปลอดภัยฯ (B-CAREs) เป็นอย่างไร?</label>
             <asp:RadioButtonList ID="optQ18" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่เคยได้รับข้อมูลเกี่ยวกับ B-CAREs เลย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="รู้จัก B-CAREs แต่ไม่ทราบถึงประโยชน์และวิธีการนำมาประยุกต์ในการใช้งาน"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการสื่อสาร อบรม B-CAREs อย่างเป็นระบบ"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="พนักงานทุกคนได้รับการสื่อสาร อบรม B-CAREs และนำไปใช้ประโยชน์ในงานเป็นประจำ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="พนักงานทุกคนเข้าใจและเห็นคุณค่าของ B-CAREs นำไปใช้ในงานทุกๆ วันด้วยความรู้สึกเต็มใจและยินดี"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>                
        </div>
    </div>
</div>   

   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 7 การประเมินและการบริหารจัดการความเสี่ยง valuation & Risk Management</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">7.1 มีการตรวจสอบและทบทวนด้านความปลอดภัยฯ อย่างไร?</label>
             <asp:RadioButtonList ID="optQ19" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="มีการตรวจสอบและทบทวนด้านความปลอดภัยฯ เท่าที่กฎหมายบังคับ โดยเน้นเรื่องของการเงินและค่าใช้จ่ายเป็นหลัก"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="ยอมรับการถูกตรวจสอบและทบทวนด้านความปลอดภัยที่หลีกเลี่ยงไม่ได้ โดยเฉพาะหลังจากเกิดอุบัติเหตุ"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีแผนการตรวจสอบและทบทวนด้านความปลอดภัยฯ ที่แน่นอนมีการตรวจสอบและทบทวนตามแผน พร้อมที่จะตรวจสอบผู้อื่นแต่ไม่เต็มใจที่จะถูกตรวจสอบ"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการกำหนดแผนการตรวจสอบและทบทวนด้านความปลอดภัยฯ ที่แน่นอน ผู้บริหารและหัวหน้างานยินดีรับการตรวจสอบจากสายงานอื่น ๆ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="มีระบบในการตรวจสอบและทบทวนด้านความปลอดภัยฯ อย่างชัดเจน รวมไปถึงการติดตามผลการตรวจสอบอยู่เสมอ และนำผลไปพัฒนาปรับปรุงอย่างต่อเนื่อง"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>                
        </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">7.2 มีการเปรียบเทียบผลการดำเนินงานด้านความปลอดภัยฯ และสถิติอุบัติเหตุกับบริษัทอื่น ๆ หรือไม่อย่างไร?</label>
              <asp:RadioButtonList ID="optQ20" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีการเปรียบเทียบผลการดำเนินงานด้านความปลอดภัย"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีการเปรียบเทียบผลการดำเนินงานด้านความปลอดภัยฯ กับบริษัทอื่น ๆ ตามความจำเป็น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีการเปรียบเทียบผลการดำเนินงานด้านความปลอดภัยกับบริษัทอื่นๆ เพื่อพัฒนาปรับปรุงให้ดีขึ้น"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการเปรียบเทียบกับผลการดำเนินงานด้านความปลอดภัยฯ กับบริษัทอื่น ๆ ในอุตสาหกรรมในประเภทเดียวกัน ผู้บริหารมีเป้าหมายที่จะเป็นบริษัทชั้นนำด้านความปลอดภัยฯ"></asp:ListItem>
                   <asp:ListItem Value="5" Text="มีการเปรียบเทียบผลงานด้านความปลอดภัยฯ กับบริษัทในกลุ่มธุรกิจอื่น ๆ และพนักงานทุกระดับเข้าใจผลการเปรียบเทียบและมีส่วนร่วมในการกำหนดแนวทางการยกระดับสู่บริษัทชั้นนำด้านความปลอดภัย"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div> 
        </div>       
    </div>
</div>   

   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ชุดคำถามที่ 8 การจัดการความปลอดภัยนอกเวลางาน  Off-the-job Safety Program</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">8.1 การดำเนินงานเรื่องความปลอดภัย “นอกเวลางาน (Off-the-job)” มีมากน้อยเพียงใด?</label>
             <asp:RadioButtonList ID="optQ21" runat="server" RepeatDirection="Vertical" >
                  <asp:ListItem Value="1" Text="ไม่มีการพูดถึงหรือดำเนินการเรื่องความปลอดภัยนอกเวลางาน (Off-the-job)"></asp:ListItem>            	
                   <asp:ListItem Value="2" Text="มีการสื่อสารความปลอดภัยนอกเวลางาน (Off-the-job) เมื่อมีอุบัติเหตุนอกเวลางานเกิดขึ้น เช่น การเกิดอุบัติเหตุทางยานพาหนะ เป็นต้น"></asp:ListItem> 
                   <asp:ListItem Value="3" Text="มีโปรแกรมความปลอดภัยนอกเวลางาน (Off-the-job Safety Program) เช่น การขับขี่ปลอดภัย (Driving safety) เป็นต้น"></asp:ListItem> 
                   <asp:ListItem Value="4" Text="มีการรายงาน สอบสวน และเก็บสถิติความปลอดภัยนอกเวลางาน (Off-the-job) และมีการพัฒนาปรับปรุง เช่น ความปลอดภัยที่บ้าน ความปลอดภัยในการขับรถนอกเวลางาน"></asp:ListItem>
                   <asp:ListItem Value="5" Text="ผู้บริหารและพนักงานใส่ใจความปลอดภัยนอกเวลางาน (Off-the-job) เช่นเดียวกับความปลอดภัยในการทำงาน แสดงความห่วงใยทั้งในและนอกเวลางาน"></asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>                
        </div>      
    </div>
</div>   
   
   <div class="box box-primary">
       <div class="box-header">
              <i class="fa fa-question-circle"></i>          
              <h3 class="box-title text-blue text-bold">ความคิดเห็นหรือข้อเสนอแนะเพิ่มเติม</h3>
           <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>

    <div class="box-body">
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">ท่านมีความคิดเห็นหรือข้อเสนอแนะเกี่ยวกับการจัดการด้านความปลอดภัย อาชีวอนามัย และสิ่งแวดล้อม ในบริษัทฯ เพิ่มเติมหรือไม่ อย่างไร</label>
              <asp:TextBox ID="txtComment1" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
          </div>
        </div>                
        </div>  
        <div class="row">             
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">ท่านมีความคิดเห็นหรือข้อเสนอแนะเกี่ยวกับ แบบสอบถามนี้หรือไม่ อย่างไร</label>
              <asp:TextBox ID="txtComment2" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
          </div>
        </div>                
        </div>     
    </div>
</div>   
         <div class="row"> 
        <div class="col-md-12 text-center">
  <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" />  
            
              </div>
      </div>
</section>
</asp:Content>