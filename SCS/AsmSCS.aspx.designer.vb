﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AsmSCS
    
    '''<summary>
    '''hdUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdUserID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''lblCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEmployeeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmployeeName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPositionName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPositionName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblDepartmentName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDepartmentName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblDivisionName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDivisionName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''optQ1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ1 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ2 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ3 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ4 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ5 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ6 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ7 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ8 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ9 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ10 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ11 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ12 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ13 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ14 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ15 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ15 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ16 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ16 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ17 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ17 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ18 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ18 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ19 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ19 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ20 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ20 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optQ21 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ21 As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtComment1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtComment2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
End Class
