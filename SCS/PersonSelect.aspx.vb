﻿Imports System.Data
Public Class PersonSelect
    Inherits System.Web.UI.Page
    Dim ctlEmp As New CustomerController
    Public dtES As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("iIncident")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            If Request.Cookies("iIncident")("ROLE_ADM") = False And Request.Cookies("iIncident")("ROLE_SPA") = False Then
                LoadPersonByCustomer(Request.Cookies("iIncident")("LoginCompanyUID"))
            Else
                LoadPersonByAdmin()
            End If

        End If
    End Sub
    Private Sub LoadPersonByAdmin()
        Select Case Request("ActionType")
            Case "hraasm"
                dtES = ctlEmp.CustomerRisk_Get
            Case "health"
                dtES = ctlEmp.Customer_GetByStatus("A")
            Case Else
                dtES = ctlEmp.Customer_GetAll
        End Select
    End Sub

    Private Sub LoadPersonByCustomer(CompanyUID As Integer)

        Select Case Request("ActionType")
            Case "hraasm"
                dtES = ctlEmp.CustomerRisk_Get(CompanyUID)
            Case "health"
                dtES = ctlEmp.Customer_GetByCompanyUID(CompanyUID)
            Case Else
                dtES = ctlEmp.Customer_GetByCompanyUID(Request.Cookies("iIncident")("LoginCompanyUID"))
        End Select
    End Sub
End Class