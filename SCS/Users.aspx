﻿<%@ Page Title="User Account" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Users.aspx.vb" Inherits="SCS.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-success"></i>
            </div>
            <div>
                <%: Title %>
                    <div class="page-title-subheading">
                        <%: MetaDescription %>
                    </div>
            </div>
        </div>
    </div>
</div>
     
<section class="content">  
     <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-users icon-gradient bg-success">
            </i>ผู้ใช้งานทั้งหมด
            <div class="btn-actions-pane-right">
                <a href="UserModify?m=u&s=u" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
            </div>
        </div>
        <div class="card-body">    
            
                  <div class="row">
                    <div class="col-md-5">
                            <div class="form-group">
                                <label>บริษัท</label>
                                <asp:DropDownList ID="ddlCompany"  runat="server"  AutoPostBack="True"  CssClass="form-control select2">
                                </asp:DropDownList>
                            </div>
                    </div>
                     <div class="col-md-3">
                          <div class="form-group">
                              <label>User Group</label>
                              <asp:DropDownList ID="ddlGroup"  runat="server"  AutoPostBack="True"  CssClass="form-control select2">
                     <asp:ListItem Value="0">---ทั้งหมด---</asp:ListItem> 
            <asp:ListItem Value="1">Customer Admin</asp:ListItem>             
            <asp:ListItem Value="2">Customer User</asp:ListItem>
            <asp:ListItem Value="3">Assessor</asp:ListItem>
             <asp:ListItem Value="9">Administrator</asp:ListItem>
                                                  </asp:DropDownList>  
                          </div>
                    </div>                  
                    <div class="col-md-3">
                            <div class="form-group">
                                <label>ค้นหา</label>
                                <asp:TextBox ID="txtSearch" runat="server" Width="250px" CssClass="form-control" placeholder="สามารถค้นหาได้จาก username,ชื่อ" ></asp:TextBox>
                            </div>
                        </div>
                     <div class="col-md-1">
                            <div class="form-group">                              
                                <br />  
                                <asp:Button ID="cmdSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>

                </div>
            
                  <div class="row justify-content-center">           
                       <div class="col-md-12">
                <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                            <asp:BoundField HeaderText="ชื่อ-สกุล" DataField="Name" />
                            <asp:BoundField HeaderText="หน่วยงาน" DataField="DepartmentName" />
                            <asp:BoundField DataField="LastLogin" HeaderText="Last Login" />
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# IIf(DataBinder.Eval(Container.DataItem, "StatusFlag") = "A", True, False) %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
                      </div>
                        </div>
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 
     </section>
</asp:Content>
