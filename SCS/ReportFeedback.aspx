﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportFeedback.aspx.vb" Inherits="SCS.ReportFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>
    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div class="app-page-title">
          <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph2 icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Feedback report
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>      
   <section class="content">  
          <div class="row"> 
      <section class="col-lg-12 connectedSortable">
          <div class="box box-solid">  
    <div class="box-body">             
                  <div id="chart"></div>                 
              </div>
          </div>
</section>   
      
        </div> 
    <div class="row"> 
      <section class="col-lg-12 connectedSortable">

          <div class="box box-solid">  
    <div class="box-body">             
         <asp:GridView ID="grdData" 
                             runat="server" 
                                                        GridLines="None" Width="100%" PageSize="20" CssClass="table table-hover">
            <RowStyle HorizontalAlign="Center" />
            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <headerstyle CssClass="text-top"                  VerticalAlign="Top" HorizontalAlign="Center" />          
          </asp:GridView>
              </div>
          </div>
</section>   
      
        </div>   
        <div class="row">       
        <div class="col-md-12 text-center">
          <asp:Button ID="cmdExport" CssClass="btn btn-primary" runat="server" Text="Export Data" Width="120px" /> 
        </div>
      </div>
</section>

     <script>
      
        var options = {
          series: [<%=databar1 %>],
          chart: {
          height: 350,
          type: 'line',
          dropShadow: {
            enabled: true,
            color: '#000',
            top: 18,
            left: 7,
            blur: 10,
            opacity: 0.2
          },
          toolbar: {
            show: true
          }
        },
        colors: [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ],
        dataLabels: {
          enabled: true,
        },
        stroke: {
          curve: 'smooth'
        },
        title: {
          text: 'Feedback report',
          align: 'left'
        },
        grid: {
          borderColor: '#e7e7e7',
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        markers: {
          size: 1
        },
        xaxis: {
          categories: [<%=catebar1 %>],
          title: {
            text: 'Survey Criterias'
          }
        },
        yaxis: {
          title: {
            text: ''
          },
          min: 0,
          max: 5
        },
        legend: {
          position: 'top',
          horizontalAlign: 'center',
          floating: true,
          offsetY: -25,
          offsetX: -5
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>


    <script>
      
        var options2 = {
          series: [{
          name: '',
          data: [<%=databar1 %>]
        }],
          chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            borderRadius: 10,
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ["#304758"]
          }
        },
        
        xaxis: {
          categories: [<%=catebar1 %>],
          position: 'bottom',
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: true
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#D8E3F0',
                colorTo: '#BED1E6',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false,
            },
          min: 0,
      max: 5,  
          labels: {
            show: true,
            formatter: function (val) {
              return val ;
            }
          }
        
        },
        title: {
          text: '',
          show: false,
          position:'top',
          floating: true,
          offsetY: 330,
          align: 'center',
          style: {
            color: '#444'
          }
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
        chart2.render();
      
      
    </script>  
                
</asp:Content>