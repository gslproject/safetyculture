﻿Public Class CompanyModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CompanyController
    Dim ctlM As New MasterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("SafetyCS")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("SafetyCS")("ROLE_ADM") = False And Request.Cookies("SafetyCS")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadProvince()
            'LoadBusinessType()
            'LoadCategory()
            LoadCompanyData()
        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
        txtMaxPerson.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtEmployeeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub
    'Private Sub LoadBusinessType()
    '    ddlBusinessType.DataSource = ctlM.BusinessType_Get()
    '    ddlBusinessType.DataTextField = "Name"
    '    ddlBusinessType.DataValueField = "UID"
    '    ddlBusinessType.DataBind()
    'End Sub
    Private Sub LoadProvince()
        ddlProvince.DataSource = ctlM.Province_Get()
        ddlProvince.DataTextField = "ProvinceName"
        ddlProvince.DataValueField = "ProvinceID"
        ddlProvince.DataBind()
    End Sub
    'Private Sub LoadCategory()
    '    chkCategory.DataSource = ctlM.Category_GetActive()
    '    chkCategory.DataTextField = "Name"
    '    chkCategory.DataValueField = "UID"
    '    chkCategory.DataBind()
    'End Sub
    Private Sub LoadCompanyData()
        dt = ctlE.Company_GetByUID(Request("cid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim dr As DataRow = dt.Rows(0)

                hdCompanyUID.Value = String.Concat(dr("UID"))
                txtCompanyID.Text = String.Concat(dr("CompanyCode"))
                txtNameTH.Text = String.Concat(dr("NameTH"))
                txtNameEN.Text = String.Concat(dr("NameEN"))
                txtBranch.Text = String.Concat(dr("Branch"))
                txtTaxID.Text = String.Concat(dr("VATID"))
                txtAddressNo.Text = String.Concat(dr("AddressNumber"))
                txtDistrict.Text = String.Concat(dr("District"))
                txtEmail.Text = String.Concat(dr("Email"))
                txtFax.Text = String.Concat(dr("Fax"))
                txtLane.Text = String.Concat(dr("Lane"))
                txtRoad.Text = String.Concat(dr("Road"))
                txtSubDistrict.Text = String.Concat(dr("SubDistrict"))
                txtTel.Text = String.Concat(dr("Telephone"))
                txtWebsite.Text = String.Concat(dr("Website"))
                txtZipcode.Text = String.Concat(dr("Zipcode"))
                ddlCountry.SelectedValue = String.Concat(dr("Country"))
                ddlProvince.SelectedValue = String.Concat(dr("ProvinceID"))
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(dr("StatusFlag")))

                'ddlBusinessType.SelectedValue = String.Concat(dr("BusinessTypeUID"))
                'optPhase.SelectedValue = String.Concat(dr("PhaseUID"))
                txtMaxPerson.Text = String.Concat(dr("MAXPERSON"))
                'txtEmployeeQTY.Text = String.Concat(dr("Employee"))
                'ddlBusinessGoup.SelectedValue = String.Concat(dr("BusinessGroupUID"))

                'ddlPackage.SelectedValue = String.Concat(dr("PackageNo"))

                'LoadCategoryDetail(String.Concat(.Item("UID")))

                dr = Nothing


            End With

        Else

        End If
    End Sub
    'Private Sub LoadCategoryDetail(CompanyUID As Integer)
    '    Dim dtC As New DataTable
    '    dtC = ctlE.CompanyCategoryDetail_Get(CompanyUID)
    '    If dtC.Rows.Count > 0 Then
    '        chkCategory.ClearSelection()

    '        For i = 0 To chkCategory.Items.Count - 1
    '            For n = 0 To dtC.Rows.Count - 1
    '                If chkCategory.Items(i).Value = dtC.Rows(n)("CategoryUID") Then
    '                    chkCategory.Items(i).Selected = True
    '                End If
    '            Next
    '        Next
    '    End If
    'End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtCompanyID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุรหัสบริษัท');", True)
            Exit Sub
        End If
        If txtNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtMaxPerson.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุจำนวนพนักงานสูงสุด (Max Person)');", True)
            Exit Sub
        End If

        ctlE.Company_Save(StrNull2Zero(hdCompanyUID.Value), txtCompanyID.Text, txtNameTH.Text, txtNameEN.Text, txtBranch.Text, txtTaxID.Text, txtAddressNo.Text, txtLane.Text, txtRoad.Text, txtSubDistrict.Text, txtDistrict.Text, ddlProvince.SelectedValue, txtZipcode.Text, ddlCountry.SelectedValue, txtTel.Text, txtFax.Text, txtEmail.Text, txtWebsite.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), StrNull2Zero(txtMaxPerson.Text), Request.Cookies("SafetyCS")("userid"))

        Dim CompanyUID As Integer

        If StrNull2Zero(hdCompanyUID.Value) = 0 Then
            CompanyUID = ctlE.Company_GetUID(txtCompanyID.Text)

        Else
            CompanyUID = hdCompanyUID.Value
        End If

        Dim ctlCfg As New SystemConfigController
        ctlCfg.DataConfig_Add4NewCompany(CompanyUID)


        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("SafetyCS")("username"), ACTTYPE_UPD, "Company", "บันทึก/แก้ไข บริษัท/โรงงาน :{uid=" & hdCompanyUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        'ctlE.CompanyCategoryDetail_Delete(CompanyUID)

        'For i = 0 To chkCategory.Items.Count - 1
        '    If chkCategory.Items(i).Selected Then
        '        ctlE.CompanyCategoryDetail_Save(CompanyUID, chkCategory.Items(i).Value, Request.Cookies("SafetyCS")("userid"))
        '    End If
        'Next

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("Company.aspx?p=complete")
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlE.Company_Delete(StrNull2Zero(hdCompanyUID.Value))
    End Sub
End Class